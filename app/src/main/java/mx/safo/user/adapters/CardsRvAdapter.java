package mx.safo.user.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.daimajia.swipe.SwipeLayout;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.activities.OrderDriverActivity;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.CardM;
import mx.safo.user.models.User;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by moyhdez on 24/02/17.
 */

public class CardsRvAdapter extends RecyclerView.Adapter<CardsRvAdapter.CardsViewHolder> {
    private int expandedCell = -1;
    private List<CardM> cards;
    private Activity activity;
    private Boolean canSwipe;

    public CardsRvAdapter(List<CardM> cards, Activity activity, Boolean canSwipe) {
        this.activity = activity;
        this.cards = cards;
        this.canSwipe = canSwipe;
    }

    @Override
    public CardsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (canSwipe)
            return new CardsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_cards_item_swipe, parent, false));
        else
            return new CardsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_cards_item, parent, false));
    }

    @Override
    public void onBindViewHolder(CardsViewHolder holder, int position) {
        CardM card = cards.get(holder.getAdapterPosition());
        if (card.getImage() != null) {
            Picasso.with(activity)
                    .load(card.getImage())
                    .into(holder.ivBrand);
//            holder.ivBrand.setImageDrawable(ContextCompat.getDrawable(activity, card.getImage()));
            holder.tvDigits.setText(card.getLastDigits());
        }
        if (canSwipe) {
            if (expandedCell == holder.getAdapterPosition())
                holder.swipeLayout.open(false);
            else if (holder.swipeLayout.getOpenStatus() == SwipeLayout.Status.Open
                    || holder.swipeLayout.getOpenStatus() == SwipeLayout.Status.Middle)
                holder.swipeLayout.close(true);
        }

        if (activity instanceof OrderDriverActivity)
            holder.itemView.setBackgroundColor(activity.getResources().getColor(
                    holder.getAdapterPosition() == 0 ? R.color.white : R.color.greyLightBackground));

        holder.tvTitle.setVisibility(holder.getAdapterPosition() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public class CardsViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDigits;
        private ImageView ivBrand;
        private TextView tvTitle;
        private SwipeLayout swipeLayout;

        public CardsViewHolder(final View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_cards_item);
            tvDigits = (TextView) itemView.findViewById(R.id.tv_last_digits_cards_item);
            ivBrand = (ImageView) itemView.findViewById(R.id.iv_card_brand_cards_item);

            if (canSwipe) swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);


            if (canSwipe) {
                ((SwipeLayout) itemView.findViewById(R.id.swipe)).addSwipeListener(new SwipeLayout.SwipeListener() {
                    @Override
                    public void onStartOpen(SwipeLayout layout) {
                        swipeLayout.setOnClickListener(null);
                    }

                    @Override
                    public void onOpen(SwipeLayout layout) {
                        swipeLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (swipeLayout.getOpenStatus() == SwipeLayout.Status.Open) {
                                    swipeLayout.close();
                                }
                            }
                        });

                        if (expandedCell != getAdapterPosition()) {
                            int aux = expandedCell;
                            expandedCell = getAdapterPosition();

                            if (aux >= 0) notifyItemChanged(aux);
                        }
                    }

                    @Override
                    public void onStartClose(SwipeLayout layout) {
                        swipeLayout.setOnClickListener(null);
                    }

                    @Override
                    public void onClose(SwipeLayout layout) {
                        swipeLayout.setOnClickListener(null);
                        if (expandedCell == getAdapterPosition()) expandedCell = -1;
                    }

                    @Override
                    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                    }

                    @Override
                    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                    }
                });

                itemView.findViewById(R.id.delete_card).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(cards.size() > 1)
                            deleteCard();
                        else Toast.makeText(activity, "No puede eliminar su unica forma de pago", Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (activity instanceof OrderDriverActivity && cards.size() > 1) {
                            OrderDriverActivity orderDriverActivity = (OrderDriverActivity) activity;
                            if (orderDriverActivity.currentScreen == OrderDriverActivity.screens.confirm
                                    || orderDriverActivity.currentScreen == OrderDriverActivity.screens.selectCard
                                    || orderDriverActivity.currentScreen == OrderDriverActivity.screens.selectCar) {

                                if (orderDriverActivity.currentScreen == OrderDriverActivity.screens.selectCar) {
                                    orderDriverActivity.carSelected(0);
                                }

                                cards = orderDriverActivity.cardSelected(getAdapterPosition());

                                notifyDataSetChanged();
                            }
                        }
                    }
                });
            }
        }

        private void deleteCard(){
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(itemView.getContext());
            alertDialogBuilder.setMessage("¿Desea eliminar esta forma de pago?")
                    .setCancelable(false)
                    .setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
//                                    Map<String, String> params = new LinkedHashMap<>();
//                                    params.put("card_id", cards.get(getAdapterPosition()).getId().toString());
//                                    params.put("row_card_Index", String.valueOf(getAdapterPosition()));
                                    String url = activity.getString(R.string.delete_card, BuildConfig.API_URL) + "/" + cards.get(getAdapterPosition()).getId().toString() + "/0";
                                    ApiClient.getInstance(activity).addToRequestQueue(new ApiClient.GsonRequest<>(
                                            url,
                                            ApiResponse.class,
                                            Request.Method.GET,
                                            ApiClient.Header.HEADER_DEFAULT,
                                            null,
                                            new Response.Listener<ApiResponse>() {
                                                @Override
                                                public void onResponse(ApiResponse response) {
                                                    cards.remove(getAdapterPosition());
                                                    notifyItemRemoved(getAdapterPosition());

                                                    // Add this card to locally stored user
                                                    User user = User.fromSharedPref(activity);
                                                    user.removeCards();
                                                    user.setCards(cards);
                                                    SharedPreferences.Editor editor = activity.getSharedPreferences("user", MODE_PRIVATE).edit();
                                                    editor.putString("user", new Gson().toJson(user));
                                                    editor.apply();
                                                }
                                            },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    if(!error.getMessage().isEmpty())
                                                    Toast.makeText(activity, error.getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            }, true
                                    ));
                                }
                            });
            alertDialogBuilder.setNegativeButton("Cancelar",null);
            AlertDialog alertGPS = alertDialogBuilder.create();
            alertGPS.show();

        }
    }
}
