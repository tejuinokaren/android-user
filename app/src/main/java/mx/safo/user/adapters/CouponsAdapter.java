package mx.safo.user.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import mx.safo.user.R;
import mx.safo.user.models.Coupon;

/**
 * Created by Fabian on 27/07/17.
 */

public class CouponsAdapter extends RecyclerView.Adapter<CouponsAdapter.CouponsHolder> {

    private Activity activity;
    private ArrayList<Coupon> coupons;

    public CouponsAdapter(Activity activity, ArrayList<Coupon> coupons) {
        this.activity = activity;
        this.coupons = new ArrayList<>();
        //Add only the active coupons
        for (Coupon coupon: coupons) {
            if(coupon.getStatus().equals("Canjeado"))
                this.coupons.add(coupon);
        }
    }

    public CouponsAdapter(Activity activity) {
        this.activity = activity;

    }

    @Override
    public CouponsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CouponsHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_coupons, parent, false));
    }

    @Override
    public void onBindViewHolder(CouponsHolder holder, int position) {
        if(coupons.get(position).getPromo() != null) {
            holder.tvTitle.setText(coupons.get(position).getPromo().getTitle());
            holder.tvDescription.setText("$" + coupons.get(position).getAmount() + " MXN en tu próximo viaje");
            holder.tvExpiresIn.setText("Vigencia:(" + coupons.get(position).getPromo().getEnd() + ")");
            Picasso.with(activity).load(coupons.get(position).getPromo().getImage()).error(R.drawable.profile_holder).into(holder.ivCoupon);
//        changeColorStatus(holder, coupons.get(position).getStatus());
        }
    }

    @Override
    public int getItemCount() {
        return coupons.size();
    }

    public void setCoupons(ArrayList<Coupon> coupons) {
        this.coupons = new ArrayList<>();
        for (Coupon coupon: coupons) {
            if(coupon.getStatus().equals("Canjeado"))
                this.coupons.add(coupon);
        }
    }

    class CouponsHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private TextView tvDescription;
        private TextView tvExpiresIn;
//        private TextView tvStatus;
        private ImageView ivCoupon;

        CouponsHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_coupon_title);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_coupon_description);
//            tvStatus = (TextView) itemView.findViewById(R.id.tv_coupon_status);
            tvExpiresIn = (TextView) itemView.findViewById(R.id.tv_coupon_expires_in);
            ivCoupon = (ImageView) itemView.findViewById(R.id.iv_coupon);
        }

    }

}
