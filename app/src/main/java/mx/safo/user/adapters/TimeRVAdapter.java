package mx.safo.user.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.View;


import mx.safo.user.R;

/**
 * Created by moyhdez on 06/03/17.
 */

public class TimeRVAdapter extends RecyclerView.Adapter<TimeRVAdapter.TimeRVViewHolder> {

    private Boolean isHours;

    public TimeRVAdapter(boolean isHours) {
        this.isHours = isHours;
    }

    @Override
    public TimeRVViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TimeRVViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_schedule_item, parent, false));
    }

    @Override
    public void onBindViewHolder(TimeRVViewHolder holder, int position) {
        String format = "%02d"; // two digits
        String text;
        if (isHours)
            text = String.format(format, holder.getAdapterPosition() % 12 + 1);
        else
            text = String.format(format, (holder.getAdapterPosition() % 12) * 5);
        holder.textView.setText(text);
    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }

    public class TimeRVViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;

        public TimeRVViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tv_schedule_item);
        }
    }

}
