package mx.safo.user.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.daimajia.swipe.SwipeLayout;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.activities.OrderDriverActivity;
import mx.safo.user.activities.VehicleDetailsActivity;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.User;
import mx.safo.user.models.Vehicle;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by moyhdez on 24/02/17.
 */

public class VehiclesRvAdapter extends RecyclerView.Adapter<VehiclesRvAdapter.VehiclesViewHolder> {
    private static final int EDIT_VEHICLE = 467;
    private int expandedCell = -1;
    private List<Vehicle> vehicles;
    private Activity activity;
    private Boolean canSwipe;

    public VehiclesRvAdapter(List<Vehicle> vehicles, Activity activity, Boolean canSwipe) {
        this.activity = activity;
        this.vehicles = vehicles;
        this.canSwipe = canSwipe;
    }

    public VehiclesRvAdapter(List<Vehicle> vehicles, Boolean canSwipe) {
        this.activity = activity;
        this.vehicles = vehicles;
        this.canSwipe = canSwipe;
    }

    @Override
    public VehiclesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (canSwipe)
            return new VehiclesViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_cars_item_swipe, parent, false));
        else
            return new VehiclesViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_vehicles_item, parent, false));
    }

    @Override
    public void onBindViewHolder(VehiclesViewHolder holder, int position) {
        Vehicle vehicle = vehicles.get(holder.getAdapterPosition());
        Picasso.with(holder.itemView.getContext()).load(vehicle.getModel().getBrand().getImage()).into(holder.ivBrand);
        holder.tvCar.setText(vehicle.getTitle());
        holder.tvPlates.setText(vehicle.getPlates());

        if (canSwipe) {
            if (expandedCell == holder.getAdapterPosition())
                holder.swipeLayout.open(false);
            else if (holder.swipeLayout.getOpenStatus() == SwipeLayout.Status.Open
                    || holder.swipeLayout.getOpenStatus() == SwipeLayout.Status.Middle)
                holder.swipeLayout.close(true);
        }

        if (activity instanceof OrderDriverActivity)
            holder.itemView.setBackgroundColor(activity.getResources().getColor(
                    holder.getAdapterPosition() == 0 ? R.color.white : R.color.greyLightBackground));

        holder.tvTitle.setVisibility(holder.getAdapterPosition() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return vehicles.size();
    }

    public class VehiclesViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCar;
        private TextView tvPlates;
        private ImageView ivBrand;
        private TextView tvTitle;
        private SwipeLayout swipeLayout;

        public VehiclesViewHolder(final View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_cars_item);
            tvCar = (TextView) itemView.findViewById(R.id.tv_car_cars_item);
            tvPlates = (TextView) itemView.findViewById(R.id.tv_plates_cars_item);
            ivBrand = (ImageView) itemView.findViewById(R.id.iv_car_brand_cars_item);

            if (canSwipe) swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);


            if (canSwipe) {
                swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
                    @Override
                    public void onStartOpen(SwipeLayout layout) {
                        swipeLayout.setOnClickListener(null);
                    }

                    @Override
                    public void onOpen(SwipeLayout layout) {
                        swipeLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (swipeLayout.getOpenStatus() == SwipeLayout.Status.Open) {
                                    swipeLayout.close();
                                }
                            }
                        });

                        if (expandedCell != getAdapterPosition()) {
                            int aux = expandedCell;
                            expandedCell = getAdapterPosition();

                            if (aux >= 0) notifyItemChanged(aux);
                        }
                    }

                    @Override
                    public void onStartClose(SwipeLayout layout) {
                        swipeLayout.setOnClickListener(null);
                    }

                    @Override
                    public void onClose(SwipeLayout layout) {
                        swipeLayout.setOnClickListener(null);
                        if (expandedCell == getAdapterPosition()) expandedCell = -1;
                    }

                    @Override
                    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                    }

                    @Override
                    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                    }
                });

                itemView.findViewById(R.id.edit_car).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(itemView.getContext(), VehicleDetailsActivity.class);
                        i.putExtra("vehicle", vehicles.get(getAdapterPosition()));
                        ((Activity) itemView.getContext()).startActivityForResult(i, EDIT_VEHICLE);

                    }
                });

                itemView.findViewById(R.id.delete_car).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(vehicles.size() > 1)
                            deleteVehicle();
                        else Toast.makeText(activity, "No puede eliminar su único automóvil activo", Toast.LENGTH_SHORT).show();
                    }
                });

                } else {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (activity instanceof OrderDriverActivity && vehicles.size() > 1) {
                            OrderDriverActivity orderDriverActivity = (OrderDriverActivity) activity;
                            if (orderDriverActivity.currentScreen == OrderDriverActivity.screens.confirm
                                    || orderDriverActivity.currentScreen == OrderDriverActivity.screens.selectCar
                                    || orderDriverActivity.currentScreen == OrderDriverActivity.screens.selectCard) {

                                if (orderDriverActivity.currentScreen == OrderDriverActivity.screens.selectCard)
                                    orderDriverActivity.cardSelected(0);

                                vehicles = orderDriverActivity.carSelected(getAdapterPosition());

                                notifyDataSetChanged();
                            }
                        }
                    }
                });
            }
        }

        private void deleteVehicle(){
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(itemView.getContext());
            alertDialogBuilder.setMessage("¿Desea eliminar este auto?")
                    .setCancelable(false)
                    .setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            String url = activity.getString(R.string.delete_vehicle, BuildConfig.API_URL) + "/" + vehicles.get(getAdapterPosition()).getId().toString();
                            ApiClient.getInstance(activity).addToRequestQueue(new ApiClient.GsonRequest<>(
                                    url,
                                    ApiResponse.class,
                                    Request.Method.GET,
                                    ApiClient.Header.HEADER_DEFAULT,
                                    null,
                                    new Response.Listener<ApiResponse>() {
                                        @Override
                                        public void onResponse(ApiResponse response) {
                                            vehicles.remove(getAdapterPosition());
                                            notifyItemRemoved(getAdapterPosition());

                                            // Add this card to locally stored user
                                            User user = User.fromSharedPref(activity);
                                            user.removeVehicles();
                                            user.setVehicles(vehicles);
                                            SharedPreferences.Editor editor = activity.getSharedPreferences("user", MODE_PRIVATE).edit();
                                            editor.putString("user", new Gson().toJson(user));
                                            editor.apply();
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            if(!error.getMessage().isEmpty())
                                                Toast.makeText(activity, error.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }, true
                            ));
                        }
                    });
            alertDialogBuilder.setNegativeButton("Cancelar",null);
            AlertDialog alertGPS = alertDialogBuilder.create();
            alertGPS.show();

        }

    }
}
