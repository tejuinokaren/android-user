package mx.safo.user.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import mx.safo.user.R;
import mx.safo.user.activities.HomeMapActivity;
import mx.safo.user.activities.OrderDriverActivity;
import mx.safo.user.models.PlaceLocal;

/**
 * Created by moyhdez on 22/02/17.
 */

public class SearchRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<PlaceLocal> places;
    private Context mContext;

    public SearchRvAdapter(ArrayList<PlaceLocal> places, Context context) {
        this.places = places;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case PlaceLocal.TYPE_HEADER:
                return new HeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_search_header, parent, false));
                default: return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_search_item, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PlaceLocal place = places.get(holder.getAdapterPosition());

        switch (getItemViewType(position)) {
            case PlaceLocal.TYPE_ITEM:
                ItemViewHolder itemHolder = (ItemViewHolder) holder;
                itemHolder.primaryTv.setText(place.getPrimaryText());
                itemHolder.secondaryTv.setText(place.getSecondaryText());
                break;
            default:
                // HeaderView doesn't require any special config
                break;
        }
    }

    @Override
    public int getItemCount() {
        return places.size();
    }

    @Override
    public int getItemViewType(int position) {
        return places.get(position).getItemType();
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView primaryTv;
        private TextView secondaryTv;

        public ItemViewHolder(View itemView) {
            super(itemView);
            primaryTv = (TextView) itemView.findViewById(R.id.tv_primary_search_item);
            secondaryTv = (TextView) itemView.findViewById(R.id.tv_secondary_search_item);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mContext instanceof HomeMapActivity) {
                        ((HomeMapActivity) mContext).placeSelected(getAdapterPosition());
                    } else if(mContext instanceof OrderDriverActivity) {
                        ((OrderDriverActivity) mContext).placeSelected(getAdapterPosition());
                    }
                }
            });
        }
    }
}
