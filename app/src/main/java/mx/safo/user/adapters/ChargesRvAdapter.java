package mx.safo.user.adapters;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import mx.safo.user.R;
import mx.safo.user.models.CardM;
import mx.safo.user.models.Charges;

/**
 * Created by Fabian on 03/08/17.
 */

public class ChargesRvAdapter extends RecyclerView.Adapter<ChargesRvAdapter.ChargesViewHolder> {
    private static final int EDIT_VEHICLE = 467;
    private Activity activity;
    private ArrayList<Charges> charges;
    private CardM cardM;



    public ChargesRvAdapter(ArrayList<Charges> charges, Activity activity, CardM cardM) {
        this.activity = activity;
        this.charges = charges;
        this.cardM = cardM;
    }

    @Override
    public ChargesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ChargesViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_charges_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ChargesViewHolder holder, int position) {
        holder.tvAmount.setText("$" + charges.get(position).getAmount() + " MXN");
        if(charges.get(position).getMethod().equals("Cupón")) {
            holder.tvSubInfo.setText(charges.get(position).getAuthCode());
            holder.tvInfo.setText("Cupón: ");
            holder.ivImage.setVisibility(View.INVISIBLE);
        }
        else{
            holder.tvInfo.setText("Tarjeta de pago: ");
            holder.tvSubInfo.setText(cardM.getLastDigits());
            holder.ivImage.setVisibility(View.VISIBLE);
            holder.ivImage.setImageDrawable(ContextCompat.getDrawable(activity, cardM.getImage()));
        }
    }

    @Override
    public int getItemCount() {
        return charges.size();
    }

    class ChargesViewHolder extends RecyclerView.ViewHolder {
        private TextView tvInfo;
        private TextView tvSubInfo;
        private ImageView ivImage;
        private TextView tvAmount;

        ChargesViewHolder(final View itemView) {
            super(itemView);
            tvInfo = (TextView) itemView.findViewById(R.id.tv_info_charges_item);
            tvSubInfo = (TextView) itemView.findViewById(R.id.tv_sub_info_charges_item);
            ivImage = (ImageView) itemView.findViewById(R.id.iv_image_charges_item);
            tvAmount = (TextView) itemView.findViewById(R.id.tv_amount_charges_item);

        }
    }
}
