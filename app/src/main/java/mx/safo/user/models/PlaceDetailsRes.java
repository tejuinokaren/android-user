package mx.safo.user.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by karen on 03/11/17.
 */

public class PlaceDetailsRes implements Serializable {
    @SerializedName("result")
    @Expose
    public Result result;
}
