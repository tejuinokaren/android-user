package mx.safo.user.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Fabian on 31/08/17.
 */

public class Vehicle implements Serializable, Cloneable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("brand_id")
    @Expose
    private Integer brandId;
    @SerializedName("model_id")
    @Expose
    private Integer modelId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("plates")
    @Expose
    private String plates;
    @SerializedName("selected")
    @Expose
    private Integer selected;
    @SerializedName("year")
    @Expose
    private Integer year;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("model")
    @Expose
    private Model model;
    @SerializedName("insurance_company")
    @Expose
    private String insuranceCompany;
    @SerializedName("insurance_number")
    @Expose
    private String insuranceNumber;
    @SerializedName("insurance_expiration")
    @Expose
    private String insuranceExpiration;

    @Override
    protected Vehicle clone() throws CloneNotSupportedException {
        Vehicle cloned = (Vehicle)super.clone();
        // the above is applicable in case of primitive member types,
        // however, in case of non primitive types
        // cloned.setNonPrimitiveType(cloned.getNonPrimitiveType().clone());
        return cloned;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPlates() {
        return plates;
    }

    public void setPlates(String plates) {
        this.plates = plates;
    }

    public Integer getSelected() {
        return selected;
    }

    public void setSelected(Integer selected) {
        this.selected = selected;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getInsuranceExpiration() {
        return insuranceExpiration;
    }

    public void setInsuranceExpiration(String insuranceExpiration) {
        this.insuranceExpiration = insuranceExpiration;
    }
}