package mx.safo.user.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tejuino on 7/09/17.
 */

public class Charges implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("trip_id")
    @Expose
    private Integer tripId;
    @SerializedName("coupon_id")
    @Expose
    private Object couponId;
    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("auth_code")
    @Expose
    private String authCode;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("conekta_order")
    @Expose
    private String conektaOrder;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("created_at_date")
    @Expose
    private String createdAtDate;
    @SerializedName("created_at_time")
    @Expose
    private String createdAtTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTripId() {
        return tripId;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    public Object getCouponId() {
        return couponId;
    }

    public void setCouponId(Object couponId) {
        this.couponId = couponId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getConektaOrder() {
        return conektaOrder;
    }

    public void setConektaOrder(String conektaOrder) {
        this.conektaOrder = conektaOrder;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAtDate() {
        return createdAtDate;
    }

    public void setCreatedAtDate(String createdAtDate) {
        this.createdAtDate = createdAtDate;
    }

    public String getCreatedAtTime() {
        return createdAtTime;
    }

    public void setCreatedAtTime(String createdAtTime) {
        this.createdAtTime = createdAtTime;
    }
}
