package mx.safo.user.models;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fabian on 04/09/17.
 */

public class User implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("role_id")
    @Expose
    private Integer roleId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("birth_date")
    @Expose
    private String birthDate;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("total_ratings")
    @Expose
    private Integer totalRatings;
    @SerializedName("score")
    @Expose
    private Integer score;
    @SerializedName("fb")
    @Expose
    private Integer fb;
    @SerializedName("vehicles")
    @Expose
    private List<Vehicle> mVehicles = new ArrayList<>();
    @SerializedName("cards")
    @Expose
    private List<CardM> mCards = new ArrayList<>();
    @SerializedName("contact1_name")
    @Expose
    private String contact1Name;
    @SerializedName("contact1_phone")
    @Expose
    private String contact1Phone;
    @SerializedName("contact2_name")
    @Expose
    private String contact2Name;
    @SerializedName("contact2_phone")
    @Expose
    private String contact2Phone;
    @SerializedName("is_driver")
    @Expose
    private Integer isDriver;

    private AuthToken authToken;

    public static User fromSharedPref(Context context) {
        return new Gson().fromJson(context.getSharedPreferences("user", Context.MODE_PRIVATE)
                .getString("user", null), User.class);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getTotalRatings() {
        return totalRatings;
    }

    public void setTotalRatings(Integer totalRatings) {
        this.totalRatings = totalRatings;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getFb() {
        return fb;
    }

    public void setFb(Integer fb) {
        this.fb = fb;
    }

    public AuthToken getAuthToken() {
        return authToken;
    }

    public void setAuthToken(AuthToken authToken) {
        this.authToken = authToken;
    }

    public List<Vehicle> getVehicles() {
        return mVehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        List<Vehicle> copy = cloneVehiclesList(vehicles);
        if(this.mVehicles != null) {
            this.mVehicles.clear();
            this.mVehicles.addAll(copy);
        } else {
            this.mVehicles = copy;
        }
    }

    public void removeVehicles() {
        if(this.mVehicles != null) {
            this.mVehicles.clear();
        }
    }

    public void addVehicle(Vehicle vehicle) {
        if(this.mVehicles == null) {
            this.mVehicles = new ArrayList<>();
        }
        this.mVehicles.add(vehicle);
    }
    public List<CardM> getCards() {
        return mCards;
    }

    public void setCards(List<CardM> cards) {
        List<CardM> copy = cloneCardsList(cards);
        if(this.mCards != null) {
            this.mCards.clear();
            this.mCards.addAll(copy);
        } else {
            this.mCards = copy;
        }

    }

    public void addCard(CardM card) {
        if(this.mCards == null) {
            this.mCards = new ArrayList<>();
        }
        this.mCards.add(card);
    }

    public void removeCards() {
        if(this.mCards != null) {
            this.mCards.clear();
        }
    }

    public static List<CardM> cloneCardsList(List<CardM> list) {
        List<CardM> clone = new ArrayList<>(list.size());
        for (CardM item : list)
            try {
                clone.add(item.clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        return clone;
    }

    public static List<Vehicle> cloneVehiclesList(List<Vehicle> list) {
        List<Vehicle> clone = new ArrayList<>(list.size());
        for (Vehicle item : list)
            try {
                clone.add(item.clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        return clone;
    }

    public String getContact1Name() {
        return contact1Name;
    }

    public void setContact1Name(String contact1Name) {
        this.contact1Name = contact1Name;
    }

    public String getContact1Phone() {
        return contact1Phone;
    }

    public void setContact1Phone(String contact1Phone) {
        this.contact1Phone = contact1Phone;
    }

    public String getContact2Name() {
        return contact2Name;
    }

    public void setContact2Name(String contact2Name) {
        this.contact2Name = contact2Name;
    }

    public String getContact2Phone() {
        return contact2Phone;
    }

    public void setContact2Phone(String contact2Phone) {
        this.contact2Phone = contact2Phone;
    }

    public Integer getIsDriver() {
        return isDriver;
    }

    public void setIsDriver(Integer isDriver) {
        this.isDriver = isDriver;
    }
}
