package mx.safo.user.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Fabian on 05/09/17.
 */

public class auxArrayResponse implements Serializable{
    @SerializedName("data")
    @Expose
    public auxArrayData data = null;

    @SerializedName("code")
    @Expose
    public int code = 0;

    @SerializedName("message")
    @Expose
    public String message = null;

    public void setData(auxArrayData data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
