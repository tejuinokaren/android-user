package mx.safo.user.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tejuino on 7/09/17.
 */

public class Trip implements Serializable {

    public static final int TAG_PROGRESS = 0;
    public static final int TAG_HISTORY = 1;

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("driver_user_id")
    @Expose
    private String driverUserId;
    @SerializedName("driver_partner_user_id")
    @Expose
    private String driverPartnerUserId;
    @SerializedName("city_id")
    @Expose
    private Integer cityId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("card_id")
    @Expose
    private Integer cardId;
    @SerializedName("vehicle_id")
    @Expose
    private Integer vehicleId;
    @SerializedName("driver_vehicle_id")
    @Expose
    private String driverVehicleId;
    @SerializedName("request_origin_lat")
    @Expose
    private String requestOriginLat;
    @SerializedName("request_origin_lng")
    @Expose
    private String requestOriginLng;
    @SerializedName("request_origin_address")
    @Expose
    private String requestOriginAddress;
    @SerializedName("request_destination_lat")
    @Expose
    private String requestDestinationLat;
    @SerializedName("request_destination_lng")
    @Expose
    private String requestDestinationLng;
    @SerializedName("request_destination_address")
    @Expose
    private String requestDestinationAddress;
    @SerializedName("requested_at")
    @Expose
    private String requestedAt;
    @SerializedName("origin_lat")
    @Expose
    private String originLat;
    @SerializedName("origin_lng")
    @Expose
    private String originLng;
    @SerializedName("origin_address")
    @Expose
    private String originAddress;
    @SerializedName("destination_lat")
    @Expose
    private String destinationLat;
    @SerializedName("destination_lng")
    @Expose
    private String destinationLng;
    @SerializedName("destination_address")
    @Expose
    private String destinationAddress;
    @SerializedName("started_at")
    @Expose
    private String startedAt;
    @SerializedName("finished_at")
    @Expose
    private String finishedAt;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("payment_total")
    @Expose
    private String paymentTotal;
    @SerializedName("payment_payed")
    @Expose
    private String paymentPayed;
    @SerializedName("payment_remaining")
    @Expose
    private String paymentRemaining;
    @SerializedName("payment_attempts")
    @Expose
    private Integer paymentAttempts;
    @SerializedName("rate_start")
    @Expose
    private String rateStart;
    @SerializedName("rate_minute")
    @Expose
    private String rateMinute;
    @SerializedName("rate_km")
    @Expose
    private String rateKm;
    @SerializedName("rate_minimum")
    @Expose
    private String rateMinimum;
    @SerializedName("comission")
    @Expose
    private String comission;
    @SerializedName("estimated_distance")
    @Expose
    private String estimatedDistance;
    @SerializedName("estimated_time")
    @Expose
    private String estimatedTime;
    @SerializedName("estimated_total")
    @Expose
    private Float estimatedTotal;
    @SerializedName("estimated_polylines")
    @Expose
    private String estimatedPolylines;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("polylines")
    @Expose
    private String polylines;
    @SerializedName("map_image")
    @Expose
    private String mapImage;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("seen")
    @Expose
    private Integer seen;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("driver")
    @Expose
    private Driver driver;
    @SerializedName("driver_partner")
    @Expose
    private Driver driverPartner;
    @SerializedName("vehicle")
    @Expose
    private Vehicle vehicle;
    @SerializedName("driver_vehicle")
    @Expose
    private Vehicle driverVehicle;
    @SerializedName("card")
    @Expose
    private CardM card;
    @SerializedName("waypoints")
    @Expose
    private List<Waypoint> waypoints = null;
    @SerializedName("charges")
    @Expose
    private List<Charges> charges = null;

    private Integer type;

    public Trip() {
        this.type = TAG_HISTORY;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDriverUserId() {
        return driverUserId;
    }

    public void setDriverUserId(String driverUserId) {
        this.driverUserId = driverUserId;
    }

    public String getDriverPartnerUserId() {
        return driverPartnerUserId;
    }

    public void setDriverPartnerUserId(String driverPartnerUserId) {
        this.driverPartnerUserId = driverPartnerUserId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getDriverVehicleId() {
        return driverVehicleId;
    }

    public void setDriverVehicleId(String driverVehicleId) {
        this.driverVehicleId = driverVehicleId;
    }

    public String getRequestOriginLat() {
        return requestOriginLat;
    }

    public void setRequestOriginLat(String requestOriginLat) {
        this.requestOriginLat = requestOriginLat;
    }

    public String getRequestOriginLng() {
        return requestOriginLng;
    }

    public void setRequestOriginLng(String requestOriginLng) {
        this.requestOriginLng = requestOriginLng;
    }

    public String getRequestOriginAddress() {
        return requestOriginAddress;
    }

    public void setRequestOriginAddress(String requestOriginAddress) {
        this.requestOriginAddress = requestOriginAddress;
    }

    public String getRequestDestinationLat() {
        return requestDestinationLat;
    }

    public void setRequestDestinationLat(String requestDestinationLat) {
        this.requestDestinationLat = requestDestinationLat;
    }

    public String getRequestDestinationLng() {
        return requestDestinationLng;
    }

    public void setRequestDestinationLng(String requestDestinationLng) {
        this.requestDestinationLng = requestDestinationLng;
    }

    public String getRequestDestinationAddress() {
        return requestDestinationAddress;
    }

    public void setRequestDestinationAddress(String requestDestinationAddress) {
        this.requestDestinationAddress = requestDestinationAddress;
    }

    public String getRequestedAt() {
        return requestedAt;
    }

    public void setRequestedAt(String requestedAt) {
        this.requestedAt = requestedAt;
    }

    public String getOriginLat() {
        return originLat;
    }

    public void setOriginLat(String originLat) {
        this.originLat = originLat;
    }

    public String getOriginLng() {
        return originLng;
    }

    public void setOriginLng(String originLng) {
        this.originLng = originLng;
    }

    public String getOriginAddress() {
        return originAddress;
    }

    public void setOriginAddress(String originAddress) {
        this.originAddress = originAddress;
    }

    public String getDestinationLat() {
        return destinationLat;
    }

    public void setDestinationLat(String destinationLat) {
        this.destinationLat = destinationLat;
    }

    public String getDestinationLng() {
        return destinationLng;
    }

    public void setDestinationLng(String destinationLng) {
        this.destinationLng = destinationLng;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    public String getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(String finishedAt) {
        this.finishedAt = finishedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentTotal() {
        return paymentTotal;
    }

    public void setPaymentTotal(String paymentTotal) {
        this.paymentTotal = paymentTotal;
    }

    public String getPaymentPayed() {
        return paymentPayed;
    }

    public void setPaymentPayed(String paymentPayed) {
        this.paymentPayed = paymentPayed;
    }

    public String getPaymentRemaining() {
        return paymentRemaining;
    }

    public void setPaymentRemaining(String paymentRemaining) {
        this.paymentRemaining = paymentRemaining;
    }

    public Integer getPaymentAttempts() {
        return paymentAttempts;
    }

    public void setPaymentAttempts(Integer paymentAttempts) {
        this.paymentAttempts = paymentAttempts;
    }

    public String getRateStart() {
        return rateStart;
    }

    public void setRateStart(String rateStart) {
        this.rateStart = rateStart;
    }

    public String getRateMinute() {
        return rateMinute;
    }

    public void setRateMinute(String rateMinute) {
        this.rateMinute = rateMinute;
    }

    public String getRateKm() {
        return rateKm;
    }

    public void setRateKm(String rateKm) {
        this.rateKm = rateKm;
    }

    public String getRateMinimum() {
        return rateMinimum;
    }

    public void setRateMinimum(String rateMinimum) {
        this.rateMinimum = rateMinimum;
    }

    public String getComission() {
        return comission;
    }

    public void setComission(String comission) {
        this.comission = comission;
    }

    public String getEstimatedDistance() {
        return estimatedDistance;
    }

    public void setEstimatedDistance(String estimatedDistance) {
        this.estimatedDistance = estimatedDistance;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public Float getEstimatedTotal() {
        return estimatedTotal;
    }

    public void setEstimatedTotal(Float estimatedTotal) {
        this.estimatedTotal = estimatedTotal;
    }

    public String getEstimatedPolylines() {
        return estimatedPolylines;
    }

    public void setEstimatedPolylines(String estimatedPolylines) {
        this.estimatedPolylines = estimatedPolylines;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getPolylines() {
        return polylines;
    }

    public void setPolylines(String polylines) {
        this.polylines = polylines;
    }

    public String getMapImage() {
        return mapImage;
    }

    public void setMapImage(String mapImage) {
        this.mapImage = mapImage;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Driver getDriverPartner() {
        return driverPartner;
    }

    public void setDriverPartner(Driver driverPartner) {
        this.driverPartner = driverPartner;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Vehicle getDriverVehicle() {
        return driverVehicle;
    }

    public void setDriverVehicle(Vehicle driverVehicle) {
        this.driverVehicle = driverVehicle;
    }

    public CardM getCard() {
        return card;
    }

    public void setCard(CardM card) {
        this.card = card;
    }

    public Integer getSeen() {
        return seen;
    }

    public void setSeen(Integer seen) {
        this.seen = seen;
    }

    public Integer getType() {
        return type;
    }

    public Trip setType(Integer type) {
        this.type = type;
        return this;
    }

    public List<Waypoint> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<Waypoint> waypoints) {
        this.waypoints = waypoints;
    }

    public List<Charges> getCharges() {
        return charges;
    }

    public void setCharges(List<Charges> charges) {
        this.charges = charges;
    }
}
