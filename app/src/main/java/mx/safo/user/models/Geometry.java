package mx.safo.user.models;

import android.location.Location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by karen on 03/11/17.
 */

public class Geometry implements Serializable {
    @SerializedName("location")
    @Expose
    private Coordinates location;

    public Coordinates getLocation() {
        return location;
    }

    public void setLocation(Coordinates location) {
        this.location = location;
    }
}
