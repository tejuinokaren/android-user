package mx.safo.user.models;

import android.content.ClipData;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by moyhdez on 22/02/17.
 */

public class PlaceLocal {
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;
    private String secondaryText = "";
    private String primaryText = "";
    private String placeID;
    private LatLng latLng;
    private int itemType;

    public PlaceLocal() {
    }

    public PlaceLocal(String secondaryText, String primaryText, String placeID) {
        this.secondaryText = secondaryText;
        this.primaryText = primaryText;
        this.placeID = placeID;
        this.itemType = TYPE_ITEM;
    }

    public PlaceLocal(String secondaryText, String primaryText, String placeID, LatLng latLng) {
        this.secondaryText = secondaryText;
        this.primaryText = primaryText;
        this.placeID = placeID;
        this.latLng = latLng;
        this.itemType = TYPE_ITEM;
    }

    public PlaceLocal placeLocalHeader() {
        this.itemType = TYPE_HEADER;
        return this;
    }

    public String getSecondaryText() {
        return secondaryText;
    }

    public void setSecondaryText(String secondaryText) {
        this.secondaryText = secondaryText;
    }

    public String getPrimaryText() {
        return primaryText;
    }

    public void setPrimaryText(String primaryText) {
        this.primaryText = primaryText;
    }

    public String getPlaceID() {
        return placeID;
    }

    public void setPlaceID(String placeID) {
        this.placeID = placeID;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }
}
