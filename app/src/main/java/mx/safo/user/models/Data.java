package mx.safo.user.models;

import android.graphics.Point;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Fabian on 31/08/17.
 */

public class Data implements Serializable{

    @SerializedName("vehicles")
    @Expose
    private List<Vehicle> vehicles = null;

    @SerializedName("vehicle")
    @Expose
    private Vehicle vehicle;

    @SerializedName("brands")
    @Expose
    private List<Brand> brands = null;

    @SerializedName("user")
    @Expose
    private User user = null;

    @SerializedName("cards")
    @Expose
    private List<CardM> cards = null;

    @SerializedName("card")
    @Expose
    private CardM card;

    @SerializedName("models")
    @Expose
    private List<Model> models = null;

    @SerializedName("coupons")
    @Expose
    private List<Coupon> coupons = null;

    @SerializedName("coupon")
    @Expose
    private Coupon coupon = null;

    @SerializedName("trips")
    @Expose
    private DataPage dataPage = null;

    @SerializedName("estimation")
    @Expose
    private Estimated mEstimated;

    @SerializedName("trip")
    @Expose
    private Trip trip;

    @SerializedName("request_id")
    @Expose
    private Integer requestID;

    @SerializedName("drivers")
    @Expose
    private JsonArray nearDrivers;
    @SerializedName("point")
    @Expose
    private TripPoint point;
    @SerializedName("destinations")
    @Expose
    public List<Destination> destinations = null;

    //<------ GETTERS & SETTERS ------------------------------------------------------------------->
    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<CardM> getCards() {
        return cards;
    }

    public void setCards(List<CardM> cards) {
        this.cards = cards;
    }

    public CardM getCard() {
        return card;
    }

    public void setCard(CardM card) {
        this.card = card;
    }

    public List<Model> getModels() {
        return models;
    }

    public void setModels(List<Model> models) {
        this.models = models;
    }

    public List<Coupon> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<Coupon> coupons) {
        this.coupons = coupons;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    public void setCoupon(Coupon coupon) {
        this.coupon = coupon;
    }

    public DataPage getDataPage() {
        return dataPage;
    }

    public void setDataPage(DataPage dataPage) {
        this.dataPage = dataPage;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public Estimated getEstimated() {
        return mEstimated;
    }

    public void setEstimated(Estimated estimated) {
        this.mEstimated = estimated;
    }

    public Integer getRequestID() {
        return requestID;
    }

    public void setRequestID(Integer requestID) {
        this.requestID = requestID;
    }

    public JsonArray getNearDrivers() {
        return nearDrivers;
    }

    public void setNearDrivers(JsonArray nearDrivers) {
        this.nearDrivers = nearDrivers;
    }

    public TripPoint getPoint() {
        return point;
    }

    public void setPoint(TripPoint point) {
        this.point = point;
    }

    public List<Destination> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Destination> destinations) {
        this.destinations = destinations;
    }
}
