package mx.safo.user.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by karen on 25/10/17.
 */

public class Driver implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("is_main")
    @Expose
    private Integer isMain;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("birth_date")
    @Expose
    private String birthDate;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("home_phone")
    @Expose
    private String homePhone;
    @SerializedName("position_lat")
    @Expose
    private String positionLat;
    @SerializedName("position_lng")
    @Expose
    private String positionLng;
    @SerializedName("prev_position_lat")
    @Expose
    private String prevPositionLat;
    @SerializedName("prev_position_lng")
    @Expose
    private String prevPositionLng;
    @SerializedName("position_updated_at")
    @Expose
    private String positionUpdatedAt;
    @SerializedName("position_updated_seconds")
    @Expose
    private Integer positionUpdatedSeconds;
    @SerializedName("connection_status")
    @Expose
    private String connectionStatus;
    @SerializedName("on_trip")
    @Expose
    private Integer onTrip;
    @SerializedName("platform")
    @Expose
    private String platform;
    @SerializedName("total_ratings")
    @Expose
    private Integer totalRatings;
    @SerializedName("score")
    @Expose
    private String score;
    @SerializedName("is_driver")
    @Expose
    private Integer isDriver;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIsMain() {
        return isMain;
    }

    public void setIsMain(Integer isMain) {
        this.isMain = isMain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getPositionLat() {
        return positionLat;
    }

    public void setPositionLat(String positionLat) {
        this.positionLat = positionLat;
    }

    public String getPositionLng() {
        return positionLng;
    }

    public void setPositionLng(String positionLng) {
        this.positionLng = positionLng;
    }

    public String getPrevPositionLat() {
        return prevPositionLat;
    }

    public void setPrevPositionLat(String prevPositionLat) {
        this.prevPositionLat = prevPositionLat;
    }

    public String getPrevPositionLng() {
        return prevPositionLng;
    }

    public void setPrevPositionLng(String prevPositionLng) {
        this.prevPositionLng = prevPositionLng;
    }

    public String getPositionUpdatedAt() {
        return positionUpdatedAt;
    }

    public void setPositionUpdatedAt(String positionUpdatedAt) {
        this.positionUpdatedAt = positionUpdatedAt;
    }

    public Integer getPositionUpdatedSeconds() {
        return positionUpdatedSeconds;
    }

    public void setPositionUpdatedSeconds(Integer positionUpdatedSeconds) {
        this.positionUpdatedSeconds = positionUpdatedSeconds;
    }

    public String getConnectionStatus() {
        return connectionStatus;
    }

    public void setConnectionStatus(String connectionStatus) {
        this.connectionStatus = connectionStatus;
    }

    public Integer getOnTrip() {
        return onTrip;
    }

    public void setOnTrip(Integer onTrip) {
        this.onTrip = onTrip;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Integer getTotalRatings() {
        return totalRatings;
    }

    public void setTotalRatings(Integer totalRatings) {
        this.totalRatings = totalRatings;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public Integer getIsDriver() {
        return isDriver;
    }

    public void setIsDriver(Integer isDriver) {
        this.isDriver = isDriver;
    }
}
