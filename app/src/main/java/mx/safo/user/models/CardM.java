package mx.safo.user.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import mx.safo.user.R;

/**
 * Created by Fabian on 05/09/17.
 */

public class CardM implements Serializable, Cloneable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("title")
    @Expose
    private String lastDigits;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @Override
    protected CardM clone() throws CloneNotSupportedException {
        CardM cloned = (CardM)super.clone();
        // the above is applicable in case of primitive member types,
        // however, in case of non primitive types
        // cloned.setNonPrimitiveType(cloned.getNonPrimitiveType().clone());
        return cloned;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLastDigits() {
        return "****".concat(lastDigits.substring(lastDigits.length() -4));
    }

    public void setLastDigits(String lastDigits) {
        this.lastDigits = lastDigits;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getImage() {
        switch (company) {
            case "VISA":
                return R.drawable.ic_visa;
            case "MASTERCARD":
                return R.drawable.ic_mastercard;
            case "AMERICAN EXPRESS":
                return R.drawable.ic_amex;
            default: return R.drawable.rectangle_grey;
        }
    }

}