package mx.safo.user.models;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by moyhdez on 08/05/17.
 */

public class DriverPosition {
    private LatLng position;
    private LatLng prevPosition;
    private Long distance;
    private int id;

    public DriverPosition(LatLng position) {
        this.position = position;
    }

    public DriverPosition(JSONObject json) throws JSONException {
        if(json.has("id"))
        id = json.getInt("id");
        position = new LatLng(json.getDouble("position_lat"), json.getDouble("position_lng"));
        prevPosition = new LatLng(json.getDouble("prev_position_lat"), json.getDouble("prev_position_lng"));
        if(json.has("distance"))
            distance = json.getLong("distance");
    }

    public DriverPosition(LatLng position, LatLng prevPosition, Long distance, int id) {
        this.position = position;
        this.prevPosition = prevPosition;
        this.distance = distance;
        this.id = id;
    }

    public static ArrayList<DriverPosition> getArray(JSONArray array) throws JSONException {
        ArrayList<DriverPosition> driverPositions = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            driverPositions.add(new DriverPosition(array.getJSONObject(i)));
        }

        return driverPositions;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public LatLng getPrevPosition() {
        return prevPosition;
    }

    public void setPrevPosition(LatLng prevPosition) {
        this.prevPosition = prevPosition;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
