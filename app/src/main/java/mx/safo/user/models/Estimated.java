package mx.safo.user.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tejuino on 7/09/17.
 */

public class Estimated implements Serializable {
    @SerializedName("estimated_distance")
    @Expose
    private String estimatedDistance;
    @SerializedName("estimated_time")
    @Expose
    private String estimatedTime;
    @SerializedName("estimated_polylines")
    @Expose
    private String estimatedPolylines;
    @SerializedName("estimated_total")
    @Expose
    private Float estimatedTotal;
    @SerializedName("driver_position_lat")
    @Expose
    private Float driverPositionLat;
    @SerializedName("driver_position_lng")
    @Expose
    private Float driverPositionLng;

    public String getEstimatedDistance() {
        return estimatedDistance;
    }

    public void setEstimatedDistance(String estimatedDistance) {
        this.estimatedDistance = estimatedDistance;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getEstimatedPolylines() {
        return estimatedPolylines;
    }

    public void setEstimatedPolylines(String estimatedPolylines) {
        this.estimatedPolylines = estimatedPolylines;
    }

    public Float getEstimatedTotal() {
        return estimatedTotal;
    }

    public void setEstimatedTotal(Float estimatedTotal) {
        this.estimatedTotal = estimatedTotal;
    }

    public Float getDriverPositionLat() {
        return driverPositionLat;
    }

    public void setDriverPositionLat(Float driverPositionLat) {
        this.driverPositionLat = driverPositionLat;
    }

    public Float getDriverPositionLng() {
        return driverPositionLng;
    }

    public void setDriverPositionLng(Float driverPositionLng) {
        this.driverPositionLng = driverPositionLng;
    }
}
