package mx.safo.user.models;

import java.util.Calendar;

/**
 * Created by moyhdez on 20/04/17.
 */

public class Schedule {
    private static Long id;
    private Calendar hour;
    private boolean isActive;
    private Long millis;
    private String textHour;

    public Schedule(Calendar hour, boolean isActive, String textHour) {
        this.hour = hour;
        this.isActive = isActive;
        this.textHour = textHour;
        this.millis = hour.getTimeInMillis() - System.currentTimeMillis();
        this.id = hour.getTimeInMillis();
    }


    public Long getId() {
        return id;
    }

    public void setId() {
        this.id = this.hour.getTimeInMillis();
    }

    public Calendar getHour() {
        return hour;
    }

    public void setHour(Calendar hour) {
        this.hour = hour;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Long getMillis() {
        return millis;
    }

    public void setMillis(Long millis) {
        this.millis = millis;
    }

    public String getTextHour() {
        return textHour;
    }

    public void setTextHour(String textHour) {
        this.textHour = textHour;
    }
}
