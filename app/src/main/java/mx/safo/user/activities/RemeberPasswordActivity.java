package mx.safo.user.activities;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.LinkedHashMap;
import java.util.Map;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.utils.FormValidator;
import mx.safo.user.utils.MyReceiver;

public class RemeberPasswordActivity extends AppCompatActivity implements View.OnClickListener{
    private Boolean sendEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remeber_password);
        hideKeyboard();
        sendEmail = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(),new IntentFilter("broadCastName"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(MyReceiver.getInstance());
    }

    //<------ API  -------------------------------------------------------------------------------->
    public void sendEmail(){
        if(validate()) {
            Map<String, String> params = new LinkedHashMap<>();
            params.put("email", getEditTextValue(R.id.et_email_remember));

            ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                    getString(R.string.forgot_pass, BuildConfig.API_URL),
                    ApiResponse.class,
                    Request.Method.POST,
                    ApiClient.Header.HEADER_EMPTY,
                    params,
                    new Response.Listener<ApiResponse>() {
                        @Override
                        public void onResponse(ApiResponse response) {
                            findViewById(R.id.til_conf_password_remember).setVisibility(View.VISIBLE);
                            findViewById(R.id.til_password_remember).setVisibility(View.VISIBLE);
                            findViewById(R.id.til_code_remember).setVisibility(View.VISIBLE);
                            YoYo.with(Techniques.FadeIn).duration(500).playOn(findViewById(R.id.tv_done_remember));
                            ((TextView) findViewById(R.id.tv_done_remember)).setText("Cambiar Contraseña");
                            sendEmail = false;
                            Toast.makeText(RemeberPasswordActivity.this, "Se ha enviado a tu correo el codigo de recuperacion", Toast.LENGTH_SHORT).show();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(!error.getMessage().isEmpty())
                                Toast.makeText(RemeberPasswordActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }, true
            ));
        }
    }

    public void changePass(){
        if(validate()) {
            Map<String, String> params = new LinkedHashMap<>();
            params.put("email", getEditTextValue(R.id.et_email_remember));
//            params.put("password", getEditTextValue(R.id.et_password_remember));
            params.put("new_password", getEditTextValue(R.id.et_conf_password_remember));
            params.put("recover_code", getEditTextValue(R.id.et_code_remeber));

            ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                    getString(R.string.recover_pass, BuildConfig.API_URL),
                    ApiResponse.class,
                    Request.Method.POST,
                    ApiClient.Header.HEADER_EMPTY,
                    params,
                    new Response.Listener<ApiResponse>() {
                        @Override
                        public void onResponse(ApiResponse response) {
                            Toast.makeText(RemeberPasswordActivity.this, "Contraseña cambiada", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(!error.getMessage().isEmpty())
                                Toast.makeText(RemeberPasswordActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }, true
            ));
        }
    }

    //<------ FUNCTIONS --------------------------------------------------------------------------->
    public Boolean validate() {
        EditText username = (EditText) findViewById(R.id.et_email_remember);

        FormValidator validator = new FormValidator(getResources());
        validator.addInput(username, FormValidator.INPUT_TYPE_EMAIL, true);

        if (!sendEmail) {
            EditText confPassword = (EditText) findViewById(R.id.et_conf_password_remember);
            EditText code = (EditText) findViewById(R.id.et_code_remeber);
            EditText password = (EditText) findViewById(R.id.et_password_remember);

            validator.addInput(password, FormValidator.INPUT_TYPE_ALL, true , 4, 20);
            validator.addInput(confPassword, FormValidator.INPUT_TYPE_ALL, true, 2, 20);
            validator.addInput(code, FormValidator.INPUT_TYPE_TEXT, true);

            if (!getEditTextValue(R.id.et_conf_password_remember).equals(getEditTextValue(R.id.et_password_remember))) {
                ((EditText) findViewById(R.id.et_conf_password_remember)).setError("Las contraseñas no coinciden");
                return false;
            }
        }

        return validator.isValid();
    }

    private String getEditTextValue(int id) {
        return ((EditText) findViewById(id)).getText().toString();
    }

    private void hideKeyboard(){
        findViewById(R.id.rl_content_remember).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                finish();
                return true;
            }
        });
    }

    //<------ ACTIONS ----------------------------------------------------------------------------->
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_done_remember:
                if(sendEmail)
                    sendEmail();
                else
                    changePass();
                break;
        }
    }
}
