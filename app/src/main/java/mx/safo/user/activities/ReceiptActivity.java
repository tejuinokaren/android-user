package mx.safo.user.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.Trip;
import mx.safo.user.utils.MyReceiver;
import mx.safo.user.utils.Repository;

import static mx.safo.user.activities.HomeMapActivity.sCurrentTrip;

public class ReceiptActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String EXTRA_TRIP = "sCurrentTrip";
    private RelativeLayout contentView;
    private LinearLayout llDataContent;
    private TextView tvTitle;
    private ArrayList<AppCompatImageView> stars;
    private int rate = 0;
    private Context mContext;

    public static Intent newIntent(Context packageContext, Trip trip) {
        Intent i = new Intent(packageContext, OrderDriverActivity.class);
        i.putExtra(EXTRA_TRIP, trip);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_receipt);
        super.onCreate(savedInstanceState);

        mContext = this;

        llDataContent = (LinearLayout) findViewById(R.id.ll_receipt_data_content);
        contentView = (RelativeLayout) findViewById(R.id.rl_content_receipt);
        tvTitle = (TextView) findViewById(R.id.tv_title_receipt);

        stars = new ArrayList<>();
        stars.add((AppCompatImageView) findViewById(R.id.star_1_receipt));
        stars.add((AppCompatImageView) findViewById(R.id.star_2_receipt));
        stars.add((AppCompatImageView) findViewById(R.id.star_3_receipt));
        stars.add((AppCompatImageView) findViewById(R.id.star_4_receipt));
        stars.add((AppCompatImageView) findViewById(R.id.star_5_receipt));
        for(AppCompatImageView star : stars) {
            star.setOnClickListener(this);
        }

        findViewById(R.id.iv_go_receipt).setOnClickListener(this);

        llDataContent.setAlpha(0);

        contentView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                contentView.getViewTreeObserver().removeOnPreDrawListener(this);
                YoYo.with(Techniques.BounceInDown).duration(800).playOn(tvTitle);
                YoYo.with(Techniques.BounceInUp).duration(1000).playOn(llDataContent);
                setValues();
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(), new IntentFilter("broadCastName"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(MyReceiver.getInstance());
    }

    //<------ INIT VIEWS -------------------------------------------------------------------------->
    private void setValues() {
        setTextViewValue(R.id.tv_driver_receipt, "Chofer: "
                .concat(sCurrentTrip.getDriver().getName().concat(" ").concat(sCurrentTrip.getDriver().getLastName())));
        setTextViewValue(R.id.tv_driver_vehicle_receipt, sCurrentTrip.getDriverVehicle().getModel().getTitle());
        setTextViewValue(R.id.tv_driver_plates_receipt, sCurrentTrip.getDriverVehicle().getPlates());
        Picasso.with(this).setLoggingEnabled(true);
        Picasso.with(this).load(sCurrentTrip.getDriver().getImage().replace("http://", "https://"))
                .fit().into(((ImageView) findViewById(R.id.iv_driver_pic_receipt)));


        Picasso.with(this).load(sCurrentTrip.getVehicle().getModel().getBrand().getImage()).into((ImageView) findViewById(R.id.iv_car_brand_receipt));
        setTextViewValue(R.id.tv_car_receipt, sCurrentTrip.getVehicle().getTitle());
        setTextViewValue(R.id.tv_plates_receipt, sCurrentTrip.getVehicle().getPlates());

        setTextViewValue(R.id.tv_origin_receipt, sCurrentTrip.getRequestOriginAddress());
        setTextViewValue(R.id.tv_destination_receipt, sCurrentTrip.getRequestDestinationAddress());
        setTextViewValue(R.id.tv_trip_ends_at, sCurrentTrip.getFinishedAt());
        setTextViewValue(R.id.tv_title_receipt, "Total:" + Repository.formatPriceToMXN(sCurrentTrip.getTotal()));


        if (sCurrentTrip.getCard().getImage() != null) {
            Picasso.with(this)
                    .load(sCurrentTrip.getCard().getImage())
                    .into((ImageView) findViewById(R.id.iv_card_receipt));
        }
        setTextViewValue(R.id.tv_card_receipt, sCurrentTrip.getCard().getLastDigits());
    }

    //<------ FUNCTIONS --------------------------------------------------------------------------->
    private void setTextViewValue(int id, String text) {
        ((TextView) findViewById(id)).setText(text);
    }

    private void setStars(final Integer starTapped) {
        for (int i = 0; i < stars.size(); i++) {
            stars.get(i).setImageResource(R.drawable.ic_starempty);
        }

        for (int i = 0; i < starTapped; i++) {
            stars.get(i).setImageResource(R.drawable.ic_starfull);
        }
    }

    //<------ API --------------------------------------------------------------------------------->
    private void rateTrip(int rate, int tripId) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("rating", String.valueOf(rate));
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.trip_rate, BuildConfig.API_URL, String.valueOf(tripId)),
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
//                        startActivity(new Intent(ReceiptActivity.this, HomeMapActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (!error.getMessage().isEmpty()) {
                            if(error.getMessage().contains("calificado")) {
                                finish();
                            } else {
                                Toast.makeText(ReceiptActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, true
        ));
    }

    //<------ ACTIONS ----------------------------------------------------------------------------->
    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.iv_go_receipt:
                if(rate != 0) {
                    rateTrip(rate, sCurrentTrip.getId());
                } else {
                    Toast.makeText(mContext, "Califica a tu chofer", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.star_1_receipt:
                setStars(1);
                rate = 1;
                break;
            case R.id.star_2_receipt:
                setStars(2);
                rate = 2;
                break;
            case R.id.star_3_receipt:
                setStars(3);
                rate = 3;
                break;
            case R.id.star_4_receipt:
                setStars(4);
                rate = 4;
                break;
            case R.id.star_5_receipt:
                setStars(5);
                rate = 5;
                break;
        }
    }
    @Override
    public void onBackPressed() {

    }
}
