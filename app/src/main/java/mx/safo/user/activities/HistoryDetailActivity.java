package mx.safo.user.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.adapters.ChargesRvAdapter;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.Charges;
import mx.safo.user.models.Trip;
import mx.safo.user.utils.MyReceiver;

public class HistoryDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private Trip history;
    private TextView tvDriverName;
    private TextView tvDriverCar;
    private TextView tvDriverPlates;
    private TextView tvDate;
    private TextView tvMyCar;
    private TextView tvMyCarPlates;
    private TextView tvOriginDescription;
    private TextView tvDestinationDescription;
    private TextView tvArriveTime;
//    private TextView tvCardDigits;
    private TextView tvTotal;
//    private ImageView ivCardBrand;
    private RecyclerView rvCharges;
    private LinearLayoutManager linearLayoutManager;
    private ChargesRvAdapter chargesRvAdapter;
    private RelativeLayout mDestinationContainer;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_history_detail);

        mContext = this;
        initViews();
        if(getIntent().hasExtra("History")){
            Bundle b = getIntent().getExtras();
            history = (Trip) (b.getSerializable("History"));
            fillDataHistory();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(),new IntentFilter("broadCastName"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(MyReceiver.getInstance());
    }

    //<------ INIT VIEWS -------------------------------------------------------------------------->
    public void initViews(){
        tvDriverName = (TextView)findViewById(R.id.tv_driver_history_details);
        tvDriverCar = (TextView)findViewById(R.id.tv_motor_driver_history_detail);
        tvDriverPlates = (TextView)findViewById(R.id.tv_plates_driver_history_detail);
        tvDate = (TextView)findViewById(R.id.tv_date_history_details);
        tvMyCar = (TextView)findViewById(R.id.tv_car_history_detail);
        tvMyCarPlates = (TextView)findViewById(R.id.tv_plates_history_detail);
        tvOriginDescription = (TextView)findViewById(R.id.tv_origin_history_details);
        tvDestinationDescription = (TextView)findViewById(R.id.tv_destination_history_details);
        tvArriveTime = (TextView)findViewById(R.id.tv_arrive_history_details);
//        tvCardDigits = (TextView)findViewById(R.id.tv_last_digits_history_detail);
        tvTotal = (TextView)findViewById(R.id.tv_total_history_details);
//        ivCardBrand = (ImageView)findViewById(R.id.iv_card_brand_history_detail);
        rvCharges = (RecyclerView) findViewById(R.id.rv_charges_history_detail);
        mDestinationContainer = (RelativeLayout) findViewById(R.id.rl_destination_history_detail);
        linearLayoutManager = new LinearLayoutManager(this);
    }

    private void fillDataHistory(){
        if(history.getDriver() != null) {
            tvDriverName.setText("Tu chofer fue: " + history.getDriver().getName().concat(" " + history.getDriver().getLastName()));
            tvDriverCar.setText(history.getDriverVehicle().getModel().getBrand().getTitle().concat(" " + history.getDriverVehicle().getModel().getTitle()));
            tvDriverPlates.setText(history.getDriverVehicle().getPlates());
            Picasso.with(HistoryDetailActivity.this).load(history.getDriver().getImage())
                    .error(R.drawable.profile_holder).into((ImageView)findViewById(R.id.iv_driver_history_detail));
            Picasso.with(HistoryDetailActivity.this).load(history.getVehicle().getModel().getBrand().getImage())
                    .error(R.drawable.profile_holder)
                    .into((ImageView)findViewById(R.id.iv_car_brand_history_detail));
        }
        if(history.getStartedAt() != null)
            tvDate.setText(history.getStartedAt());
        else  tvDate.setText(history.getRequestedAt());
        tvMyCar.setText(history.getVehicle().getTitle());
        tvMyCarPlates.setText(history.getVehicle().getPlates());
        tvOriginDescription.setText(history.getRequestOriginAddress());
        tvDestinationDescription.setText(history.getRequestDestinationAddress());
        if(history.getFinishedAt()!= null)
            tvArriveTime.setText(history.getFinishedAt());
        else tvArriveTime.setVisibility(View.INVISIBLE);
        tvTotal.setText("$" + String.valueOf(history.getTotal()) + " MXN");
        ImageView mapImage = (ImageView)findViewById(R.id.iv_map_history_detail);
        Picasso.with(HistoryDetailActivity.this)
                .load(history.getMapImage())
                .error(R.drawable.map).into(mapImage);
        mapImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(ZoomActivity.newIntent(mContext, history.getMapImage()));
            }
        });
        rvCharges.setLayoutManager(linearLayoutManager);
        rvCharges.setAdapter(new ChargesRvAdapter((ArrayList) history.getCharges(), this, history.getCard()));

        if(history.getDriver() == null) {
            findViewById(R.id.rl_driver_history_detail).setVisibility(View.GONE);
        }

        if(history.getCharges().isEmpty()) {
            findViewById(R.id.payment_method_wrapper).setVisibility(View.GONE);
        }

        if(history.getWaypoints().isEmpty()) {
            findViewById(R.id.rl_destination_history_detail).setEnabled(false);
            findViewById(R.id.right_icon).setVisibility(View.INVISIBLE);
        }
    }

    //<------ API --------------------------------------------------------------------------------->
    private void confirmDeleteDialog(){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setMessage("¿Desea eliminar este viaje de su Historial?")
                .setCancelable(false)
                .setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                deleteTrip();
                            }
                        });
        alertDialogBuilder
                .setNegativeButton("Cancelar", null);
        AlertDialog alertGPS = alertDialogBuilder.create();
        alertGPS.show();
    }

    private void deleteTrip(){

        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest(
                getString(R.string.history_delete, BuildConfig.API_URL, String.valueOf(history.getId())),
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_DEFAULT,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        Intent i = new Intent();
                        i.putExtra("position", getIntent().getIntExtra("position", 0));
                        setResult(RESULT_OK, i);
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().equals(""))
                            Toast.makeText(HistoryDetailActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    //<------ ACTIONS ----------------------------------------------------------------------------->
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.iv_back_history_detail:
                setResult(RESULT_CANCELED);
                onBackPressed();
                break;
            case R.id.btn_eliminar_history_details:
                confirmDeleteDialog();
                break;
            case R.id.rl_destination_history_detail:
                startActivity(HistoryDestinationsActivity.newIntent(mContext, history.getWaypoints()));
                break;

        }
    }
}
