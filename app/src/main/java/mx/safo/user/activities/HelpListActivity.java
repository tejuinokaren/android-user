package mx.safo.user.activities;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import mx.safo.user.R;
import mx.safo.user.utils.MyReceiver;

public class HelpListActivity extends AppCompatActivity implements  View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_list);
        ListView lvHelp = (ListView) findViewById(R.id.list_view_help);
        lvHelp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(HelpListActivity.this, HelpDetailsActivity.class);
                switch (position){
                    case 0:
                        i.putExtra("case", 0);
                        startActivity(i);
                        break;
                    case 1:
                        i.putExtra("case", 1);
                        startActivity(i);
                        break;
                    case 2:
                        i.putExtra("case", 2);
                        startActivity(i);
                        break;
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(), new IntentFilter("broadCastName"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(MyReceiver.getInstance());

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.iv_back_help_list:
                finish();
                break;

        }
    }
}
// TODO: terminar textos de ayuda