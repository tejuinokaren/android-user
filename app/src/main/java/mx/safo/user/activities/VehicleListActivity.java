package mx.safo.user.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.adapters.VehiclesRvAdapter;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.User;
import mx.safo.user.models.Vehicle;
import mx.safo.user.utils.MyReceiver;

public class VehicleListActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int NEW_VEHICLE = 434;
    private static final int EDIT_VEHICLE = 467;
    private static final String Tag = "VehicleListActivity";
    public static final String EXTRA_NEW_VEHICLE = "vehicleNew";

    private ArrayList<Vehicle> mVehicles;
    private RecyclerView rvVehicles;
    private LinearLayoutManager layoutManager;
    private Context mContext ;

    public static Intent newIntent(Context packageContext) {
        Intent i = new Intent(packageContext, VehicleListActivity.class);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_list);

        mContext = this;
        rvVehicles = (RecyclerView) findViewById(R.id.rv_vehicle_list);
        getVehicles();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(),new IntentFilter("broadCastName"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(MyReceiver.getInstance());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NEW_VEHICLE && resultCode == RESULT_OK) {
            if (mVehicles == null)
                mVehicles = new ArrayList<>();
            mVehicles.add((Vehicle) data.getExtras().getSerializable(VehicleListActivity.EXTRA_NEW_VEHICLE));
            setRv();
        }
        else if (requestCode == EDIT_VEHICLE && resultCode == RESULT_OK) {
            getVehicles();
        }
    }

    //<--------- FUNCTIONS ------------------------------------------------------------------------>
    private void setRv() {
        layoutManager = new LinearLayoutManager(this);

        VehiclesRvAdapter vehiclesRvAdapter = new VehiclesRvAdapter(mVehicles, this, true);
        rvVehicles.setAdapter(vehiclesRvAdapter);
        rvVehicles.setLayoutManager(layoutManager);
        ((SimpleItemAnimator) rvVehicles.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    //<--------- API ------------------------------------------------------------------------------>
    private void getVehicles() {
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest(
                getString(R.string.get_vehicles, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        ApiResponse res = (ApiResponse) response;
                        mVehicles = (ArrayList<Vehicle>) res.getData().getVehicles();
                        setRv();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().equals(""))
                        Toast.makeText(VehicleListActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    //<--------- ACTIONS -------------------------------------------------------------------------->
    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.iv_back_vehicle_list:
                finish();
                break;

            case R.id.iv_add_vehicle_list:
                startActivityForResult(VehicleDetailsActivity.newIntent(mContext, null), NEW_VEHICLE);
                break;
        }
    }
}
