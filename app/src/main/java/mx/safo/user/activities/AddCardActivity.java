package mx.safo.user.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import io.conekta.conektasdk.Card;
import io.conekta.conektasdk.Conekta;
import io.conekta.conektasdk.Token;
import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.CardM;
import mx.safo.user.models.User;
import mx.safo.user.utils.FormValidator;
import mx.safo.user.utils.MyReceiver;

import static mx.safo.user.utils.Repository.hideProgressDialog;
import static mx.safo.user.utils.Repository.showProgressDialog;

public class AddCardActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<StringDuo> listOfPattern;

    private ArrayList<Integer> years;
    private Integer selectedYear;
    private ArrayList<Integer> months;
    private Integer selectedMonth;
    private Boolean isAdded;
    private Context mContext;

    private static String TAG = AddCardActivity.class.getSimpleName();

    public static Intent newIntent(Context packageContext) {
        Intent i = new Intent(packageContext, AddCardActivity.class);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_add_card);

        mContext = this;

        listOfPattern = new ArrayList<>();
        listOfPattern.add(new StringDuo("^4[0-9]{6,}$", "VISA"));
        listOfPattern.add(new StringDuo("^5[1-5][0-9]{5,}$", "MASTERCARD"));
        listOfPattern.add(new StringDuo("^3[47][0-9]{5,}$", "AMERICAN EXPRESS"));
        isAdded = false;

        getEditText(R.id.et_card_number_add_card).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String ccNum = s.toString();
                boolean finded = false;
                for (StringDuo p : listOfPattern) {
                    if (ccNum.matches(p.getPattern())) {
                        setEditTextValue(R.id.et_company_add_card, p.getCompany());
                        finded = true;
                        break;
                    }
                }
                if (!finded)
                    setEditTextValue(R.id.et_company_add_card, "");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(),new IntentFilter("broadCastName"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(MyReceiver.getInstance());
    }

    //<------ API --------------------------------------------------------------------------------->
    private void uploadCard(String token) {
        Map<String, String> params = new LinkedHashMap<>();
        String title = getEditTextValue(R.id.et_card_number_add_card);
        title = title.substring(title.length() - 4, title.length());
        params.put("title", title);
        params.put("company", getEditTextValue(R.id.et_company_add_card));
        params.put("token", token);

        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.add_card, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        hideProgressDialog();
                        Intent i = new Intent();
                        CardM card = response.getData().getCard();
                        i.putExtra(CardListActivity.EXTRA_NEW_CARD, card);
                        AddCardActivity.this.setResult(RESULT_OK, i);
                        Toast.makeText(AddCardActivity.this, "La tarjeta se subió exitosamente", Toast.LENGTH_SHORT).show();

                        // Add this card to locally stored user
                        User user = User.fromSharedPref(mContext);
                        user.addCard(card);
                        SharedPreferences.Editor editor = mContext.getSharedPreferences("user", MODE_PRIVATE).edit();
                        editor.putString("user", new Gson().toJson(user));
                        editor.apply();

                        user = User.fromSharedPref(mContext);

                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(AddCardActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, false
        ));
    }

    //<------ FUNCTIONS --------------------------------------------------------------------------->
    private void setType(){

        final String[] data = new String[2];
        data[0] = "Débito";
        data[1] = "Crédito";

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(data, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String i = data[which];
                setEditTextValue(R.id.et_type_add_card, i);
                getEditText(R.id.et_type_add_card).setError(null);
            }
        });
        builder.create().show();
    }

    private void selectMonth() {
        if (months == null) {
            setMonths();
        }

        final String[] data = new String[months.size()];
        for (int i = 0; i < data.length; i++) {
            data[i] = String.format("%02d", months.get(i));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(data, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                selectedMonth = months.get(which);
                setEditTextValue(R.id.et_month_add_card, data[which]);
                getEditText(R.id.et_month_add_card).setError(null);
            }
        });
        builder.create().show();

    }

    private void setMonths() {
        months = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            months.add(i);
        }
    }

    private void selectYear() {
        if (years == null) {
            setYears();
        }

        final String[] data = new String[years.size()];
        for (int i = 0; i < data.length; i++) {
            data[i] = years.get(i).toString();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(data, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                selectedYear = years.get(which);
                setEditTextValue(R.id.et_year_add_card, selectedYear.toString());
                getEditText(R.id.et_year_add_card).setError(null);
            }
        });
        builder.create().show();
    }

    private void setYears() {
        years = new ArrayList<>();
        for (int i = Calendar.getInstance().get(Calendar.YEAR); i <= (Calendar.getInstance().get(Calendar.YEAR) + 10); i++) {
            years.add(i);
        }
    }

    private void setEditTextValue(int id, String text) {
        ((EditText) findViewById(id)).setText(text);
    }

    private String getEditTextValue(int id) {
        return ((EditText) findViewById(id)).getText().toString();
    }

    private EditText getEditText(int id) {
        return (EditText) findViewById(id);
    }

    private void submit() {

        FormValidator validator = new FormValidator(getResources());

        validator.addInput(getEditText(R.id.et_owner_add_card), FormValidator.INPUT_TYPE_TEXT, true, 2, 50);
        validator.addInput(getEditText(R.id.et_card_number_add_card), FormValidator.INPUT_TYPE_TEXT, true);
        validator.addInput(getEditText(R.id.et_company_add_card), FormValidator.INPUT_TYPE_TEXT, true);
        validator.addInput(getEditText(R.id.et_cvv_add_card), FormValidator.INPUT_TYPE_NUMBER, true, 3, 4);
        validator.addInput(getEditText(R.id.et_month_add_card), FormValidator.INPUT_TYPE_TEXT, true);
        validator.addInput(getEditText(R.id.et_year_add_card), FormValidator.INPUT_TYPE_TEXT, true);
        validator.addInput(getEditText(R.id.et_type_add_card), FormValidator.INPUT_TYPE_TEXT, true);

        if (validator.isValid()) submitCard();
    }

    private void submitCard() {
        showProgressDialog(mContext);
        Conekta.setPublicKey(BuildConfig.CONEKTA_PUBLIC_KEY); //Set public key
        Conekta.setApiVersion("0.3.0"); //Set api version (optional)
        Conekta.collectDevice(this); //Collect device

        Card card = new Card(getEditTextValue(R.id.et_owner_add_card), getEditTextValue(R.id.et_card_number_add_card),
                getEditTextValue(R.id.et_cvv_add_card), getEditTextValue(R.id.et_month_add_card),
                getEditTextValue(R.id.et_year_add_card));

        Token token = new Token(this);

        token.onCreateTokenListener(new Token.CreateToken() {
            @Override
            public void onCreateTokenReady(JSONObject data) {
                try {
                    Log.e(TAG, data.toString());
                    if (data.getString("object").equals("token")) {
                        uploadCard(data.getString("id"));
                    } else {
                        Toast.makeText(getBaseContext(), data.getString("message_to_purchaser"),
                                Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }
                } catch (Exception err) {
                    hideProgressDialog();
                    Log.e(TAG, err.toString());
                    Toast.makeText(getBaseContext(), "Error: " + err.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        token.create(card);
    }

    //<------ ACTIONS ----------------------------------------------------------------------------->
    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btn_done_add_card:
                submit();
                break;
            case R.id.iv_back_add_card:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.et_month_add_card:
                selectMonth();
                break;
            case R.id.et_year_add_card:
                selectYear();
                break;
            case R.id.et_type_add_card:
                setType();
        }
    }

    private class StringDuo {
        private String pattern;
        private String company;

        StringDuo(String pattern, String company) {
            this.pattern = pattern;
            this.company = company;
        }

        String getPattern() {
            return pattern;
        }

        String getCompany() {
            return company;
        }

    }
}
