package mx.safo.user.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResolvingResultCallbacks;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.adapters.CardsRvAdapter;
import mx.safo.user.adapters.SearchRvAdapter;
import mx.safo.user.adapters.VehiclesRvAdapter;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.CardM;
import mx.safo.user.models.Estimated;
import mx.safo.user.models.PlaceDetailsRes;
import mx.safo.user.models.PlaceLocal;
import mx.safo.user.models.User;
import mx.safo.user.models.Vehicle;
import mx.safo.user.utils.MyReceiver;
import mx.safo.user.utils.Repository;

import static mx.safo.user.activities.HomeMapActivity.sCurrentTrip;
import static mx.safo.user.activities.HomeMapActivity.sDestination;
import static mx.safo.user.activities.HomeMapActivity.sGetDriverETA;
import static mx.safo.user.activities.HomeMapActivity.sGetNearDrivers;
import static mx.safo.user.activities.HomeMapActivity.sGetTripLocation;
import static mx.safo.user.activities.HomeMapActivity.sHashMapMakers;
import static mx.safo.user.activities.HomeMapActivity.sOrigin;
import static mx.safo.user.utils.Repository.hideProgressDialog;
import static mx.safo.user.utils.UserLocationManager.GOOGLE_DIRECTIONS_KEY;
import static mx.safo.user.utils.UserLocationManager.sUserLocation;

public class OrderDriverActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = "OderDriverActivity";
    private static final String EXTRA_TRIP = "sCurrentTrip";
    private static final String EXTRA_RATE = "rate";
    private static final int REQUEST_ADD_VEHICLE = 1;
    private static final int REQUEST_ADD_CARD = 2;

    //VIEWS
    private RelativeLayout rlConfOrder;
    private RelativeLayout rlDriverOHW;
    private RelativeLayout rlDriver;
    private TextView tvCancelBack;
    private TextView tvPriceOrderDriver;
    private TextView tvSelectCarTitle;
    private TextView tvSelectCardTitle;
    private TextView tvTripOnCourseTitle;
    private LinearLayout llBack;
    private LinearLayout content;
    private RecyclerView rvCards;
    private RecyclerView rvCars;
    private List<Vehicle> mVehicleList = new ArrayList<>();
    private List<CardM> mCardsList = new ArrayList<>();;

    private Estimated mEstimated;

    private boolean isCarsExpanded = false;
    private boolean isCardsExpanded = false;
    private boolean isDialogShowing = false;

    private Timer chronometer;
    private ProgressDialog progressDialog;

    public screens currentScreen;
    private ImageButton mChangeRouteBtn;
    private Dialog searchDialog;
    private EditText etSearchDestination;
    private RecyclerView searchRV;
    private ArrayList<PlaceLocal> searchPlaces;
    private GoogleApiClient mGoogleApiClient;
    private LatLngBounds bounds;
    private AutocompleteFilter filter;
    private Timer timerAuxsearch;
    private LocationRequest mLocationRequest;
    private Context mContext;
    private VehiclesRvAdapter mVehiclesRvAdapter;
    private CardsRvAdapter mCardsRvAdapter;
    private AlertDialog mNoVehiclesAlertDialog, mNoCardsAlertDialog;
    private User mUser;

    public enum screens {
        confirm,
        selectCar,
        selectCard,
        driverOHW,
        requested,
        tripOnCourse,
        user_cancel_request
    }

    public static Intent newIntent(Context packageContext, Estimated estimated) {
        Intent i = new Intent(packageContext, OrderDriverActivity.class);
        i.putExtra(EXTRA_RATE, estimated);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_order_driver);

        mContext = this;

        tvTripOnCourseTitle = (TextView) findViewById(R.id.tv_trip_on_course_order_driver);
        rlDriverOHW = (RelativeLayout) findViewById(R.id.rl_driver_ohw_order_driver);
        tvSelectCarTitle = (TextView) findViewById(R.id.tv_select_car_order_driver);
        tvSelectCardTitle = (TextView) findViewById(R.id.tv_select_card_order_driver);
        tvCancelBack = (TextView) findViewById(R.id.tv_cancel_back_order_driver);
        tvPriceOrderDriver= (TextView) findViewById(R.id.tv_price_order_driver);
        rlConfOrder = (RelativeLayout) findViewById(R.id.rl_conf_order_driver);
        rlDriver = (RelativeLayout) findViewById(R.id.rl_driver_order_driver);
        content = (LinearLayout) findViewById(R.id.ll_content_order_driver);
        llBack = (LinearLayout) findViewById(R.id.ll_back_order_driver);
        rvCars = (RecyclerView) findViewById(R.id.rv_cars_order_driver);
        rvCards = (RecyclerView) findViewById(R.id.rv_cards_order_driver);
        currentScreen = screens.confirm;
        content.setAlpha(0);
        content.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                content.setVisibility(View.VISIBLE);
                return true;
            }
        });
        tvCancelBack.setVisibility(View.GONE);
        rlDriverOHW.setVisibility(View.GONE);
        rlDriver.setVisibility(View.GONE);

        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

        findViewById(R.id.iv_driver_order_driver).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        findViewById(R.id.rl_bigger_order_driver).setVisibility(View.VISIBLE);
                        return true;
                    case MotionEvent.ACTION_UP:
                        findViewById(R.id.rl_bigger_order_driver).setVisibility(View.GONE);
                        return true;
                    case MotionEvent.ACTION_CANCEL:
                        return true;
                }
                return false;
            }
        });

        mChangeRouteBtn = (ImageButton) findViewById(R.id.btn_change_route);
        mChangeRouteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSearch();
            }
        });

        mUser = User.fromSharedPref(this);
        initMapUtils();
        setSearchDialog();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(), new IntentFilter("broadCastName"));
        Bundle b = getIntent().getExtras();
        if (getIntent().hasExtra(EXTRA_RATE)) {
            mEstimated = (Estimated) b.getSerializable(EXTRA_RATE);
            setTvs();
        } else if (sCurrentTrip != null){
            checkTripStatus(sCurrentTrip.getStatus());
        }
        getTrip();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(MyReceiver.getInstance());
    }

    @Override
    protected void onStop() {
        super.onStop();
        searchDialog.dismiss();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideProgressDialog();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        createLocationRequest();
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(final Location location) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(getSharedPreferences("user", MODE_PRIVATE).getBoolean("isLogged", false)) {
                    sUserLocation = location;
                    Log.v("HomeMap", "Location Update");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == REQUEST_ADD_VEHICLE) {
            mVehicleList.add((Vehicle) data.getSerializableExtra(VehicleListActivity.EXTRA_NEW_VEHICLE));
        } else if(resultCode == RESULT_OK && requestCode == REQUEST_ADD_CARD) {
            mCardsList.add((CardM) data.getSerializableExtra(CardListActivity.EXTRA_NEW_CARD));
        }
    }

    //<------ INIT VIEWS -------------------------------------------------------------------------->
    private void setTvs() {
        setTextViewValue(R.id.tv_origin_order_driver, sOrigin.getPrimaryText());
        if (sDestination != null) {
            setTextViewValue(R.id.tv_destination_order_driver, sDestination.getPrimaryText() + " " + sDestination.getSecondaryText());
            setTextViewValue(R.id.tv_destination_order_time, mEstimated.getEstimatedTime());
            if(mEstimated.getEstimatedTotal() != null) {
                tvPriceOrderDriver.setText(Repository.formatPriceToMXN(mEstimated.getEstimatedTotal()));
            }
            setTextViewValue(R.id.tv_eta_order_driver, sCurrentTrip.getEstimatedTime());
        } else {
            setTextViewValue(R.id.tv_destination_order_driver, "Sin destino");
            tvPriceOrderDriver.setText("");
            setTextViewValue(R.id.tv_eta_order_driver, "");
        }

        setVehiclesRv();
        setCardsRv();
    }

    private void setVehiclesRv() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };

        mVehicleList = mUser.getVehicles();
        mVehiclesRvAdapter = new VehiclesRvAdapter(mVehicleList, this, false);
        rvCars.setAdapter(mVehiclesRvAdapter);
        rvCars.setLayoutManager(layoutManager);
        getVehicles();
    }

    private void setTextViewValue(int id, String text) {
        ((TextView) findViewById(id)).setText(text);
    }

    private void setCardsRv() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };

        mCardsList = mUser.getCards();
        mCardsRvAdapter = new CardsRvAdapter(mCardsList, this, false);
        rvCards.setAdapter(mCardsRvAdapter);
        rvCards.setLayoutManager(layoutManager);
        getCards();
    }

    private void setRvCarsSize() {
        int height = convertDpToPixel(isCarsExpanded ? 60 : 60 * mVehicleList.size(), this);
        final ViewGroup.LayoutParams params = rvCars.getLayoutParams();
        params.height = height;
        rvCars.setLayoutParams(params);
        rvCars.requestLayout();
        isCarsExpanded = !isCarsExpanded;
        if (isCarsExpanded) {
            currentScreen = screens.selectCar;
            YoYo.with(Techniques.BounceInDown).duration(400).playOn(tvSelectCarTitle);
        } else {
            currentScreen = screens.confirm;
            YoYo.with(Techniques.SlideOutUp).duration(400).playOn(tvSelectCarTitle);
        }
    }

    private void setRvCardsSize() {
        int height = convertDpToPixel(isCardsExpanded ? 60 : 60 * mCardsList.size(), this);
        final ViewGroup.LayoutParams params = rvCards.getLayoutParams();
        params.height = height;
        rvCards.setLayoutParams(params);
        rvCards.requestLayout();
        isCardsExpanded = !isCardsExpanded;
        if (isCardsExpanded) {
            currentScreen = screens.selectCard;
            YoYo.with(Techniques.BounceInDown).duration(400).playOn(tvSelectCardTitle);
        } else {
            currentScreen = screens.confirm;
            YoYo.with(Techniques.SlideOutUp).duration(400).playOn(tvSelectCardTitle);
        }
    }

    private void setAllViews(){
        try {
            setTextViewValue(R.id.tv_eta_order_driver, sCurrentTrip.getEstimatedTime());
            setTextViewValue(R.id.tv_driver_name_order_driver, "Chofer: ".concat(sCurrentTrip.getDriver().getName().concat(" ").concat(sCurrentTrip.getDriver().getLastName())));
            setTextViewValue(R.id.tv_motor_driver_order_driver, sCurrentTrip.getDriverVehicle().getModel().getBrand().getTitle().concat(" " + sCurrentTrip.getDriverVehicle().getModel().getTitle()));
            setTextViewValue(R.id.tv_plates_driver_order_driver, sCurrentTrip.getDriverVehicle().getPlates());
            setTextViewValue(R.id.tv_origin_order_driver, sCurrentTrip.getRequestOriginAddress());
            setTextViewValue(R.id.tv_destination_order_driver, sCurrentTrip.getRequestDestinationAddress());
            setTextViewValue(R.id.tv_destination_order_time, sCurrentTrip.getEstimatedTime());

            Picasso.with(OrderDriverActivity.this).setLoggingEnabled(true);
            Picasso.with(OrderDriverActivity.this).load(sCurrentTrip.getDriver().getImage().replace("http://", "https://")).fit().into(((ImageView) OrderDriverActivity.this.findViewById(R.id.iv_driver_order_driver)));
            Picasso.with(OrderDriverActivity.this).load(sCurrentTrip.getDriver().getImage().replace("http://", "https://")).fit().into(((ImageView) OrderDriverActivity.this.findViewById(R.id.iv_bigger_driver_order_driver)));

            mCardsList.add(sCurrentTrip.getCard());
            setCardsRv();
            mVehicleList = new ArrayList<>();
            mVehicleList.add(sCurrentTrip.getVehicle());
            setVehiclesRv();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void initMapUtils() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        filter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE).build();
        bounds = new LatLngBounds(new LatLng(20.6322588, -103.4638812), new LatLng(20.6420844, -103.4083913));
    }

    private void openSearch() {
        searchDialog.show();
        sDestination = null;
        etSearchDestination.setText(null);
    }

    private void setRvSearch() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(OrderDriverActivity.this);
        SearchRvAdapter adapter = new SearchRvAdapter(searchPlaces, this);
        searchRV.setLayoutManager(layoutManager);
        searchRV.setAdapter(adapter);
        searchRV.setHasFixedSize(true);
    }

    private void setSearchDialog() {
        searchDialog = new Dialog(OrderDriverActivity.this);
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.dialog_change_route);
        searchDialog.findViewById(R.id.iv_close_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeSearch();
            }
        });

        searchRV = (RecyclerView) searchDialog.findViewById(R.id.rv_search);
        etSearchDestination = (EditText) searchDialog.findViewById(R.id.et_destination_search);
        etSearchDestination.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (timerAuxsearch != null) {
                    timerAuxsearch.cancel();
                }

                timerAuxsearch = new Timer();
                timerAuxsearch.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        autocomplete(etSearchDestination.getText().toString(), false);
                    }
                }, 500);
            }
        });

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        searchDialog.getWindow().setLayout((int) (metrics.widthPixels * 0.99), (int) (metrics.heightPixels * 0.9));
    }

    private void autocomplete(String query, final boolean shouldSelectFirst) {
        final PendingResult<AutocompletePredictionBuffer> result = Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, query, bounds, filter);
        result.setResultCallback(new ResolvingResultCallbacks<AutocompletePredictionBuffer>(OrderDriverActivity.this, 1) {
            @Override
            public void onSuccess(@NonNull AutocompletePredictionBuffer autocompletePredictions) {
                if (autocompletePredictions.getStatus().isSuccess()) {
                    searchPlaces = new ArrayList<>();
                    for (int i = 0; i < autocompletePredictions.getCount(); i++) {
                        AutocompletePrediction acPrediction = autocompletePredictions.get(i);
                        searchPlaces.add(
                                new PlaceLocal(
                                        acPrediction.getSecondaryText(null).toString(),
                                        acPrediction.getPrimaryText(null).toString(),
                                        acPrediction.getPlaceId()));
                    }
                    if (shouldSelectFirst && !searchPlaces.isEmpty()) {
                        placeSelected(0);
                    }
                    setRvSearch();
                }
                autocompletePredictions.release();
            }

            @Override
            public void onUnresolvableFailure(@NonNull Status status) {
                Log.v("Auto Complete", "UnresolvableFailure");
            }
        });
    }

    private void closeSearch() {
        if(sDestination != null) {
            getPlaceDetails(sDestination.getPlaceID());
        }
    }

    public void placeSelected(int position) {
        sDestination = searchPlaces.get(position);
        etSearchDestination.setText(sDestination.getPrimaryText());
        closeSearch();

    }

    private void getPlaceDetails(String placeId) {
        String parameters = "placeid=".concat(placeId).concat("&key=").concat(GOOGLE_DIRECTIONS_KEY);
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                "https://maps.googleapis.com/maps/api/place/details/json?".concat(parameters.replace(" ", "%20")),
                PlaceDetailsRes.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_EMPTY,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        Log.d(TAG, response.toString());
                        PlaceDetailsRes res = (PlaceDetailsRes) response;

                        Log.d("lat/lng", res.result.getGeometry().getLocation().getLat().toString()
                                + ","
                                + res.result.getGeometry().getLocation().getLng().toString());
                        changeRoute(res.result.getFormattedAddress(), res.result.getGeometry().getLocation().getLat(), res.result.getGeometry().getLocation().getLng());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    //<------ API --------------------------------------------------------------------------------->
    public void getTrip() {
        Map<String, String> params = new LinkedHashMap<>();
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.trip_get_last, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        sCurrentTrip = response.getData().getTrip();
                        checkTripStatus(sCurrentTrip.getStatus());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(OrderDriverActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, false
        ));
    }

    public void checkTripStatus(String status){
        switch (status){
            /*solicitado
            * iniciado
            * aceptado
            * finalizado
            * cancelado*/
            case "Solicitado":
                sGetNearDrivers = true;
                sGetDriverETA = false;
                sGetTripLocation = false;
                if(!isDialogShowing) {
                    progressDialog = createProgressDialog(OrderDriverActivity.this);
                    progressDialog.show();
                }
                isDialogShowing = true;
                setTextViewValue(R.id.tv_origin_order_driver, sCurrentTrip.getRequestOriginAddress());
                setTextViewValue(R.id.tv_destination_order_driver, sCurrentTrip.getRequestDestinationAddress());
                mVehicleList = new ArrayList<>();
                mVehicleList.add(sCurrentTrip.getVehicle());
                setVehiclesRv();
                mCardsList.add(sCurrentTrip.getCard());
                setCardsRv();
                sHashMapMakers.clear();
                break;
            case "Aceptado":
                sGetNearDrivers = false;
                sGetDriverETA = true;
                sGetTripLocation = false;
                if(isDialogShowing)
                    dismissProgressDialog();
                setAllViews();
                driverOHW();
                break;
            case "Iniciado":
                sGetNearDrivers = false;
                sGetDriverETA = false;
                sGetTripLocation = true;
                if(isDialogShowing)
                    dismissProgressDialog();
                setAllViews();
                tripOnCourse();
                break;
            case "Finalizado":
                sGetNearDrivers = true;
                sGetDriverETA = false;
                sGetTripLocation = false;
                if(isDialogShowing) {
                    dismissProgressDialog();
                }
                if(sCurrentTrip.getRating() == 0) {
                    tripCompleted();
                }
                break;
            case "Cancelado": // From push
                sGetNearDrivers = true;
                sGetDriverETA = false;
                sGetTripLocation = false;
                if(isDialogShowing)
                    dismissProgressDialog();
                if(sCurrentTrip.getSeen() == 0) {
                    setTextViewValue(R.id.tv_origin_order_driver, sCurrentTrip.getOriginAddress());
                    setTextViewValue(R.id.tv_destination_order_driver, sCurrentTrip.getDestinationAddress());
                    mVehicleList = new ArrayList<>();
                    mVehicleList.add(sCurrentTrip.getVehicle());
                    setVehiclesRv();
                    mCardsList.add(sCurrentTrip.getCard());
                    setCardsRv();
                    driverCanceledTrip();
                }
                break;
            /*case "request_timeout":
                if(isDialogShowing)
                    dismissProgressDialog();
                if(sCurrentTrip.getSeen() == 0) {
                    setTextViewValue(R.id.tv_origin_order_driver, sCurrentTrip.getRequest().getOriginDescription());
                    setTextViewValue(R.id.tv_destination_order_driver, sCurrentTrip.getRequest().getDestinationDescription());
                    mVehicleList = new ArrayList<>();
                    mVehicleList.add(sCurrentTrip.getRequest().getVehicle());
                    setVehiclesRv();
                    mCardsList.add(sCurrentTrip.getRequest().getCard());
                    setCardsRv();
                    requesTimeout();
                }
                break;*/
        }
    }

    private void requestSafo() {
        if (mVehicleList.isEmpty()) {
           noVehiclesDialog();
        } else if (mCardsList.isEmpty()) {
           noCardsDialog();
        } else {
            Map<String, String> params = new LinkedHashMap<>();
            params.put("vehicle_id", String.valueOf(mVehicleList.get(0).getId()));
            params.put("card_id", String.valueOf(mCardsList.get(0).getId()));
            params.put("origin_address", sOrigin.getPrimaryText());
            params.put("origin_lat", String.valueOf(sOrigin.getLatLng().latitude));
            params.put("origin_lng", String.valueOf(sOrigin.getLatLng().longitude));
            params.put("destination_address", sDestination == null ? "Sin destino"
                    : sDestination.getPrimaryText().concat(" "));
            try {
                params.put("destination_lat", sDestination == null ? "0" : String.valueOf(sDestination.getLatLng().latitude));
                params.put("destination_lng", sDestination == null ? "0" : String.valueOf(sDestination.getLatLng().longitude));
            } catch (NullPointerException e) {
                Log.e("requestSafo", "sDestination lat/lng is null");
                return;
            }
            Log.d("requestSafo", "params " + params.toString());

            ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                    getString(R.string.request_create, BuildConfig.API_URL),
                    ApiResponse.class,
                    Request.Method.POST,
                    ApiClient.Header.HEADER_DEFAULT,
                    params,
                    new Response.Listener<ApiResponse>() {
                        @Override
                        public void onResponse(ApiResponse response) {
                            sCurrentTrip = response.getData().getTrip();
                            currentScreen = screens.requested;
                            progressDialog = createProgressDialog(OrderDriverActivity.this);
                            progressDialog.show();
                            isDialogShowing = true;
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(!error.getMessage().isEmpty())
                                Toast.makeText(OrderDriverActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }, true
            ));
        }
    }

    public void changeRoute(final String formattedAddress, final Float destLat, final Float destLng) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(sUserLocation != null) {
                    final  Map<String, String> params = new LinkedHashMap<>();
                    params.put("origin_lat", String.valueOf(sUserLocation.getLatitude()));
                    params.put("origin_lng", String.valueOf(sUserLocation.getLongitude()));
                    params.put("origin_address", sOrigin.getPrimaryText());
                    params.put("destination_address", formattedAddress);
                    params.put("destination_lat", String.valueOf(destLat));
                    params.put("destination_lng", String.valueOf(destLng));

                    ApiClient.getInstance(mContext).addToRequestQueue(new ApiClient.GsonRequest<>(
                            getString(R.string.trip_change_route, BuildConfig.API_URL, sCurrentTrip.getId().toString()),
                            ApiResponse.class,
                            Request.Method.POST,
                            ApiClient.Header.HEADER_DEFAULT,
                            params,
                            new Response.Listener<ApiResponse>() {
                                @Override
                                public void onResponse(ApiResponse response) {
                                    Log.d("changedDestination",
                                            "orign lat,lng: " + String.valueOf(sUserLocation.getLatitude()) + "," + String.valueOf(sUserLocation.getLongitude())
                                                    + " destination lat,lng: " + destLat.toString() + "," + destLng.toString());
                                    searchDialog.dismiss();
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if(!error.getMessage().isEmpty())
                                        Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }, true
                    ));
                }
            }
        });
    }

    private void getCards() {

        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.get_cards, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                null,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        mCardsList.clear();
                        mCardsList.addAll((ArrayList) response.getData().getCards());
                        if (mCardsList.isEmpty()) {
                            noCardsDialog();
                        }

                        mUser.setCards(mCardsList);
                        SharedPreferences.Editor editor = getSharedPreferences("user", MODE_PRIVATE).edit();
                        editor.putString("user", new Gson().toJson(mUser));
                        editor.putBoolean("isLogged", true);
                        editor.apply();

                        mCardsRvAdapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, false
        ));
    }

    private void getVehicles() {

        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.get_vehicles, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                null,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        mVehicleList.clear();
                        mVehicleList.addAll((ArrayList) response.getData().getVehicles());
                        if (mVehicleList.isEmpty()) {
                            noVehiclesDialog();
                        }

                        mUser.setVehicles(mVehicleList);
                        SharedPreferences.Editor editor = getSharedPreferences("user", MODE_PRIVATE).edit();
                        editor.putString("user", new Gson().toJson(mUser));
                        editor.putBoolean("isLogged", true);
                        editor.apply();

                        mVehiclesRvAdapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(OrderDriverActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, false
        ));
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    protected void startLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            //this==>(com.google.android.gms.location.LocationListener) not android.app.LocationListener
            Log.d(TAG, "Location update started ..............: ");
        } catch (SecurityException e) {
            Log.e("PERMISSION_EXCEPTION", "PERMISSION_NOT_GRANTED");
        }
    }

    /*private void updateTripStatus(Integer tripId, String status){
        Map<String, String> params = new LinkedHashMap<>();
        params.put("trip_id", tripId.toString());
        params.put("status", status);

        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.trip_cancel_request, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        currentScreen = screens.confirm;
                        tvCancelBack.setVisibility(View.GONE);
                        rlDriverOHW.setVisibility(View.GONE);
                        rlDriver.setVisibility(View.GONE);
                        rlConfOrder.setVisibility(View.VISIBLE);
                        back();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(OrderDriverActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }*/

    //<------ FUNCTIONS --------------------------------------------------------------------------->
    public void driverOHW() {
        findViewById(R.id.ll_see_map).setVisibility(View.VISIBLE);
        findViewById(R.id.iv_call_see_map).setVisibility(View.VISIBLE);
        if (isCarsExpanded) {
            setRvCarsSize();
        }
        rlDriverOHW.setVisibility(View.VISIBLE);
        tvCancelBack.setVisibility(View.VISIBLE);
        rlDriver.setVisibility(View.VISIBLE);
        rlConfOrder.setVisibility(View.GONE);
        currentScreen = screens.driverOHW;

        if (chronometer != null) {
            chronometer.cancel();
        }
    }

    public void tripOnCourse() {
        // TODO: Now seeing map on Course is a requirement
        findViewById(R.id.ll_see_map).setVisibility(View.VISIBLE);
        findViewById(R.id.iv_call_see_map).setVisibility(View.INVISIBLE);

        if (isCarsExpanded)
            setRvCarsSize();
        tvCancelBack.setVisibility(View.VISIBLE);
        rlDriver.setVisibility(View.VISIBLE);
        rlConfOrder.setVisibility(View.GONE);
        YoYo.with(Techniques.BounceInDown).duration(400).playOn(tvTripOnCourseTitle);
        llBack.setVisibility(View.INVISIBLE);
        rlDriverOHW.setVisibility(View.GONE);
        mChangeRouteBtn.setVisibility(View.VISIBLE);
        currentScreen = screens.tripOnCourse;

        if (chronometer != null) {
            chronometer.cancel();
        }

        chronometer = new Timer();
        chronometer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(sCurrentTrip.getStatus().equalsIgnoreCase("Iniciado")) {
                            final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date date = null;
                            try {
                                date = dateFormat.parse(sCurrentTrip.getStartedAt());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            } catch (java.text.ParseException e) {
                                e.printStackTrace();
                            }
                            Calendar startsAt = Calendar.getInstance();
                            startsAt.setTime(date);
                            long time = Calendar.getInstance().getTimeInMillis() - startsAt.getTimeInMillis();
                            final String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(time),
                                    TimeUnit.MILLISECONDS.toMinutes(time) % TimeUnit.HOURS.toMinutes(1),
                                    TimeUnit.MILLISECONDS.toSeconds(time) % TimeUnit.MINUTES.toSeconds(1));
                            tvTripOnCourseTitle.setText("Tu viaje: ".concat(hms));
                        }

                    }
                });
            }
        }, 0, 1000);

    }

    public void driverCanceledTrip(){

        new AlertDialog.Builder(this)
                .setTitle("Safo®")
                .setMessage("Tu chofer ha cancelado el viaje")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        currentScreen = screens.confirm;
                        tvCancelBack.setVisibility(View.GONE);
                        rlDriverOHW.setVisibility(View.GONE);
                        rlDriver.setVisibility(View.GONE);
                        rlConfOrder.setVisibility(View.VISIBLE);
                        back();
                    }

                })
                .show();
    }

    public void requesTimeout(){
        new AlertDialog.Builder(this)
                .setTitle("Safo®")
                .setMessage("No se encontró ningún chofer cercano a tu posición")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        currentScreen = screens.confirm;
                        tvCancelBack.setVisibility(View.GONE);
                        rlDriverOHW.setVisibility(View.GONE);
                        rlDriver.setVisibility(View.GONE);
                        rlConfOrder.setVisibility(View.VISIBLE);
                    }

                })
                .show();
    }

    public void tripCompleted(){
        searchDialog.dismiss();
        sDestination = null;
        findViewById(R.id.ll_see_map).setVisibility(View.GONE);
        Intent intent = new Intent(OrderDriverActivity.this, ReceiptActivity.class);
        intent.putExtra("sCurrentTrip", sCurrentTrip);
        startActivity(intent);
        finish();
    }

    public void cancelRequestDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder
                .setMessage("¿Desea cancelar su SAFO?")
                .setCancelable(false)
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                cancelRequest();
                            }
                        })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressDialog = createProgressDialog(OrderDriverActivity.this);
                    }
                });
        AlertDialog alertGPS = alertDialogBuilder.create();
        alertGPS.show();
    }

    private void cancelRequest(){
        Map<String, String> params = new LinkedHashMap<>();
        params.put("lat", String.valueOf(sUserLocation.getLatitude()));
        params.put("lng", String.valueOf(sUserLocation.getLongitude()));

        String url = getString(R.string.trip_cancel_request, BuildConfig.API_URL, sCurrentTrip.getId().toString());
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                url,
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        currentScreen = screens.user_cancel_request;
                        Toast.makeText(mContext, "Safo cancelado", Toast.LENGTH_SHORT).show();
                        back();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(OrderDriverActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    public List<Vehicle> carSelected(int position) {
        setRvCarsSize();
        Vehicle vehicleAux = mVehicleList.get(position);
        mVehicleList.remove(position);
        mVehicleList.add(0, vehicleAux);
        return mVehicleList;
    }

    public List<CardM> cardSelected(int position) {
        setRvCardsSize();
        CardM cardAux = mCardsList.get(position);
        mCardsList.remove(position);
        mCardsList.add(0, cardAux);
        return mCardsList;
    }

    public static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

    private ProgressDialog createProgressDialog(Context mContext) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setOwnerActivity((Activity) mContext);
        dialog.setCancelable(false);
        dialog.setTitle("Solicitando Safer");
        dialog.setMessage("Espere un momento . . .");
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                OrderDriverActivity.this.cancelRequestDialog();
            }
        });
        try {
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }


        return dialog;
    }

    public void dismissProgressDialog(){
        progressDialog.dismiss();
    }

    private void noVehiclesDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Aún no tienes agregado un vehículo")
                .setMessage("Es necesario primero agregar un vehículo a tu cuenta")
                .setPositiveButton("Agregar ahora", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mNoVehiclesAlertDialog.dismiss();
                        mNoVehiclesAlertDialog = null;
                        startActivityForResult(VehicleDetailsActivity.newIntent(mContext, null), REQUEST_ADD_VEHICLE);
                    }
                })
                .setNegativeButton("Más tarde", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Show each vehicle and card dialog once
                       dismissVehiclesDialog();
                    }
                });
        // This prevents showing both vehicle and card dialogs at the same time
        mNoVehiclesAlertDialog = builder.create();
        if(mNoCardsAlertDialog == null) {
            mNoVehiclesAlertDialog.show();
        }

    }

    private void noCardsDialog(){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Aún no tienes agregada una forma de pago")
                .setMessage("Es necesario primero agregar una forma de pago a tu cuenta")
                .setPositiveButton("Agregar ahora", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mNoCardsAlertDialog.dismiss();
                        mNoCardsAlertDialog = null;
                        startActivityForResult(AddCardActivity.newIntent(mContext), REQUEST_ADD_CARD);
                    }
                })
                .setNegativeButton("Más tarde", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Show each vehicle and card dialog once
                        dismissCardsDialog();
                    }
                });
        // This prevents showing both vehicle and card dialogs at the same time
        mNoCardsAlertDialog = builder.create();
        if(mNoVehiclesAlertDialog == null) {
            mNoCardsAlertDialog.show();
        }
    }

    private void dismissVehiclesDialog() {
        mNoVehiclesAlertDialog.dismiss();
        mNoVehiclesAlertDialog = null;

        if(mNoCardsAlertDialog != null) {
            mNoCardsAlertDialog.show();
        }
    }

    private void dismissCardsDialog() {
        mNoCardsAlertDialog.dismiss();
        mNoCardsAlertDialog = null;

        if(mNoVehiclesAlertDialog != null) {
            mNoVehiclesAlertDialog.show();
        }
    }

    private void callDriver(String num){
            if(num != null && !num.equals("")) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + num));
                if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
            }
            else Toast.makeText(this, "Número no encontrado", Toast.LENGTH_SHORT).show();
    }

    //<------ ACTIONS ----------------------------------------------------------------------------->
    @Override
    public void onBackPressed() {
        back();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.rl_conf_order_driver:
                requestSafo();
                break;
            case R.id.iv_see_map:
                YoYo.with(Techniques.SlideOutDown).delay(0).duration(200).playOn(content);
                Intent intent = new Intent();
                intent.putExtra("tripSeeMap" , sCurrentTrip);
                OrderDriverActivity.this.setResult(RESULT_OK, intent);
                OrderDriverActivity.this.finish();
                break;
            case R.id.iv_call_see_map:
                callDriver(sCurrentTrip.getDriver().getPhone());
                break;
        }
    }

    private void back() {
        if (chronometer != null) {
            chronometer.cancel();
        }

        if (currentScreen == screens.confirm) {
            YoYo.with(Techniques.SlideOutDown).delay(0).duration(200).playOn(content);
            finish();
        } else if (currentScreen == screens.driverOHW) {
            cancelRequest();
        } else if (currentScreen == screens.selectCar) {
            setRvCarsSize();
        } else if (currentScreen == screens.selectCard) {
            setRvCardsSize();
        } else if(currentScreen == screens.tripOnCourse){

        } else {
            finish();
        }
    }

}
