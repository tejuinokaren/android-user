package mx.safo.user.activities;

import android.Manifest;
import android.animation.LayoutTransition;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResolvingResultCallbacks;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.adapters.SearchRvAdapter;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.map.MapStateListener;
import mx.safo.user.map.TouchableMapFragment;
import mx.safo.user.models.Destination;
import mx.safo.user.models.DriverPosition;
import mx.safo.user.models.Estimated;
import mx.safo.user.models.PlaceDetailsRes;
import mx.safo.user.models.PlaceLocal;
import mx.safo.user.models.Result;
import mx.safo.user.models.Trip;
import mx.safo.user.models.TripPoint;
import mx.safo.user.models.User;
import mx.safo.user.utils.MapAnimator;
import mx.safo.user.utils.MyFirebaseService;
import mx.safo.user.utils.MyReceiver;
import mx.safo.user.utils.PolylineDecoder;
import mx.safo.user.utils.SessionManager;
import mx.safo.user.utils.UserLocationManager;
import mx.safo.user.views.AutoResizeTextView;

import static mx.safo.user.utils.UserLocationManager.GOOGLE_DIRECTIONS_KEY;
import static mx.safo.user.utils.UserLocationManager.sUserLocation;

public class HomeMapActivity extends AppCompatActivity
        implements OnMapReadyCallback, LocationListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapLoadedCallback {

    private static final String TAG = HomeMapActivity.class.getSimpleName();
    private static final int CALL_PERMISSIONS = 112;
    private static final int UPDATE_PROFILE = 446;
    private static final int HIDE_ORDER = 345;

    public static PlaceLocal sDestination; // keep same destination through the hole app
    public static PlaceLocal sOrigin; // keep same origin through the hole app

    //MAP
    private PlaceLocal mTempSearchDest;
    private ArrayList<PlaceLocal> searchPlaces;
    public static HashMap<Integer, Marker> sHashMapMakers;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private TouchableMapFragment mapFragment;
    private LocationManager locationManager;
    private BitmapDescriptor markersView1;
    private BitmapDescriptor markersView;
    private AutocompleteFilter filter;
    private Marker plDestination;
    private LatLngBounds bounds;
    private Polyline polyline;
    private Marker plOrigin;
    private GoogleMap mMap;
    private Timer timerNearDrivers;
    private Handler timerETA;
    private Boolean shouldUpdateOrigin = true;

    //VIEWS
    private AutoResizeTextView tvOriginTitle;
    private EditText etSearchDestination;
    private RelativeLayout rlDestination;
    private ImageView ivGoToUserLocation;
    private TextView tvDestinationTitle;
    private LinearLayout llOrderDriver;
    private RelativeLayout contentView;
    private TextView tvOriginSubtitle;
    private LinearLayout llOriginLbls;
    private EditText etSearchOrigin;
    private FrameLayout flCircles;
    private RecyclerView searchRV;
    private View circleToAnimate;
    private Dialog searchDialog;
    private View gradientView;
    private View targetPoint;
    private TextView tvTripType;
    private TextView tvTotalAmount;

    //DRAWER
    private DrawerLayout drawer;
    private Timer timerAuxsearch;
    private Animation circleAnimation;
    private Boolean isMarkerAnimating = false;
    private AlertDialog alertGPS;
    private boolean seeMap = false;
    private Estimated mEstimated = new Estimated();
    private Context mContext;
    private ArrayList<LatLng> mLatLngArrayList = new ArrayList<>();
    public static boolean sGetNearDrivers = false;
    public static boolean sGetDriverETA = false;
    public static boolean sGetTripLocation = false;
    public static Trip sCurrentTrip = new Trip();
    private Timer timerDriverEta;
    private Timer timerTripLocation;
    private String mDriverPolyline = "";
    private TextView tvTripEstTime;
    private TextView tvDriverArrivalTitle;
    private Dialog mPlayServicesDialog;
    private List<Destination> mRecentDestinationsList = new ArrayList<>();
    private CameraPosition mCameraPosition;
    private boolean mSetMapZoom = true;

    public static Intent newIntent(Context packageContext) {
        Intent i = new Intent(packageContext, HomeMapActivity.class);
        return i;
    }

    // region <------ APPLICATION FLOW -------------------------------------------------------------------------->
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_home_map);

        mContext = this;
        if(!checkPlayServices()) {
            return;
        }

        globalInit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Check if GPS is enabled
        if (!checkPlayServices() && UserLocationManager.getInstance(mContext).isGPSEnabled()) {
            return;
        } else if(mapFragment == null) {
            // We check mapFragment as is the first initialized view in globalInit
            // this prevent initializing everything multiple times
//            globalInit();
        }

        // Register this activity to listen to push notifications
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(), new IntentFilter("broadCastName"));

        // Update Firebase token on server
        MyFirebaseService.updateToken(mContext);

        getRecentDestinations();
        if (!seeMap) {
            getLastTrip();
            findViewById(R.id.rl_see_driver).setVisibility(View.GONE);
            contentView.setLayoutTransition(null);
            ivGoToUserLocation.setVisibility(View.VISIBLE);
            rlDestination.setVisibility(View.VISIBLE);
            llOrderDriver.setVisibility(View.VISIBLE);
            llOriginLbls.setVisibility(View.VISIBLE);
            gradientView.setVisibility(View.VISIBLE);
            contentView.setLayoutTransition(new LayoutTransition());
            viewsInAnimations();
        } else {
            mSetMapZoom = true;
            if (sGetNearDrivers) {
                getNearestDrivers();
            } else if (sGetDriverETA) {
                if(sCurrentTrip != null) {
                    getDriverEta();
                }
            } else if (sGetTripLocation) {
                if(sCurrentTrip != null) {
                    getTripLocation();
                }
            }
        }

        try {
            if(sDestination != null && !sCurrentTrip.getStatus().equalsIgnoreCase("Aceptado")) {
                if ((sOrigin.getPlaceID() != null || sOrigin.getPrimaryText() != null)
                        && sDestination.getPlaceID() != null ) {
                    showRequestedRouteOnMap();
                }
            } else {
                showEmptyDestinationState();
            }
        } catch (NullPointerException ex) {
            Log.d("onResume", "sOrigin/sDestination empty" + ex.getMessage());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        searchDialog.dismiss();
        unregisterReceiver(MyReceiver.getInstance());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }
    // endregion

    // region <------ IMPLEMENTATIONS -------------------------------------------------------------------------->
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } if(sDestination != null) {
            sDestination = null;
            showEmptyDestinationState();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.iv_goto_current_location_home_map:
                setCurrentLocationOnMap();
                break;
            case R.id.btn_change_destination_home_map:
                openSearch();
                break;
            case R.id.ll_order_driver_home_map:
                if(sDestination != null && mEstimated != null) {
                    if(UserLocationManager.getInstance(mContext).checkForPermission(contentView)) {
                        sGetNearDrivers = true;
                        sGetDriverETA = false;
                        sGetTripLocation = false;
                        requestSafo();
                    }
                } else {
                    // There is not sDestination set up
                    openSearch();
                }
                break;
            case  R.id.rl_back_see_driver:
                if(timerETA != null) {
                    timerETA.removeCallbacksAndMessages(null);
                }
                findViewById(R.id.rl_see_driver).setVisibility(View.GONE);
                viewsInAnimations();
                startActivityForResult(OrderDriverActivity.newIntent(mContext, mEstimated), HIDE_ORDER);
                break;
        }
    }

    /**
     * Moves camera to current user location
     */
    private void setCurrentLocationOnMap() {
        if (UserLocationManager.getInstance(mContext).checkForPermission(contentView) ) {
            if (sUserLocation != null)
                setCamera(new LatLng(sUserLocation.getLatitude(), sUserLocation.getLongitude()));
            else Snackbar.make(contentView, "Buscando ubicación mediante GPS ...", Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        createLocationRequest();
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(final Location location) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(getSharedPreferences("user", MODE_PRIVATE).getBoolean("isLogged", false)) {
                    sUserLocation = location;
                    UserLocationManager
                            .getInstance(mContext)
                            .updateUserLocation();
                    Log.v("HomeMap", "Location Update");
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapLoadedCallback(this);
        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
//                mSetMapZoom = true;

                /*if(cameraPosition.zoom > 18.0) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                } else {
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }*/
            }
        });
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                callDriver(sCurrentTrip.getDriver().getPhone());
            }
        });
        if (mMap != null) {
            mMap.getUiSettings().setRotateGesturesEnabled(false);
            setMyMap();
        }
    }

    private void setCamera(LatLng location) {
        if (location != null) {
            LatLng center = new LatLng(location.latitude, location.longitude);
            int zoom = 16;
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(center, zoom);
            mMap.animateCamera(cameraUpdate);
        }
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        if(seeMap && marker.getTag() != null && marker.getTag().equals("moto")){
            marker.setTitle("Llamar");
            Log.v("marker", "true");
            return false;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == UPDATE_PROFILE) {
                initNavData();
            } else if (requestCode == HIDE_ORDER){
                if (UserLocationManager.getInstance(mContext).checkForPermission(contentView) ) {
                    if (sUserLocation != null)
                        setCamera(new LatLng(sUserLocation.getLatitude(), sUserLocation.getLongitude()));
                    else Snackbar.make(contentView, "Buscando ubicación mediante GPS ...", Snackbar.LENGTH_SHORT).show();
                }

                viewsOutAnimations();
                seeMap = true;
                findViewById(R.id.rl_see_driver).setVisibility(View.VISIBLE);
//                getDriverEta();
            }
        } else{
            seeMap = false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case UserLocationManager.MAP_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setMyMap();
                }
            }
        }
    }

    @Override
    public void onMapLoaded() {
        animateCircle();
    }
    // endregion

    // region <------ INIT VIEWS -------------------------------------------------------------------------->
    /**
     * This function initializes all required variables, timers, etc.
     * Is needed to wrap all this initializations to call them after
     * PlayServices has been updated.
     */
    private void globalInit() {
        MapsInitializer.initialize(getApplicationContext());

        initNav();
        initViews();
        initAnimations();
        initMapUtils();
        setSearchDialog();

        sHashMapMakers = new HashMap<>();
        timerNearDrivers = new Timer();
        timerDriverEta = new Timer();
        timerTripLocation = new Timer();
        timerETA = new Handler();

        scheduleGetNearestDrivers();
        scheduleGetETA();
        scheduleGetTripLocation();
    }

    private void initMapUtils() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        filter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE).build();
        bounds = new LatLngBounds(new LatLng(20.6322588, -103.4638812), new LatLng(20.6420844, -103.4083913));
    }

    private void initNav() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (TouchableMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_home);
        mapFragment.getMapAsync(this);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        findViewById(R.id.iv_menu_home_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        initNavData();
    }

    private void initNavData() {
        final User user = User.fromSharedPref(this);

        int size = getResources().getDisplayMetrics().densityDpi * 65;
        Picasso.with(mContext)
                .setLoggingEnabled(true);
        Picasso.with(mContext)
                .load(user.getImage()
                        .replace("http://", "https://"))
                .error(R.drawable.profile_holder)
                .into((ImageView) drawer.findViewById(R.id.riv_nav_header));
        ((TextView) drawer.findViewById(R.id.tv_nav_email)).setText(user.getEmail());
        ((TextView) drawer.findViewById(R.id.tv_nav_name)).setText(user.getName().concat(" ").concat(user.getLastName()));
    }

    private void initViews() {
        ivGoToUserLocation = (ImageView) findViewById(R.id.iv_goto_current_location_home_map);
        tvOriginTitle = (AutoResizeTextView) findViewById(R.id.tv_origin_title_home_map);
        tvOriginSubtitle = (TextView) findViewById(R.id.tv_origin_subtitle_home_map);
        rlDestination = (RelativeLayout) findViewById(R.id.rl_destination_home_map);
        llOrderDriver = (LinearLayout) findViewById(R.id.ll_order_driver_home_map);
        tvDestinationTitle = (TextView) findViewById(R.id.tv_destination_home_map);
        llOriginLbls = (LinearLayout) findViewById(R.id.ll_origin_lbls_home_map);
        contentView = (RelativeLayout) findViewById(R.id.content_view_home_map);
        flCircles = (FrameLayout) findViewById(R.id.fl_circles_home_map);
        circleToAnimate = findViewById(R.id.circle_view_home_map);
        gradientView = findViewById(R.id.gradient_view_home_map);
        targetPoint = findViewById(R.id.taget_point_home_map);
        tvTripType = (TextView) findViewById(R.id.tv_type_homebtn);
        tvTotalAmount = (TextView) findViewById(R.id.tv_total_homebtn);
        tvTripEstTime = (TextView) findViewById(R.id.tv_estimated_arrive_time_see_driver);
        tvDriverArrivalTitle = (TextView) findViewById(R.id.tv_driver_arrive_see_driver);

        findViewById(R.id.btn_change_destination_home_map).setOnClickListener(this);
        ivGoToUserLocation.setOnClickListener(this);
        llOrderDriver.setOnClickListener(this);

        if(sUserLocation != null) {
            setAddressTextViews(new LatLng(sUserLocation.getLatitude(), sUserLocation.getLongitude()));
        }
    }

    private void initAnimations() {
        contentView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                circleToAnimate.setScaleY(0.3f);
                circleToAnimate.setScaleX(0.3f);
                markersView = BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(flCircles));
                circleToAnimate.setAlpha(0);
                markersView1 = BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(flCircles));
                circleToAnimate.setScaleY(1f);
                circleToAnimate.setScaleX(1f);
                contentView.getViewTreeObserver().removeOnPreDrawListener(this);
                viewsInAnimations();
                return true;
            }
        });

        circleAnimation = AnimationUtils.loadAnimation(HomeMapActivity.this, R.anim.circle_animation);
        circleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                circleToAnimate.setAlpha(0f);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }
    // endregion

    // region <------ ANIMATIONS -------------------------------------------------------------------------->
    private void viewsInAnimations() {
        YoYo.with(Techniques.BounceInDown).duration(700).playOn(llOriginLbls);
        YoYo.with(Techniques.BounceInUp).duration(700).playOn(rlDestination);
        YoYo.with(Techniques.BounceInUp).duration(700).playOn(llOrderDriver);
        YoYo.with(Techniques.BounceInUp).duration(700).playOn(ivGoToUserLocation);
    }

    private void viewsOutAnimations() {
        ivGoToUserLocation.setVisibility(View.GONE);
        rlDestination.setVisibility(View.GONE);
        llOrderDriver.setVisibility(View.GONE);
        llOriginLbls.setVisibility(View.GONE);
        gradientView.setVisibility(View.GONE);
    }

    public static Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getBottom() - v.getTop(), v.getRight() - v.getLeft(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }

    private void animateCircle() {
        circleToAnimate.setAlpha(1);
        circleToAnimate.startAnimation(circleAnimation);
    }

    private void rotateMarker(final Marker marker, final float toRotation) {
        final float rotation = toRotation > 180 ? toRotation - 360 : toRotation;
        if (!isMarkerAnimating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 1000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * rotation + (1 - t) * startRotation;

                    marker.setRotation(-rot > 180 ? rot - 360 : rot);
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    }
                }
            });
        }
    }

    public void animateMarker(final LatLng toPosition, final Marker m) {
        if (!isMarkerAnimating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            Projection proj = mMap.getProjection();
            Point startPoint = proj.toScreenLocation(m.getPosition());
            final LatLng startLatLng = proj.fromScreenLocation(startPoint);
            final long duration = 3000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerAnimating = true;
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed
                            / duration);
                    double lng = t * toPosition.longitude + (1 - t)
                            * startLatLng.longitude;
                    double lat = t * toPosition.latitude + (1 - t)
                            * startLatLng.latitude;
                    m.setPosition(new LatLng(lat, lng));

                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        isMarkerAnimating = false;
                    }
                }
            });
        }
    }
    // endregion

    // region <------ FUNCTIONS --------------------------------------------------------------------------->
    private Boolean checkPlayServices() {
        Integer resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if(mPlayServicesDialog == null) {
                mPlayServicesDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 0);
            }
            if (mPlayServicesDialog != null) {
                if(!mPlayServicesDialog.isShowing())
                    mPlayServicesDialog.show();
            }
            return false;
        }
        return true;
    }

    private Boolean checkForCallPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CALL_PERMISSIONS);
                return false;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CALL_PERMISSIONS);
                Snackbar snackbar = Snackbar.make(contentView, "\n" + "Habilitar permisos desde configuraciones", Snackbar.LENGTH_INDEFINITE)
                        .setDuration(1500)
                        .setAction("ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivity(intent);
                            }
                        });
                snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
                snackbar.show();
                return false;
            }
        } else {
            return true;
        }
    }

    public void placeSelected(int position) {
        if (etSearchOrigin.isFocused()) {
            sOrigin = searchPlaces.get(position);
            etSearchOrigin.setText(sOrigin.getPrimaryText());
            etSearchDestination.requestFocus();
            getPlaceLatLng(sOrigin.getPlaceID(), true);
        } else if(searchPlaces.get(position) != null) {
            sDestination = searchPlaces.get(position);
            etSearchDestination.setText(sDestination.getPrimaryText());
            if(sDestination.getLatLng() == null && !sDestination.getPlaceID().isEmpty()) {
                getPlaceLatLng(sDestination.getPlaceID(), false);
            } else {
                searchDialog.dismiss();
            }
        }
    }

    private void getPlaceLatLng(String placeId, final boolean isOrigin) {
        String parameters = "placeid=".concat(placeId).concat("&key=").concat(GOOGLE_DIRECTIONS_KEY);
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                "https://maps.googleapis.com/maps/api/place/details/json?".concat(parameters.replace(" ", "%20")),
                PlaceDetailsRes.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_EMPTY,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        Log.d(TAG, response.toString());
                        PlaceDetailsRes res = (PlaceDetailsRes) response;

                        Log.d("Destination lat/lng", res.result.getGeometry().getLocation().getLat().toString()
                                + ","
                                + res.result.getGeometry().getLocation().getLng().toString());
                        if(isOrigin) {
                            sOrigin.setLatLng(
                                    new LatLng(res.result.getGeometry().getLocation().getLat()
                                            , res.result.getGeometry().getLocation().getLng())
                            );
                        } else {
                            sDestination.setLatLng(
                                    new LatLng(res.result.getGeometry().getLocation().getLat()
                                            , res.result.getGeometry().getLocation().getLng())
                            );

                            // After places are all set then proceed to closeSearch()
                            searchDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    protected void startLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            //this==>(com.google.android.gms.location.LocationListener) not android.app.LocationListener
            Log.d(TAG, "Location update started ..............: ");
        } catch (SecurityException e) {
            Log.e("PERMISSION_EXCEPTION", "PERMISSION_NOT_GRANTED");
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    private void setAddressTextViews(LatLng latLng) {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude,
                    latLng.longitude, 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);

                if (returnedAddress.getAddressLine(0) != null) {
                    tvOriginTitle.setText(returnedAddress.getAddressLine(0));
                    sOrigin = new PlaceLocal();
                    sOrigin.setLatLng(new LatLng(latLng.latitude, latLng.longitude));
                    sOrigin.setPrimaryText(returnedAddress.getAddressLine(0));
                } else {
                    tvOriginTitle.setText("");
                }
                if (returnedAddress.getAddressLine(1) != null) {
                    tvOriginSubtitle.setText(returnedAddress.getAddressLine(1));
                } else {
                    tvOriginSubtitle.setText("");
                }

                Log.w("Current loction address",
                        "" + returnedAddress.getAddressLine(0) + "\n" + returnedAddress.getAddressLine(1));
            } else {
                Log.w("Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("Current loction address", "Canont get Address!");
        }
    }

    private void setMyMap() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(UserLocationManager.getInstance(mContext).isGPSEnabled())
                    if (UserLocationManager.getInstance(mContext).checkForPermission(contentView)) {
                        circleToAnimate.startAnimation(circleAnimation);
                        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        sUserLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if(sUserLocation != null) {
                            UserLocationManager
                                    .getInstance(mContext)
                                    .updateUserLocation();
                            setCamera(new LatLng(sUserLocation.getLatitude(), sUserLocation.getLongitude()));
                        }
                    }
                new MapStateListener(mMap, mapFragment, (Activity) mContext) {

                    @Override
                    public void onMapTouched() {
                    }

                    @Override
                    public void onMapReleased() {
                    }

                    @Override
                    public void onMapUnsettled() {
                    }

                    @Override
                    public void onMapSettled() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (shouldUpdateOrigin) {
//                                    sOrigin = null;
                                    setAddressTextViews(mMap.getCameraPosition().target);
                                }
                            }
                        });
                    }
                };

                try {
                    boolean success = mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(mContext, R.raw.map_style_json));
                    if (!success) {
                        Log.e(TAG, "Style parsing failed.");
                    } else {
                        Log.v(TAG, "Style parsing success.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e(TAG, "Can't find style. Error: ", e);
                }
            }
        });
    }

    private void scheduleGetNearestDrivers() {
        timerNearDrivers.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (getSharedPreferences("user", MODE_PRIVATE).getBoolean("isLogged", false)) {
                    if (sGetNearDrivers) {
                        getNearestDrivers();
                        Log.v("HomeMap", "Get near drivers");
                    }
                }
            }
        }, 0, 1000 * 20);
    }

    private void scheduleGetETA() {
        timerDriverEta.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (getSharedPreferences("user", MODE_PRIVATE).getBoolean("isLogged", false)) {
                    // Trip accepted but not started
                    if (sGetDriverETA) {
                        if(sCurrentTrip != null) {
                            getDriverEta();
                        }
                    }
                }
            }
        }, 0, 1000 * 10);
    }

    private void scheduleGetTripLocation() {
        timerTripLocation.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (getSharedPreferences("user", MODE_PRIVATE).getBoolean("isLogged", false)) {
                    // Trip accepted but not started
                    if (sGetTripLocation) {
                        if(sCurrentTrip != null) {
                            getTripLocation();
                        }
                    }
                }
            }
        }, 0, 1000 * 10);
    }

    private void verifyMarkerOnScreen(ArrayList<DriverPosition> driverPositions) {
        Marker m;
        for (DriverPosition driverPosition : driverPositions) { //get all drivers
            if (sHashMapMakers.containsKey(driverPosition.getId())) {
                m = sHashMapMakers.get(driverPosition.getId());
                m.setTag("moto");
                float[] results = new float[1]; //Saves the distance between prev and actual position
                Location.distanceBetween(m.getPosition().latitude, m.getPosition().longitude,
                        driverPosition.getPosition().latitude, driverPosition.getPosition().longitude, results);
                if(results[0] > 5) {
                    animateMarker(driverPosition.getPosition(), m);
                    rotateMarker(m, (float) bearingBetweenLocations(m.getPosition(), driverPosition.getPosition()));
                }
            } else {
                m = mMap.addMarker(new MarkerOptions()
                        .position(driverPosition.getPosition())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.motorcycle)).anchor(0.5f, 0.5f));
                m.setTag("moto");
                sHashMapMakers.put(driverPosition.getId(), m);
            }
        }
        for (Map.Entry<Integer, Marker> entry : sHashMapMakers.entrySet()) { //get all keys on mHashMap
            if (!isMarkerOnDriversList(entry, driverPositions))
                sHashMapMakers.get(entry.getKey()).remove();
        }
        for(Iterator<Map.Entry<Integer, Marker>> it = sHashMapMakers.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<Integer, Marker> entry = it.next();
            if (!isMarkerOnDriversList(entry, driverPositions))
                it.remove();
        }
    }

    private double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }

    private boolean isMarkerOnDriversList(Map.Entry<Integer, Marker> entry, ArrayList<DriverPosition> driverPositions) {
        for (DriverPosition driverPosition : driverPositions) {
            if (entry.getKey() == driverPosition.getId()) return true;
        }
        return false;
    }

    private void requestSafo(){
        try {
            defineTrip();
            if (!tvOriginTitle.getText().equals("")) {
                if (sOrigin != null) {
                    if (polyline != null) {
                        MapAnimator.getInstance().resetAll();
                        polyline.remove();
                        plOrigin.remove();
                        plDestination.remove();
                        targetPoint.setAlpha(1);
                    }

                    startActivityForResult(OrderDriverActivity.newIntent(mContext, mEstimated), HIDE_ORDER);

                }
            } else {
                Snackbar.make(contentView, "Seleccione un lugar de origen", Snackbar.LENGTH_SHORT).show();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    private void defineTrip() {
        PendingResult<PlaceBuffer> r;
        if (sOrigin != null && sDestination != null && sOrigin.getPlaceID() != null && sDestination.getPlaceID() != null) {
            r = Places.GeoDataApi.getPlaceById(mGoogleApiClient, sOrigin.getPlaceID(), sDestination.getPlaceID());
        } else if (sOrigin != null && sOrigin.getPlaceID() != null) {
            r = Places.GeoDataApi.getPlaceById(mGoogleApiClient, sOrigin.getPlaceID());
        } else if (sDestination != null && sDestination.getPlaceID() != null) {
            r = Places.GeoDataApi.getPlaceById(mGoogleApiClient, sDestination.getPlaceID());
        } else {
            LatLng originLatLng = mMap.getCameraPosition().target;
            sOrigin = new PlaceLocal();
            sOrigin.setLatLng(originLatLng);
            sOrigin.setPrimaryText(tvOriginTitle.getText().toString());
            sOrigin.setSecondaryText(tvOriginSubtitle.getText().toString());
            return;

        }

        r.setResultCallback(new ResultCallbacks<PlaceBuffer>() {
            @Override
            public void onSuccess(@NonNull PlaceBuffer places) {

                if (places.getStatus().isSuccess() && places.getCount() > 0) {
                    if (sOrigin != null && sDestination != null && sOrigin.getPlaceID() != null && sDestination.getPlaceID() != null) {
                        sOrigin.setLatLng(places.get(0).getLatLng());
                        sDestination.setLatLng(places.get(1).getLatLng());
                    } else if (sOrigin != null && sOrigin.getPlaceID() != null) {
                        sOrigin.setLatLng(places.get(0).getLatLng());
                    } else if (sDestination != null && sDestination.getPlaceID() != null) {
                        sDestination.setLatLng(places.get(0).getLatLng());
                    }

                    if (sOrigin == null) {
                        LatLng originLatLng = mMap.getCameraPosition().target;
                        sOrigin = new PlaceLocal();
                        sOrigin.setLatLng(originLatLng);
                        sOrigin.setPrimaryText(tvOriginTitle.getText().toString());
                        sOrigin.setSecondaryText(tvOriginSubtitle.getText().toString());
                    }
                } else {
                    Log.e(TAG, "Place not found");
                }
                places.release();
            }

            @Override
            public void onFailure(@NonNull Status status) {

            }
        });
    }

    private void openSearch() {
        searchDialog.show();
        mTempSearchDest = sDestination;
        sDestination = null;
        etSearchOrigin.setText(tvOriginTitle.getText());
        autocomplete(tvOriginTitle.getText().toString().concat("+").concat(tvOriginSubtitle.getText().toString()), true);

        etSearchDestination.setText(null);
        etSearchDestination.requestFocus();
    }

    private void autocomplete(String query, final boolean shouldSelectFirst) {
        final PendingResult<AutocompletePredictionBuffer> result = Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, query, bounds, filter);
        result.setResultCallback(new ResolvingResultCallbacks<AutocompletePredictionBuffer>((Activity) mContext, 1) {
            @Override
            public void onSuccess(@NonNull AutocompletePredictionBuffer autocompletePredictions) {
                if (autocompletePredictions.getStatus().isSuccess()) {
                    searchPlaces = new ArrayList<>();
                    for (int i = 0; i < autocompletePredictions.getCount(); i++) {
                        AutocompletePrediction acPrediction = autocompletePredictions.get(i);
                        searchPlaces.add(new PlaceLocal(acPrediction.getSecondaryText(null).toString(), acPrediction.getPrimaryText(null).toString(), acPrediction.getPlaceId()));
                    }
                    if (shouldSelectFirst && !searchPlaces.isEmpty()) {
                        placeSelected(0);
                    }
                    setRvSearch();
                }
                autocompletePredictions.release();
            }

            @Override
            public void onUnresolvableFailure(@NonNull Status status) {
                Log.v("Auto Complete", "UnresolvableFailure");
                Log.e("Status", status.getStatusMessage());
            }
        });
    }

    private void setRvSearch() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        SearchRvAdapter adapter = new SearchRvAdapter(searchPlaces, this);
        searchRV.setLayoutManager(layoutManager);
        searchRV.setAdapter(adapter);
        searchRV.setHasFixedSize(true);
    }

    private void setSearchDialog() {
        searchDialog = new Dialog(mContext);
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.dialog_search);
        searchDialog.findViewById(R.id.iv_close_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchDialog.dismiss();
            }
        });
        searchDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                closeSearch();
            }
        });

        searchRV = (RecyclerView) searchDialog.findViewById(R.id.rv_search);
        etSearchOrigin = (EditText) searchDialog.findViewById(R.id.et_origin_search);
        etSearchDestination = (EditText) searchDialog.findViewById(R.id.et_destination_search);

        etSearchOrigin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (timerAuxsearch != null) {
                    timerAuxsearch.cancel();
                }

                timerAuxsearch = new Timer();
                timerAuxsearch.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        autocomplete(etSearchOrigin.getText().toString(), false);
                    }
                }, 500);
            }
        });

        etSearchDestination.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (timerAuxsearch != null) {
                    timerAuxsearch.cancel();
                }

                timerAuxsearch = new Timer();
                timerAuxsearch.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if(etSearchDestination.getText().toString().isEmpty()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showRecentPlaces();
                                }
                            });
                        } else {
                            autocomplete(etSearchDestination.getText().toString(), false);
                        }
                    }
                }, 500);
            }
        });

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        searchDialog.getWindow().setLayout((int) (metrics.widthPixels * 0.99), (int) (metrics.heightPixels * 0.9));
    }

    /**
     * Show most recent places list while no search started
     */
    private void showRecentPlaces() {
        searchPlaces = new ArrayList<>();
        searchPlaces.add(new PlaceLocal().placeLocalHeader());
        for(Destination destination : mRecentDestinationsList) {
            searchPlaces.add(new PlaceLocal("", destination.address, destination.id, new LatLng(Double.parseDouble(destination.lat), Double.parseDouble(destination.lng))));
        }
        setRvSearch();
    }

    private void closeSearch() {
        searchPlaces.clear();
        try {
            if (sOrigin.getPlaceID() != null) {
                tvOriginTitle.setText(sOrigin.getPrimaryText());
                tvOriginSubtitle.setText(sOrigin.getSecondaryText());
                if (sDestination == null) {
                    Places.GeoDataApi.getPlaceById(mGoogleApiClient, sOrigin.getPlaceID()).setResultCallback(new ResultCallbacks<PlaceBuffer>() {
                        @Override
                        public void onSuccess(@NonNull PlaceBuffer places) {

                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                sOrigin.setLatLng(places.get(0).getLatLng());
                                setCamera(sOrigin.getLatLng());
                            } else {
                                Log.e(TAG, "Place not found");
                            }

                            places.release();
                        }

                        @Override
                        public void onFailure(@NonNull Status status) {

                        }
                    });
                }
            }
        } catch (NullPointerException e) {
            Log.d("closeSearch", e.getMessage());
        }

        // If a new destination has been selected calculate rate
        if (sDestination != null) {
            tvDestinationTitle.setText(sDestination.getPrimaryText());
            calculateRate(sOrigin, sDestination);
        } else {
            // The destination is null
            sDestination = mTempSearchDest;

            // Get the previous search if null then show empty state
            mTempSearchDest = null;
            if(sDestination == null) {
                showEmptyDestinationState();
            }
        }

        // Hide searchDialog
//        searchDialog.dismiss();
    }

    private void getPolylineByLatLng(LatLng pointA, LatLng pointB) {
        String parameters = "origin=" + pointA.latitude + "," + pointA.longitude +
                "&destination=" + pointB.latitude + "," + pointB.longitude +
                "&sensor=false&mode=driving&alternatives=true" +
                "&key=" + GOOGLE_DIRECTIONS_KEY;
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                "https://maps.googleapis.com/maps/api/directions/json?".concat(parameters.replace(" ", "%20")),
                Result.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_EMPTY,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        Log.d(TAG, response.toString());
                        Result res = (Result) response;
                        String driverPolyline = res.getRoutes().get(0).getOverviewPolyline().getPoints();
                        if(driverPolyline != null && !driverPolyline.isEmpty() ) {
                            mEstimated.setEstimatedPolylines(driverPolyline);
                            // Update polyline route
//                            if(mLatLngArrayList != null && mLatLngArrayList.isEmpty()) {
                                getDirections();
//                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, false
        ));
    }

    private void getRecentDestinations() {
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest(
                getString(R.string.api_recent_destinations, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                null,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        ApiResponse res = (ApiResponse) response;
                        mRecentDestinationsList = res.getData().destinations;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }, true
        ));
    }

    private void getDirections() {
        String points = mEstimated.getEstimatedPolylines();
        mLatLngArrayList = new PolylineDecoder().decode(points);
        PolylineOptions polylineOpts = new PolylineOptions();

        for (int i = 0; i < mLatLngArrayList.size(); i++) {
            polylineOpts.add(mLatLngArrayList.get(i));
        }
        MapAnimator.getInstance().resetAll();
        if (polyline != null)
            polyline.remove();

        if (plOrigin != null)
            plOrigin.remove();

        if (plDestination != null)
            plDestination.remove();

        polyline = mMap.addPolyline(polylineOpts);
        shouldUpdateOrigin = false;
        polyline.setColor(ContextCompat.getColor(getBaseContext(), R.color.empty));
        polyline.setWidth(4f);
        fitPolylineInMap(polyline);
        List<LatLng> plPoints = polyline.getPoints();
        if (!plPoints.isEmpty()) {
            LatLng start = plPoints.get(0);
            LatLng end = plPoints.get(plPoints.size() - 1);

            plDestination = mMap.addMarker(new MarkerOptions()
                    .icon(markersView)
                    .position(end)
                    .anchor(0.5f, 0.5f));

            plOrigin = mMap.addMarker(new MarkerOptions()
                    .icon(markersView1)
                    .position(start)
                    .anchor(0.5f, 0.5f));
        }
        targetPoint.setAlpha(0);
        if(!mLatLngArrayList.isEmpty()) {
            MapAnimator.getInstance().animateRoute(mMap, mLatLngArrayList);
            defineTrip();
        }

        Log.v(TAG, "polyline drawed");


       /* Log.v("getDirections params", parameters);
        ApiClient.getInstance(mContext).addToRequestQueue(new ApiClient.GsonRequest<>(
                "https://maps.googleapis.com/maps/api/directions/json?".concat(parameters.replace(" ", "%20")),
                DirectionsResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_EMPTY,
                null,
                new Response.Listener<DirectionsResponse>() {
                    @Override
                    public void onResponse(DirectionsResponse response) {
                        try {
                            JSONArray JSON = new JSONArray(String.valueOf(response.getRoutes()));
                            String points = JSON.getJSONObject(0)
                                    .getJSONObject("overview_polyline").getString("points");
                            mLatLngArrayList = new PolylineDecoder().decode(points);
                            PolylineOptions polylineOpts = new PolylineOptions();

                            for (int i = 0; i < mLatLngArrayList.size(); i++) {
                                polylineOpts.add(mLatLngArrayList.get(i));
                            }
                            MapAnimator.getInstance().resetAll();
                            if (polyline != null)
                                polyline.remove();

                            if (plOrigin != null)
                                plOrigin.remove();

                            if (plDestination != null)
                                plDestination.remove();

                            polyline = mMap.addPolyline(polylineOpts);
                            shouldUpdateOrigin = false;
                            polyline.setColor(ContextCompat.getColor(getBaseContext(), R.color.empty));
                            polyline.setWidth(4f);
                            fitPolylineInMap(polyline);
                            List<LatLng> plPoints = polyline.getPoints();
                            if (!plPoints.isEmpty()) {
                                LatLng start = plPoints.get(0);
                                LatLng end = plPoints.get(plPoints.size() - 1);

                                plDestination = mMap.addMarker(new MarkerOptions()
                                        .icon(markersView)
                                        .position(end)
                                        .anchor(0.5f, 0.5f));

                                plOrigin = mMap.addMarker(new MarkerOptions()
                                        .icon(markersView1)
                                        .position(start)
                                        .anchor(0.5f, 0.5f));
                            }
                            targetPoint.setAlpha(0);
                            MapAnimator.getInstance().animateRoute(mMap, mLatLngArrayList);
                            defineTrip();

                            Log.v(TAG, "polyline drawed");
                        } catch (JSONException e) {
//                            Toast.makeText(getBaseContext(), "Ocurrió un error al trazar la ruta. Intentalo de nuevo.", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            Log.v(TAG, "polyline not drawed");
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                            Log.v(TAG, "polyline not drawed");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, false
        ));*/
    }

    private void fitPolylineInMap(Polyline p) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        List<LatLng> arr = p.getPoints();
        for (int i = 0; i < arr.size(); i++) {
            builder.include(arr.get(i));
        }
        LatLngBounds bounds = builder.build();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int padding = (int) (size.y * 0.2); // offset from edges of the map in pixels
        if(mSetMapZoom) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
            mSetMapZoom = false;
        }
    }

    private void showEmptyDestinationState() {
        tvTripType.setText("Safo");
        tvTotalAmount.setText("");
        findViewById(R.id.tv_estimated_text).setVisibility(View.GONE);
//        sHashMapMakers.clear();

        tvDestinationTitle.setText("Sin especificar");
        clearMapRoute();
    }

    private void clearMapRoute() {
        if (polyline != null) {
            plOrigin.remove();
            plDestination.remove();
            shouldUpdateOrigin = true;
            targetPoint.setAlpha(1);
            animateCircle();
            MapAnimator.getInstance().resetAll();
            polyline.remove();
            polyline = null;
        }

        mLatLngArrayList.clear();
        setCurrentLocationOnMap();
    }

    public void checkTripStatus(){
        timerETA.removeCallbacksAndMessages(null);
        Intent intent;
        /*solicitado
            * iniciado
            * aceptado
            * finalizadoo
            * cancelado*/
        switch (sCurrentTrip.getStatus()){
            case "Cancelado":
                sGetNearDrivers = true;
                sGetDriverETA = false;
                sGetTripLocation = false;
                if(sCurrentTrip.getSeen() == 1)
                    break;
            /*case "request_timeout":
                if(trip.getSeen() == 1)
                    break;*/
            case "Solicitado":
                sGetNearDrivers = true;
                sGetDriverETA = false;
                sGetTripLocation = false;
                sHashMapMakers.clear();
                startActivityForResult(OrderDriverActivity.newIntent(mContext, mEstimated), HIDE_ORDER);
                break;
            case "Aceptado":
                sGetNearDrivers = false;
                sGetDriverETA = true;
                sGetTripLocation = false;
                sHashMapMakers.clear();
                tvDriverArrivalTitle.setText("Tu chofer llegará en: ");

                // Initialize origin and destination
                if(sOrigin == null || sDestination == null) {
                    sOrigin = new PlaceLocal();
                    sDestination = new PlaceLocal();
                    sOrigin.setPrimaryText(sCurrentTrip.getRequestOriginAddress());
                    sDestination.setPrimaryText(sCurrentTrip.getRequestDestinationAddress());
                    sDestination.setLatLng(new LatLng(Double.parseDouble(sCurrentTrip.getRequestDestinationLat()), Double.parseDouble(sCurrentTrip.getRequestDestinationLng())));
                    mEstimated.setEstimatedTotal(sCurrentTrip.getEstimatedTotal());
                    mEstimated.setEstimatedTime(sCurrentTrip.getEstimatedTime());

                    // Update UI
                    tvTripType.setText(String.format("$%1$,.2f", mEstimated.getEstimatedTotal()));
                    tvTotalAmount.setText(" MXN ");
                }
                startActivityForResult(OrderDriverActivity.newIntent(mContext, mEstimated), HIDE_ORDER);
                break;
            case "Iniciado":
                sGetNearDrivers = false;
                sGetDriverETA = false;
                sGetTripLocation = true;
                tvDriverArrivalTitle.setText("Tiempo estimado: ");
                tvTripEstTime.setText(mEstimated.getEstimatedTime());
                startActivityForResult(OrderDriverActivity.newIntent(mContext, mEstimated), HIDE_ORDER);
                break;
            case "Finalizado":
                sGetNearDrivers = true;
                sGetDriverETA = false;
                sGetTripLocation = false;
                if(sCurrentTrip.getRating() == 0) {
                    showEmptyDestinationState();
                    sDestination = null;
                    intent = new Intent(mContext, ReceiptActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("trip", sCurrentTrip);
                    startActivity(intent);
                }
                break;

        }
    }

    public void onNavItemClick(View view) {
        int id = view.getId();

        if (drawer != null)
            drawer.closeDrawer(Gravity.START, true);

        switch (id) {
            //******** NAV DRAWER *************
            case R.id.nav_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setTitle("Confirmar")
                        .setMessage("¿Estás seguro de que quieres cerrar sesión?")
                        .setPositiveButton("Cerrar sesión", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                logout();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                break;

            case R.id.nav_schedule:
                startActivity(new Intent(mContext, ScheduleActivity.class));
                break;

            case R.id.nav_cars:
                startActivity(new Intent(mContext, VehicleListActivity.class));
                break;

            case R.id.nav_payment:
                startActivity(new Intent(mContext, CardListActivity.class));
                break;

            case R.id.nav_history:
                startActivity(new Intent(mContext, HistoryListActivity.class));
                break;

            case R.id.ll_profile_nav:
            case R.id.riv_nav_header:
                startActivityForResult(new Intent(mContext, ProfileActivity.class), UPDATE_PROFILE);
                break;
            case R.id.nav_help:
                startActivity(new Intent(mContext, HelpListActivity.class));
                break;
            case R.id.nav_coupons:
                startActivity(new Intent(mContext, CouponActivity.class));
                break;
            case R.id.terms_and_conditions_label:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("http://safoapp.com/assets/pdf/terminos_y_condiciones.pdf"));
                startActivity(i);
                break;

            case R.id.website_label:
                Intent i2 = new Intent(Intent.ACTION_VIEW);
                i2.setData(Uri.parse("http://safoapp.com/conductor.pdf"));
                startActivity(i2);
                break;
        }
    }

    private void callDriver(String num){
        if(checkForCallPermission()) {
            if(num != null && !num.equals("")) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + num));
                if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
            }
            else Toast.makeText(this, "Número no encontrado", Toast.LENGTH_SHORT).show();
        }
    }
    // endregion

    // region <------ SERVER APIS --------------------------------------------------------------------------->
    public void getLastTrip() {
        Map<String, String> params = new LinkedHashMap<>();
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.trip_get_last, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        seeMap = false;
                        sCurrentTrip = response.getData().getTrip();
                        mEstimated.setEstimatedTime(sCurrentTrip.getEstimatedTime());

                        checkTripStatus();
                        Log.d("Last trip data", sCurrentTrip.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, false
        ));

    }

    private void getNearestDrivers() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(sUserLocation != null) {
                    String url = getString(R.string.get_nearest_drivers, BuildConfig.API_URL)
                            + "?lat=" + String.valueOf(mMap.getCameraPosition().target.latitude)
                            + "&lng=" + String.valueOf(mMap.getCameraPosition().target.longitude);
                    ApiClient.getInstance(mContext).addToRequestQueue(new ApiClient.GsonRequest<>(
                            url,
                            ApiResponse.class,
                            Request.Method.GET,
                            ApiClient.Header.HEADER_DEFAULT,
                            null,
                            new Response.Listener<ApiResponse>() {
                                @Override
                                public void onResponse(ApiResponse response) {
                                    try {
                                        JSONArray array = new JSONArray(response.getData().getNearDrivers().toString());
                                        final ArrayList<DriverPosition> driverPositions = DriverPosition.getArray(array);
                                        /*if(seeMap) {
//                                            timerNearDrivers.cancel();
                                        } else {*/
                                        verifyMarkerOnScreen(driverPositions);
//                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Log.e("Got Drivers: ", "bad response");
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if(!error.getMessage().isEmpty())
                                        Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }, false
                    ));
                }
            }
        });
    }

    /**
     * Get requested driver position and show it in map
     */
    private void getDriverEta() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(sUserLocation != null) {
                    String url = getString(R.string.get_eta, BuildConfig.API_URL, sCurrentTrip.getId());
                    ApiClient.getInstance(mContext).addToRequestQueue(new ApiClient.GsonRequest<>(
                            url,
                            ApiResponse.class,
                            Request.Method.GET,
                            ApiClient.Header.HEADER_DEFAULT,
                            null,
                            new Response.Listener<ApiResponse>() {
                                @Override
                                public void onResponse(ApiResponse response) {
                                    Estimated estimated = response.getData().getEstimated();
                                    PlaceLocal origin = new PlaceLocal();
                                    LatLng driverLatLng = new LatLng(estimated.getDriverPositionLat(), estimated.getDriverPositionLng());
                                    origin.setLatLng(driverLatLng);

//                                    Toast.makeText(mContext, "ETA location:\n" + driverLatLng.latitude + ", " + driverLatLng.longitude, Toast.LENGTH_SHORT).show();

                                    // Create an array with the driver position to show motorcycle marker on screen
                                    final ArrayList<DriverPosition> driverPositions = new ArrayList<>();
                                    DriverPosition driverPosition = new DriverPosition(driverLatLng, new LatLng(0, 0), (long) 0, sCurrentTrip.getId());
                                    driverPositions.add(driverPosition);
                                    verifyMarkerOnScreen(driverPositions);

                                    tvTripEstTime.setText(estimated.getEstimatedTime());
                                    // Draw polyline between driver and user requestedOrigin
                                    getPolylineByLatLng(driverLatLng, new LatLng(Double.parseDouble(sCurrentTrip.getRequestOriginLat()), Double.parseDouble(sCurrentTrip.getRequestOriginLng())));
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if(!error.getMessage().isEmpty())
                                        Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }, false
                    ));
                }
            }
        });
    }

    private void getTripLocation() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(sUserLocation != null) {
                    String url = getString(R.string.last_trip_location, BuildConfig.API_URL, sCurrentTrip.getId());
                    ApiClient.getInstance(mContext).addToRequestQueue(new ApiClient.GsonRequest<>(
                            url,
                            ApiResponse.class,
                            Request.Method.GET,
                            ApiClient.Header.HEADER_DEFAULT,
                            null,
                            new Response.Listener<ApiResponse>() {
                                @Override
                                public void onResponse(ApiResponse response) {
                                    tvDriverArrivalTitle.setText("Tiempo estimado: ");
                                    tvTripEstTime.setText(mEstimated.getEstimatedTime());

                                    TripPoint point = response.getData().getPoint();
                                    LatLng driverLatLng = new LatLng(point.getLat(), point.getLng());
                                    PlaceLocal origin = new PlaceLocal();
                                    origin.setLatLng(driverLatLng);
//                                    Toast.makeText(mContext, "Trip location:\n" + point.getLat().toString() + ", " + point.getLng().toString(), Toast.LENGTH_SHORT).show();

                                    // Create an array with the driver position to show motorcycle marker on screen
                                    final ArrayList<DriverPosition> driverPositions = new ArrayList<>();
                                    DriverPosition driverPosition = new DriverPosition(driverLatLng, new LatLng(0, 0), (long) 0, sCurrentTrip.getId());
                                    driverPositions.add(driverPosition);
                                    verifyMarkerOnScreen(driverPositions);

                                    LatLng destinationPoint = new LatLng(Double.parseDouble(sCurrentTrip.getRequestDestinationLat()), Double.parseDouble(sCurrentTrip.getRequestDestinationLng()));
                                    // Draw polyline between driver and user requestedOrigin
                                    getPolylineByLatLng(driverLatLng, destinationPoint);
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if(!error.getMessage().isEmpty())
                                        Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }, false
                    ));
                }
            }
        });
    }

    private void calculateRate(PlaceLocal origin, PlaceLocal destination) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("origin_address", String.valueOf(origin.getPrimaryText()));
        params.put("origin_lat", String.valueOf(origin.getLatLng().latitude));
        params.put("origin_lng", String.valueOf(origin.getLatLng().longitude));
        params.put("destination_address",destination.getPrimaryText());
        params.put("destination_lat", String.valueOf(destination.getLatLng().latitude));
        params.put("destination_lng", String.valueOf(destination.getLatLng().longitude));

        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.trip_calculate_rate, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        mEstimated = response.getData().getEstimated();
                        tvTripType.setText(String.format("$%1$,.2f", mEstimated.getEstimatedTotal()));
                        tvTotalAmount.setText(" MXN ");
                        tvTripEstTime.setText(mEstimated.getEstimatedTime());
                        findViewById(R.id.tv_estimated_text).setVisibility(View.VISIBLE);

                        if(mEstimated.getEstimatedPolylines() != null && !mEstimated.getEstimatedPolylines().isEmpty()) {
                            showRequestedRouteOnMap();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty()) {
                            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, true
        ));
    }

    private void showRequestedRouteOnMap() {
        String parameters;
        if (sOrigin.getPlaceID() != null && sDestination.getPlaceID() != null) {
            parameters = "origin=".concat("place_id:").concat(sOrigin.getPlaceID()).concat(
                    "&destination=").concat("place_id:").concat(sDestination.getPlaceID()).concat(
                    "&key=").concat(GOOGLE_DIRECTIONS_KEY);
        } else if(sOrigin.getPlaceID() == null && sDestination.getPlaceID() == null) {
            parameters = "origin=".concat(sOrigin.getPrimaryText()).concat(
                    "&destination=").concat(sDestination.getPrimaryText()).concat(
                    "&key=").concat(GOOGLE_DIRECTIONS_KEY);
        } else {
            parameters = "origin=".concat(sOrigin.getPrimaryText()).concat(
                    "&destination=").concat("place_id:").concat(sDestination.getPlaceID()).concat(
                    "&key=").concat(GOOGLE_DIRECTIONS_KEY);

        }
        if(mEstimated != null && mEstimated.getEstimatedPolylines() != null) {
            if(mLatLngArrayList != null) {
                getDirections();
            }
        }
    }

    private void logout(){
        Map<String, String> params = new LinkedHashMap<>();
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.api_logout, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        /*SharedPreferences.Editor editor = getSharedPreferences("user", MODE_PRIVATE).edit();
                        editor.remove("user");
                        editor.putBoolean("isLogged", false);
                        editor.apply();*/
                        SessionManager.getInstance(mContext).logoutUser();
                        startActivity(new Intent(mContext, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }
    // endregion
}