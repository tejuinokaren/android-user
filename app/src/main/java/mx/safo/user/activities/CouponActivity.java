package mx.safo.user.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.adapters.CouponsAdapter;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.Coupon;

public class CouponActivity extends AppCompatActivity implements OnClickListener{

    private RecyclerView rvCoupons;
    private CouponsAdapter couponsAdapter;
    private ArrayList<Coupon> coupons;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon);
        //Hide keyboard
        findViewById(R.id.content_view_coupon).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        });
        rvCoupons = (RecyclerView) findViewById(R.id.rv_coupons);
        linearLayoutManager = new LinearLayoutManager(this);
        rvCoupons.setLayoutManager(linearLayoutManager);
        rvCoupons.setAdapter(null);

        getCoupons();
    }

    //<----- API ---------------------------------------------------------------------------------->
    private void getCoupons (){
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest(
                getString(R.string.get_coupons, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        ApiResponse res = (ApiResponse) response;
                        coupons = (ArrayList) res.getData().getCoupons();
                        couponsAdapter = new CouponsAdapter(CouponActivity.this, coupons);
                        rvCoupons.setAdapter(couponsAdapter);
                        if(couponsAdapter.getItemCount() == 0) {
                            ((TextView) findViewById(R.id.tv_title_coupons)).setText("No tienes cupones activos");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().equals(""))
                            Toast.makeText(CouponActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    private void addCoupon(String code){
        HashMap<String, String> params = new LinkedHashMap<>();
        params.put("code", code);
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest(
                getString(R.string.add_coupons, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        ApiResponse res = (ApiResponse) response;
                        coupons.add(res.getData().getCoupon());
                        couponsAdapter.setCoupons(coupons);
                        couponsAdapter.notifyItemInserted(coupons.size());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().equals(""))
                            Toast.makeText(CouponActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));

    //<------ GETTERS & SETTERS ------------------------------------------------------------------->

    }

    //<----- ACTIONS ------------------------------------------------------------------------------>
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.tv_btn_exchange:
                String code = ((EditText)findViewById(R.id.et_coupon)).getText().toString();
                if(!code.isEmpty())
                    addCoupon(code);
                else
                    Toast.makeText(this, "Ingrese un codigo", Toast.LENGTH_SHORT).show();
                break;
            case R.id.iv_back_coupons:
                finish();
        }
    }
}
