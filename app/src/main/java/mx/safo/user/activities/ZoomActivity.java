package mx.safo.user.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import mx.safo.user.R;

/**
 * Created by karen on 21/06/17.
 */

public class ZoomActivity extends AppCompatActivity {

    private static final String EXTRA_MAP_URL = "cadeci.mapUrl";
    private static final String TAG = "cadeci.congressMap";
    private Context mContext;

    public static Intent newIntent(Context packageContext, String mapUrl) {
        Intent i = new Intent(packageContext, ZoomActivity.class);
        i.putExtra(EXTRA_MAP_URL, mapUrl);
        return i;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom);

        mContext = this;

        Picasso.with(mContext)
                .load(getIntent().getStringExtra(EXTRA_MAP_URL))
                .resize(1232, 720)
                .into((ImageView) findViewById(R.id.image));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
        }
    }
}