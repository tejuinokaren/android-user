package mx.safo.user.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.DataPage;
import mx.safo.user.models.Trip;
import mx.safo.user.utils.MyReceiver;
import mx.safo.user.utils.Repository;

import static mx.safo.user.activities.HistoryListActivity.DELETE_HISTORY;

public class HistoryListActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int DELETE_HISTORY = 6192;

    RvHistoryAdapter mAdapter;
    private Repository.PaginationRefreshRecycler rvHistory;
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout swipeRefresh;
    private int page = 1;
    private List<Trip> mHistories = new ArrayList();
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_history);

        mContext = this;

        rvHistory = (Repository.PaginationRefreshRecycler) findViewById(R.id.rv_history);
        rvHistory.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RvHistoryAdapter(mContext, mHistories);
        rvHistory.setAdapter(mAdapter);
        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        rvHistory.setList(mHistories)
                .addPagination(8, new Trip().setType(Trip.TAG_PROGRESS), new Repository.PaginationRefreshRecycler.PaginationListener() {
                    @Override
                    public void onLoadNextPage(int pageNumber) {
                        getHistory(pageNumber);
                    }
                })
                .addSwipeRefresh(swipeRefresh, new Repository.PaginationRefreshRecycler.SwipeRefreshListener() {
                    @Override
                    public void onRequestData() {
                        getHistory(page);
                    }
                });
        getHistory(page);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(),new IntentFilter("broadCastName"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(MyReceiver.getInstance());
    }

    //<------ API --------------------------------------------------------------------------------->
    private void getHistory(Integer page){
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.get_history, BuildConfig.API_URL).concat("?page=").concat(page.toString()),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                null,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        DataPage page = response.getData().getDataPage();
                        rvHistory.paginationLoadComplete(page.getLastPage(), page.getTrips());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        rvHistory.stopRefresh();
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(HistoryListActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    //<------ ACTIONS ----------------------------------------------------------------------------->
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.iv_back_history:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == DELETE_HISTORY) {
            if(resultCode == RESULT_OK) {
                int deletedPos = data.getIntExtra("position", 0);
                mHistories.remove(deletedPos);
                mAdapter.notifyItemRemoved(deletedPos);
            }
        }
    }
}

//<------ ADAPTER ----------------------------------------------------------------------------->
class RvHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    List<Trip> mValues;

    boolean noRecords = false;

    RvHistoryAdapter(Context context, List<Trip> mValues) {
        Collections.reverse(mValues);
        this.mContext = context;
        this.mValues = mValues;
    }

    class RvHistoryVH extends RecyclerView.ViewHolder {

        private TextView driverName;
        private TextView driverCar;
        private TextView driverPlates;
        private TextView date;
        private TextView paymentStatus;
        private ImageView driverImage;
        private ImageView mapImage;
        private RelativeLayout driverWrapper;

        RvHistoryVH(View itemView) {
            super(itemView);
            driverName = (TextView) itemView.findViewById(R.id.tv_driver_name_history);
            driverCar = (TextView) itemView.findViewById(R.id.tv_motor_driver_history);
            driverPlates = (TextView) itemView.findViewById(R.id.tv_plates_driver_history);
            date = (TextView) itemView.findViewById(R.id.tv_date_history);
            driverImage = (ImageView) itemView.findViewById(R.id.iv_driver_history);
            paymentStatus = (TextView) itemView.findViewById(R.id.tv_payment_status_history);
            mapImage = (ImageView) itemView.findViewById(R.id.iv_map_history);
            driverWrapper = (RelativeLayout) itemView.findViewById(R.id.rl_driver_history);
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        }
    }

    List<Trip> getDataSet(){
        return mValues;
    }

    void append(List<Trip> items){
        mValues.addAll(items);
        notifyDataSetChanged();

        if(mValues.isEmpty()){
            noRecords = true;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case Trip.TAG_HISTORY:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.rv_history_item, parent, false);
                return new RvHistoryVH(view);
            case Trip.TAG_PROGRESS:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_progress_view, parent, false);
                return new ProgressViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_progress_view, parent, false);
                return new ProgressViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        Trip trip = mValues.get(position);
        switch (getItemViewType(position)) {
            case Trip.TAG_HISTORY:
                final RvHistoryVH historyHolder = (RvHistoryVH) viewHolder;
                historyHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onClickItem(viewHolder.getAdapterPosition());
                    }
                });
                if(trip.getDriver() != null && trip.getDriverVehicle() != null) {
                    try {
                        historyHolder.driverWrapper.setVisibility(View.VISIBLE);
                        historyHolder.driverCar.setText(trip.getDriverVehicle().getModel().getBrand().getTitle().concat(" " + trip.getDriverVehicle().getModel().getTitle()));
                        historyHolder.driverPlates.setText(trip.getDriverVehicle().getPlates());
                        Picasso.with(mContext)
                                .load(trip.getDriver().getImage())
                                .placeholder(R.drawable.profile_holder)
                                .into(historyHolder.driverImage);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                } else {
                    historyHolder.driverWrapper.setVisibility(View.GONE);
                }
                if(trip.getStartedAt() != null) {
                    historyHolder.date.setText(trip.getStartedAt());
                } else {
                    historyHolder.date.setText(trip.getRequestedAt());
                }
                Picasso.with(mContext)
                        .load(trip.getMapImage())
                        .error(R.drawable.rectangle_grey)
                        .into(historyHolder.mapImage);
                if(trip.getStatus().equalsIgnoreCase("Cancelado")) {
                    historyHolder.paymentStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                    historyHolder.paymentStatus.setText("Cancelado");
                } else if(trip.getPaymentStatus().equalsIgnoreCase("Pendiente")){
                    historyHolder.paymentStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                    historyHolder.paymentStatus.setText("Pendiente");
                } else if(trip.getPaymentStatus().equalsIgnoreCase("Completado")) {
                    historyHolder.paymentStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green));
                    historyHolder.paymentStatus.setText("Pagado");
                }
                break;
            case Trip.TAG_PROGRESS:
                final ProgressViewHolder progHolder = (ProgressViewHolder) viewHolder;
                progHolder.progressBar.setIndeterminate(true);
                break;
            default:
                final ProgressViewHolder defHolder = (ProgressViewHolder) viewHolder;
                defHolder.progressBar.setIndeterminate(true);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mValues.get(position).getType();
    }

    void onClickItem(int position){
        Intent i = new Intent(mContext, HistoryDetailActivity.class);
        i.putExtra("History", mValues.get(position));
        i.putExtra("position", position);
        ((HistoryListActivity) mContext).startActivityForResult(i, DELETE_HISTORY);
    }

    private String formatDate(String dateStr) {
        SimpleDateFormat sourceDateFormat = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss");
        Date date = null;
        try {
            date = sourceDateFormat.parse(dateStr);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            return day + " de " + month + " de " + year ;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
