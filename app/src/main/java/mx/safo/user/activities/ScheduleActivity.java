package mx.safo.user.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;

import java.util.Calendar;

import mx.safo.user.R;
import mx.safo.user.adapters.TimeRVAdapter;
import mx.safo.user.models.Schedule;
import mx.safo.user.utils.MyReceiver;

import static mx.safo.user.utils.NotificationScheduleReceiver.MY_ACTION;

public class ScheduleActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvThumb;
    private TextView tvTime;
    private TextView tvTitleStatus;
    private TextView tvStatus;
    private RelativeLayout rlSetTime;

    private Integer hours = 0;
    private Integer minutes = 0;
    private boolean isAm = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_schedule);

        RecyclerView rvHours = (RecyclerView) findViewById(R.id.rv_hour_schedule);
        RecyclerView rvMinutes = (RecyclerView) findViewById(R.id.rv_minutes_schedule);
        tvTime = (TextView) findViewById(R.id.tv_time_schedule);
        tvStatus = (TextView) findViewById(R.id.tv_status_schedule);
        rlSetTime = (RelativeLayout) findViewById(R.id.rl_hour_minute);
        tvTitleStatus = (TextView) findViewById(R.id.tv_title_status_schedule);

        tvThumb = (TextView) findViewById(R.id.tv_thumb_schedule);

        setRecyclerViews(rvHours, true);
        setRecyclerViews(rvMinutes, false);

        hours = (Integer.MAX_VALUE / 2) % 12;
        minutes = ((Integer.MAX_VALUE / 2) % 12) * 5;

        Schedule schedule  =  new Gson().fromJson(getSharedPreferences("schedule", MODE_PRIVATE).getString("alarm", null), Schedule.class);
        setTimeTV();
        if(schedule != null && schedule.isActive()) {
            activeSchedule();
            tvTime.setText(schedule.getTextHour());
            rvHours.getLayoutManager().scrollToPosition(schedule.getHour().get(Calendar.HOUR));
            rvMinutes.getLayoutManager().scrollToPosition(schedule.getHour().get(Calendar.MINUTE));
            hours = schedule.getHour().get(Calendar.HOUR);
            minutes = schedule.getHour().get(Calendar.MINUTE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(),new IntentFilter("broadCastName"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(MyReceiver.getInstance());
    }

    //<------ INIT VIEWS -------------------------------------------------------------------------->
    private void setRecyclerViews(RecyclerView rv, final boolean isHours) {
        final SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(rv);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv.setLayoutManager(layoutManager);
        rv.setAdapter(new TimeRVAdapter(isHours));

        Resources r = getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, r.getDisplayMetrics());
        layoutManager.scrollToPositionWithOffset(Integer.MAX_VALUE / 2, (r.getDisplayMetrics().widthPixels / 2) - px);
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    Long position = (Long.valueOf(layoutManager.findFirstVisibleItemPosition()) + Long.valueOf(layoutManager.findLastVisibleItemPosition())) / 2;
                    if (!isHours) {
                        minutes = (position.intValue() % 12) * 5;
                    } else {
                        hours = position.intValue() % 12 + 1;

                    }
                    setTimeTV();
                }
            }
        });

    }

    private void setTimeTV() {
        tvTime.setText(String.format("%02d", hours) + ":" + String.format("%02d", minutes) + (isAm ? " am" : " pm"));
    }

    //<------ FUNCTIONS --------------------------------------------------------------------------->
    private void activeSchedule(){
        rlSetTime.setVisibility(View.VISIBLE);

        tvTitleStatus.setText("Recordatorio activado");
        YoYo.with(Techniques.FadeIn).duration(600).playOn(tvTitleStatus);

        tvTime.setVisibility(View.VISIBLE);

        tvStatus.setText("Desactivar");
        YoYo.with(Techniques.FadeIn).duration(300).playOn(tvStatus);

        ((TextView) findViewById(R.id.tv_text_status_schedule)).setText("Te recordaresmos a esta hora para que pidas tu Safo");
        YoYo.with(Techniques.FadeIn).duration(600).playOn( ((TextView) findViewById(R.id.tv_text_status_schedule)));

        YoYo.with(Techniques.FadeInRight).duration(300).playOn( ((Button) findViewById(R.id.btn_done_shcedule)));
        ((Button) findViewById(R.id.btn_done_shcedule)).setVisibility(View.VISIBLE);
    }

    private void deactivateSchedule(){
        rlSetTime.setVisibility(View.GONE);

        tvTitleStatus.setText("Recordatorio desactivado");
        YoYo.with(Techniques.FadeIn).duration(600).playOn(tvTitleStatus);

        tvTime.setVisibility(View.INVISIBLE);

        tvStatus.setText("Activar");
        YoYo.with(Techniques.FadeIn).duration(300).playOn(tvStatus);

        ((TextView) findViewById(R.id.tv_text_status_schedule)).setText("Al activar un recordatorio, te notificaremos a la hora que seleccionaste para que no olvides pedir tu Safo");
        YoYo.with(Techniques.FadeIn).duration(600).playOn( ((TextView) findViewById(R.id.tv_text_status_schedule)));

        YoYo.with(Techniques.FadeOutLeft).duration(300).playOn( ((Button) findViewById(R.id.btn_done_shcedule)));

        SharedPreferences.Editor editor = getSharedPreferences("schedule", MODE_PRIVATE).edit();
        editor.remove("alarm");
        editor.apply();

    }

    private void scheduleNotification(Schedule schedule) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent notificationIntent = new Intent(MY_ACTION);
        notificationIntent.putExtra("schedule", new Gson().toJson(schedule));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, notificationIntent, 0);
        Calendar calendar = Calendar.getInstance();
        int timeInMillis = schedule.getMillis().intValue();
        calendar.add(Calendar.MILLISECOND, timeInMillis);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        timeToSetAlarm(timeInMillis);

    }

    private void setSchedule() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR, hours);
        c.set(Calendar.MINUTE, minutes);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.AM_PM, isAm ? Calendar.AM : Calendar.PM);
        if(c.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())
            c.add(Calendar.DAY_OF_YEAR, 1);

        Schedule schedule = new Schedule(c, true, (String) tvTime.getText());
        SharedPreferences.Editor editor = getSharedPreferences("schedule", MODE_PRIVATE).edit();
        editor.putString("alarm", new Gson().toJson(schedule));
        editor.apply();
        scheduleNotification(schedule);
    }

    private void  timeToSetAlarm(int millis){
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("¡Recordatorio Listo!")
                .setMessage("Tu recordatorio se guardó correctamente.")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

        int minutes = millis / 60000;
        if(minutes > 60) {
            int hour = minutes / 60;
            minutes =  minutes % 60;
            minutes++;
            if(hour <= 1) {
                builder.setMessage("Tu recordatorio se guardó correctamente.\n" +
                        "Queda una hora y " + minutes + " minutos ");
                if(minutes <= 1)
                    builder.setMessage("Tu recordatorio se guardó correctamente.\n" +
                            "Queda una hora y un minuto ");
            } else {
                builder.setMessage("Tu recordatorio se guardó correctamente.\n" +
                        "Quedan " + hour + " horas y " + minutes + " minutos ");
                if(minutes <= 1)
                    builder.setMessage("Tu recordatorio se guardó correctamente.\n" +
                            "Quedan " + hour + " horas y un minuto ");
            }
        }else {
            minutes++;
            if(minutes <= 1)
                builder.setMessage("¡Queda muy poco tiempo!. Es mejor pedir tu Safo en este momento");
            else
                builder.setMessage("Tu recordatorio se guardó correctamente.\n" +
                        "Quedan " + minutes + " minutos ");
        }
        AlertDialog alert = builder.create();
        alert.show();

    }

    //<------ ACTIONS ----------------------------------------------------------------------------->
    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btn_done_shcedule:
                setSchedule();
              break;
            case R.id.fl_switch_schedule:
                YoYo.with(Techniques.Tada).duration(200).playOn(tvThumb);
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(tvThumb.getWidth(), tvThumb.getHeight(), isAm ? Gravity.END : Gravity.START);
                int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2.5f, getResources().getDisplayMetrics());
                params.setMargins(px, px, px, px);
                tvThumb.setLayoutParams(params);
                isAm = !isAm;
                if (isAm) {
                    tvThumb.setText("AM");
                    tvThumb.setBackground(getResources().getDrawable(R.drawable.shape_circular_corners_white));
                    tvThumb.setTextColor(ContextCompat.getColor(ScheduleActivity.this, R.color.green));
                } else {
                    tvThumb.setText("PM");
                    tvThumb.setBackground(getResources().getDrawable(R.drawable.shape_circular_corners_green));
                    tvThumb.setTextColor(ContextCompat.getColor(ScheduleActivity.this, R.color.white));
                }
                setTimeTV();
                break;

            case R.id.iv_back_schedule:
                finish();
                break;
            case R.id.tv_status_schedule:
                if(tvStatus.getText().equals("Activar")) {
                    activeSchedule();
                } else if(tvStatus.getText().equals("Desactivar")){
                    deactivateSchedule();
                }
                break;

        }
    }
}
