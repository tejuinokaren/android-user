package mx.safo.user.activities;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.AuthToken;
import mx.safo.user.models.User;
import mx.safo.user.utils.AlarmReceiver;
import mx.safo.user.utils.FormValidator;
import mx.safo.user.utils.MyReceiver;
import mx.safo.user.utils.SessionManager;

import static android.content.DialogInterface.*;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private User user;
    private Context mCtx;
    private CallbackManager callbackManager;
    private boolean isLogin = true;
    private int getUserTries = 0;

    public static Intent newIntent(Context packageContext) {
        Intent i =  new Intent(packageContext, LoginActivity.class);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        //Init variables
        mCtx = this;

        if (getSharedPreferences("user", MODE_PRIVATE).getBoolean("isLogged", false)) {
            user = User.fromSharedPref(this);
            if (isProfileCompleted()) {
                checkAuthToken(this);
                startActivity(HomeMapActivity.newIntent(LoginActivity.this).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            } else {
                Intent i = new Intent(this, ProfileActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("profileIncomplete", false);
                startActivity(i);
                finish();
            }
        }

        hideKeyboard();
        setUpViews();
        initFacebookSignIn();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(),new IntentFilter("broadCastName"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(MyReceiver.getInstance());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //<------ INTI VIEWS -------------------------------------------------------------------------->
    private void setUpViews() {
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.gdl));
        findViewById(R.id.til_conf_password_login).setVisibility(isLogin ? View.GONE : View.VISIBLE);
        findViewById(R.id.til_phone_login).setVisibility(isLogin ? View.GONE : View.VISIBLE);

        YoYo.with(Techniques.FadeIn).duration(500).playOn(findViewById(R.id.btn_sign_up_login));
        YoYo.with(Techniques.FadeIn).duration(500).playOn(findViewById(R.id.tv_done_login));

        ((Button) findViewById(R.id.btn_sign_up_login)).setText(isLogin ? "Crear cuenta" : "Ya tengo cuenta");
        ((TextView) findViewById(R.id.tv_done_login)).setText(isLogin ? "Ingresar" : "¡Crear mi cuenta ahora!");
    }

    private void initFacebookSignIn(){
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginResult.getAccessToken().getUserId();
                String token = loginResult.getAccessToken().getToken();
                loginFacebook(token);

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });


    }

    //<------ API --------------------------------------------------------------------------------->
    private void login() {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("grant_type", "password");
        params.put("client_id", BuildConfig.API_CLIENT_ID);
        params.put("client_secret", BuildConfig.API_CLIENT_SECRET);
        params.put("username", getEditTextValue(R.id.et_email_login));
        params.put("password", getEditTextValue(R.id.et_password_login));
        params.put("scope", "");

        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest(
                getString(R.string.api_oauth_token, BuildConfig.API_URL),
                AuthToken.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_FOR_ACCESS_TOKEN,
                params,
                new Response.Listener<AuthToken>() {
                    @Override
                    public void onResponse(AuthToken authToken) {
                        boolean userExisted = true;

                        user = User.fromSharedPref(LoginActivity.this);
                        if(user == null){
                            userExisted = false;
                            user = new User();
                        }

                        user.setAuthToken(authToken);
                        SharedPreferences.Editor editor = getSharedPreferences("user", MODE_PRIVATE).edit();
                        editor.putString("user", new Gson().toJson(user));
                        editor.putBoolean("isLogged", true);
                        editor.apply();

                        if(!userExisted) getProfile();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                            Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    private void loginFacebook(String token) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("grant_type", "social");
        params.put("client_id", BuildConfig.API_CLIENT_ID);
        params.put("client_secret", BuildConfig.API_CLIENT_SECRET);
        params.put("network", "facebook");
        params.put("access_token", token);

        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest(
                getString(R.string.api_oauth_token, BuildConfig.API_URL),
                AuthToken.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_FOR_ACCESS_TOKEN,
                params,
                new Response.Listener<AuthToken>() {
                    @Override
                    public void onResponse(AuthToken authToken) {
                        boolean userExisted = true;

                        user = User.fromSharedPref(LoginActivity.this);
                        if(user == null){
                            userExisted = false;
                            user = new User();
                        }

                        user.setAuthToken(authToken);
                        SharedPreferences.Editor editor = getSharedPreferences("user", MODE_PRIVATE).edit();
                        editor.putString("user", new Gson().toJson(user));
                        editor.putBoolean("isLogged", true);
                        editor.apply();

                        if(!userExisted) getProfile();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().equals(""))
                            Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    private void getProfile(){
        getUserTries++;
        String token = FirebaseInstanceId.getInstance().getToken() != null ? FirebaseInstanceId.getInstance().getToken() : "";

        Map<String, String> params = new LinkedHashMap<>();
        params.put("fcm_token", token);
        params.put("platform", "android");

        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest(
                getString(R.string.api_get_user, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        user = response.getData().getUser();

                        if(user.getIsDriver() == 0) { // 0 = user, 1 = driver
                            user.setAuthToken(User.fromSharedPref(LoginActivity.this).getAuthToken());

                            SharedPreferences.Editor editor = getSharedPreferences("user", MODE_PRIVATE).edit();
                            editor.putString("user", new Gson().toJson(user));
                            editor.putBoolean("isLogged", true);
                            editor.apply();
                            checkAuthToken(LoginActivity.this);
                            if (isProfileCompleted()) {
                                startActivity(HomeMapActivity.newIntent(LoginActivity.this).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            } else {
                                Intent i = new Intent(LoginActivity.this, ProfileActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.putExtra("profileIncomplete", false);
                                startActivity(i);
                            }
                            finish();
                        } else {
                            user = null;
                            SessionManager.getInstance(mCtx).clearLocalData();
                            getUserTries = 0;
                            Toast.makeText(mCtx, getResources().getString(R.string.error_wrong_user_role), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(getUserTries < 3) getProfile();
                        else showUserDataErrorDialog();
                    }
                }, true
        ));
    }

    private void register(){
        if (!getEditTextValue(R.id.et_conf_password_login).equals(getEditTextValue(R.id.et_password_login))) {
            ((EditText) findViewById(R.id.et_conf_password_login)).setError("Las contraseñas no coinciden");
            return;
        }
        final String token = FirebaseInstanceId.getInstance().getToken() != null ? FirebaseInstanceId.getInstance().getToken() : "";

        Map<String, String> params = new LinkedHashMap<>();
        params.put("email", getEditTextValue(R.id.et_email_login));
        params.put("password", getEditTextValue(R.id.et_password_login));
        params.put("phone", getEditTextValue(R.id.et_phone_login));
        params.put("platform", "android");
        params.put("fcm_token", token);

        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.api_register_user, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_EMPTY,
                params,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        login();
                        Toast.makeText(LoginActivity.this, "Usuario creado correctamente", Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    private void refreshAuthToken(){
        Map<String, String> params = new LinkedHashMap<>();
        params.put("grant_type", "refresh_token");
        params.put("client_id", BuildConfig.API_CLIENT_ID);
        params.put("client_secret", BuildConfig.API_CLIENT_SECRET);
        params.put("refresh_token", user.getAuthToken().getRefreshToken());
        params.put("scope", "");

        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.api_oauth_token, BuildConfig.API_URL),
                AuthToken.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_FOR_ACCESS_TOKEN,
                params,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        AuthToken authToken = (AuthToken) response;
                        user = User.fromSharedPref(mCtx);
                        if (user == null) {
                            user = new User();
                        }
                        user.setAuthToken(authToken);
                        SharedPreferences.Editor editor = mCtx.getSharedPreferences("user", MODE_PRIVATE).edit();
                        editor.putString("user", new Gson().toJson(user));
                        editor.putBoolean("isLogged", true);
                        editor.apply();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, true
        ));
    }

    //<------ FUNCTIONS --------------------------------------------------------------------------->
    public void checkAuthToken(final Context context){
        if (context.getSharedPreferences("user", MODE_PRIVATE).getBoolean("isLogged", false)) {
            user = User.fromSharedPref(context);
            Calendar dateRequest = user.getAuthToken().getRequestTime();
            dateRequest.getTime();
            dateRequest.add(Calendar.SECOND, user.getAuthToken().getExpiresIn());
            if (Calendar.getInstance().getTime().after(dateRequest.getTime())) refreshAuthToken();
            else {
                long expiresIn = (dateRequest.getTimeInMillis() - Calendar.getInstance().getTimeInMillis()) - 600000;
                setAlarm(expiresIn);
            }
        }
    }

    private void showUserDataErrorDialog() {
        getUserTries = 0;
        new AlertDialog.Builder(this)
                .setTitle("Safo")
                .setMessage("Ocurrió un error al obtener los datos")
                .setPositiveButton("Intentar de nuevo", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getProfile();
                    }

                })
                .setNegativeButton("Cancelar", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences.Editor editor = getSharedPreferences("user", MODE_PRIVATE).edit();
                        editor.remove("user");
                        editor.putBoolean("isLogged", false);
                        editor.apply();
                    }
                })
                .show();
    }

    private String getEditTextValue(int id) {
        return ((EditText) findViewById(id)).getText().toString();
    }

    private void setAlarm(long when){
        AlarmManager manager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent        intent  = new Intent(this, AlarmReceiver.class);
        PendingIntent pIntent = PendingIntent.getBroadcast(this, 1, intent,  PendingIntent.FLAG_CANCEL_CURRENT);
        manager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + when , pIntent);
    }

    private boolean isProfileCompleted(){
        if (user.getName().isEmpty() || user.getLastName().isEmpty()
                || user.getBirthDate().isEmpty() || user.getEmail().isEmpty() || user.getPhone().isEmpty())
            return false;
        else return true;
    }

    private void hideKeyboard(){
        findViewById(R.id.content_view_login).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        });
    }

    //<------ ACTIONS ----------------------------------------------------------------------------->
    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_fb_login:
                LoginManager.getInstance().logInWithReadPermissions(this, Collections.singletonList("email"));
                break;
            case R.id.btn_done_login:
                submit();
                break;
            case R.id.btn_sign_up_login:
                isLogin = !isLogin;
                setUpViews();
                break;
            case R.id.tv_forgot_pass:
                startActivity(new Intent(LoginActivity.this, RemeberPasswordActivity.class));
                break;
        }
    }

    public void submit() {
        EditText username = (EditText) findViewById(R.id.et_email_login);
        EditText password = (EditText) findViewById(R.id.et_password_login);

        FormValidator validator = new FormValidator(getResources());
        validator.addInput(username, FormValidator.INPUT_TYPE_EMAIL, true);
        validator.addInput(password, FormValidator.INPUT_TYPE_ALL, true, 4, 20);

        if (!isLogin) {
            EditText confPassword = (EditText) findViewById(R.id.et_conf_password_login);
            EditText phone = (EditText) findViewById(R.id.et_phone_login);

            validator.addInput(confPassword, FormValidator.INPUT_TYPE_ALL, true, 4, 20);
            validator.addInput(phone, FormValidator.INPUT_TYPE_NUMBER, true, 10, 10);
        }

        if (validator.isValid()) {
            if (isLogin) login();
            else {
                /*String link1 = "<a href=\"http://www.google.com\">http://www.google.com</a>";
                String message = "Es necesario aceptar los términos y condiciones para utilizar el servicio. \n"+link1;
                Spanned myMessage = Html.fromHtml(message);
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setMessage(myMessage)
                        .setCancelable(false)
                        .setPositiveButton("Acepto", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                register();
                            }
                        })
                        .setNegativeButton("No acepto", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();*/
                final Dialog dialog = new Dialog(mCtx,
                        android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_accept_politics);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.CENTER;

                // set the custom dialog components - text, image and button
                Button politicsLink = (Button) dialog.findViewById(R.id.politicsLink);
                politicsLink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse("http://safoapp.com/assets/pdf/terminos_y_condiciones.pdf"));
                        startActivity(i);
                    }
                });

                Button button1 = (Button) dialog.findViewById(R.id.dialogButtonOK);
                button1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        register();
                    }
                });

                Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonCancel);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

                dialog.getWindow().setAttributes(lp);
            }
        }
    }

}
