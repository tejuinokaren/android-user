package mx.safo.user.activities;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.adapters.CardsRvAdapter;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.CardM;
import mx.safo.user.utils.MyReceiver;

public class CardListActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int NEW_CARD = 444;
    public static final String EXTRA_NEW_CARD = "card";
    private ArrayList<CardM> cards;
    private RecyclerView rvCards;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_card_list);
        rvCards = (RecyclerView) findViewById(R.id.rv_card_list);
        getCards();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(),new IntentFilter("broadCastName"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(MyReceiver.getInstance());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NEW_CARD && resultCode == RESULT_OK) {
            if (cards == null)
                cards = new ArrayList<>();
            CardM card = (CardM) data.getExtras().getSerializable("card");
            cards.add(card);
            setRv();
        }
    }

    //<------ API --------------------------------------------------------------------------------->
    private void getCards() {
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.get_cards, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                null,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        cards = (ArrayList) response.getData().getCards();
                        setRv();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CardListActivity.this, "Error al obtener sus Tarjetas. Intente de nuevo más tarde", Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    //<------ FUNCTIONS --------------------------------------------------------------------------->
    private void setRv() {
        layoutManager = new LinearLayoutManager(this);

        CardsRvAdapter cardsRvAdapter = new CardsRvAdapter(cards, this, true);
        rvCards.setAdapter(cardsRvAdapter);
        rvCards.setLayoutManager(layoutManager);
        ((SimpleItemAnimator) rvCards.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    //<------ ACTIONS ----------------------------------------------------------------------------->
    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.iv_back_card_list:
                finish();
                break;
            case R.id.iv_add_card_list:
                startActivityForResult(new Intent(this, AddCardActivity.class), NEW_CARD);
                break;
        }
    }
}