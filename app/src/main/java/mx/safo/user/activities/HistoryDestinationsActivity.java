package mx.safo.user.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.safo.user.R;
import mx.safo.user.models.Waypoint;

/**
 * Created by dianakarenms@gmail.com on 04/11/17.
 */

public class HistoryDestinationsActivity extends AppCompatActivity {

    private static final String EXTRA_WAYPOINTS = "safo.user.waypoints";
    private RecyclerView mDestinationsRecycler;
    private Context mContext;

    public static Intent newIntent(Context packageContext, List<Waypoint> waypoints) {
        Intent i = new Intent(packageContext, HistoryDestinationsActivity.class);
        i.putExtra(EXTRA_WAYPOINTS, (Serializable) waypoints);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_history_destinations);

        mContext = this;

        List<Waypoint> waypointsList = (List<Waypoint>) getIntent().getSerializableExtra(EXTRA_WAYPOINTS);
        mDestinationsRecycler = (RecyclerView) findViewById(R.id.destinations_recycler);
        mDestinationsRecycler.setAdapter(new HistoryDestinationsAdapter(mContext, waypointsList));
        mDestinationsRecycler.setLayoutManager(new LinearLayoutManager(mContext));
    }

    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.iv_back_history_detail:
                setResult(RESULT_CANCELED);
                onBackPressed();
                break;
        }
    }
}

class HistoryDestinationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<Waypoint> mItems = new ArrayList<>();

    public HistoryDestinationsAdapter(Context context, List<Waypoint> list) {
        mContext = context;
        mItems = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.rv_history_destination_item, parent, false);
        return new HistoryDestinationsHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Waypoint item = mItems.get(position);
        HistoryDestinationsHolder destinationsHolder = (HistoryDestinationsHolder) holder;
        destinationsHolder.mDestinationLabel.setText(item.getOriginAddress());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    private class HistoryDestinationsHolder extends RecyclerView.ViewHolder {
        private TextView mDestinationLabel;

        public HistoryDestinationsHolder(View view) {
            super(view);
            mDestinationLabel = (TextView) view.findViewById(R.id.destination_label);
        }
    }
}
