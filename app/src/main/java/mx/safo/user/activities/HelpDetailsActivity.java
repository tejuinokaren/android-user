package mx.safo.user.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import mx.safo.user.R;

public class HelpDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_details);
        TextView tvHelpText = (TextView) findViewById(R.id.tv_text_help);
        if(getIntent().hasExtra("case"))
        switch (getIntent().getIntExtra("case",3)){
            case 0:
                ((TextView)findViewById(R.id.tv_questions_help_details)).setText(getResources().getTextArray(R.array.help_array)[0].toString().concat("\n"));
                tvHelpText.setText("Todas las tarjetas mexicanas e internacionales de" +
                        "crédito VISA, MasterCard y AMEX, así como tarjetas " +
                        "de débito mexicanas Banamex, HSBC, Inbursa y " +
                        "Santander. Safo® no almacena datos de tarjetahabientes.");
                break;
            case 1:
                ((TextView)findViewById(R.id.tv_questions_help_details)).setText(getResources().getTextArray(R.array.help_array)[1].toString().concat("\n"));
                tvHelpText.setText("Cuando calculas un viaje se hace con base en una ruta definida y su estimacion de tiempo, pero tu chofer " +
                        "podría tomar otra ruta en caso de ser necesario o tu podrías decidir cambiarla durante el trayecto. \n \n" +
                        "Esto generaría un mayor tiempo de trayecto por lo tanto un costo superior al estimado inicialmente.");
                break;
            case 2:
                ((TextView)findViewById(R.id.tv_questions_help_details)).setText(getResources().getTextArray(R.array.help_array)[2].toString().concat("\n"));
                tvHelpText.setText("No. En este momento sólo es posible realizar pagos" +
                        "en línea mediante las tarjetas mencionadas" +
                        "anteriormente. Un chofernunca te pedirá dinero en " +
                        "efectivo.");
                break;
        }
    }

    @Override
    public void onClick(View v) {
        Integer id = v.getId();
        switch (id){
            case R.id.iv_back_help_details:
                finish();
                break;
        }
    }
}
