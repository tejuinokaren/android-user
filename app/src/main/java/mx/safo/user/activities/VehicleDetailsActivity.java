package mx.safo.user.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.Brand;
import mx.safo.user.models.Model;
import mx.safo.user.models.User;
import mx.safo.user.models.Vehicle;
import mx.safo.user.utils.FormValidator;
import mx.safo.user.utils.MyReceiver;
import mx.safo.user.utils.PrettyFormatDate;
import mx.safo.user.utils.Repository;

import static mx.safo.user.utils.Repository.isFormCompleted;
import static mx.safo.user.utils.Repository.setAllTextInputLayoutAnim;

public class VehicleDetailsActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener  {

    public static final String EXTRA_VEHICLE = "vehicle";
    private ArrayList<Model> models;
    private ArrayList<Brand> brands;
    private ArrayList<Integer> years;
    private Model selectedModel;
    private Brand selectedBrand;
    private Integer selectedYear;
    private TextView tvTitleActivity;
    private Boolean isEditing;
    private Vehicle vehicleEdit;
    private View mView;
    private Context mContext;
    private String mSelectedInsuranceDate;
    private ScrollView mScrollWrapper;
    private CheckBox mInsuranceCheckbox;

    public static Intent newIntent(Context packageContext, Vehicle vehicle) {
        Intent i = new Intent(packageContext, VehicleDetailsActivity.class);
        i.putExtra(EXTRA_VEHICLE, vehicle);
        return i;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_vehicle);

        mContext = this;
        tvTitleActivity = (TextView) findViewById(R.id.tv_title_details_vehicle);
        mScrollWrapper = (ScrollView) findViewById(R.id.wrapper_scrollview);
        findViewById(R.id.et_brand_details_vehicle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectBrand();
            }
        });
        findViewById(R.id.et_model_details_vehicle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectModel();
            }
        });
        findViewById(R.id.et_year_details_vehicle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectYear();
            }
        });
        mInsuranceCheckbox = (CheckBox) findViewById(R.id.insurance_checkbox);
        isEditing = false;

        mInsuranceCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    Repository.expand(findViewById(R.id.insurance_details_wrapper));
                    mScrollWrapper.postDelayed(new Runnable() {
                        public void run() {
                            mScrollWrapper.fullScroll(View.FOCUS_DOWN);
                        }
                    }, 300);
                } else {
                    Repository.collapse(findViewById(R.id.insurance_details_wrapper));
                }
            }
        });

        vehicleEdit = (Vehicle) getIntent().getSerializableExtra(EXTRA_VEHICLE);
        if(vehicleEdit != null) {
            isEditing = true;
            mInsuranceCheckbox.setChecked(true);
            mInsuranceCheckbox.setEnabled(false);
            mView = getWindow().getDecorView().getRootView();
            setAllTextInputLayoutAnim(mView, R.id.form_layout, false);

            tvTitleActivity.setText("Editar auto");
            selectedBrand = vehicleEdit.getModel().getBrand();
            selectedModel = vehicleEdit.getModel();
            setEditTextValue(R.id.et_model_details_vehicle, vehicleEdit.getModel().getTitle());
            setEditTextValue(R.id.et_brand_details_vehicle, vehicleEdit.getModel().getBrand().getTitle());
            setEditTextValue(R.id.et_title_details_vehicle, vehicleEdit.getTitle());
            setEditTextValue(R.id.et_plates_details_vehicle, vehicleEdit.getPlates());
            setEditTextValue(R.id.et_year_details_vehicle, vehicleEdit.getYear().toString());
            setEditTextValue(R.id.et_color_details_vehicle, vehicleEdit.getColor());
            setEditTextValue(R.id.et_insurance_company_details_vehicle, vehicleEdit.getInsuranceCompany());
            setEditTextValue(R.id.et_insurance_number_details_vehicle, vehicleEdit.getInsuranceNumber());
            try {
                setEditTextValue(R.id.et_insurance_due_date_details_vehicle, PrettyFormatDate.format(vehicleEdit.getInsuranceExpiration(), "yyyy-MM-dd", "dd MMMM yyyy"));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            setAllTextInputLayoutAnim(mView, R.id.form_layout, true);
        } else {
            findViewById(R.id.et_models_wrapper).setVisibility(View.GONE);
        }

        getBrands();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(),new IntentFilter("broadCastName"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(MyReceiver.getInstance());
    }

    //<--------- API ------------------------------------------------------------------------------>
    private void uploadVehicle() {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("brand_id", String.valueOf(selectedBrand.getId()));
        params.put("model_id", String.valueOf(selectedModel.getId()));
        params.put("title", getEditTextValue(R.id.et_title_details_vehicle));
        params.put("plates", getEditTextValue(R.id.et_plates_details_vehicle));
        params.put("year", getEditTextValue(R.id.et_year_details_vehicle));
        params.put("color", getEditTextValue(R.id.et_color_details_vehicle));
        params.put("insurance_company", getEditTextValue(R.id.et_insurance_company_details_vehicle));
        params.put("insurance_number", getEditTextValue(R.id.et_insurance_number_details_vehicle));
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest(
                getString(R.string.upload_vehicle, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        Intent i = new Intent();
                        ApiResponse res = (ApiResponse) response;
                        Vehicle vehicle = res.getData().getVehicle();
                        i.putExtra(VehicleListActivity.EXTRA_NEW_VEHICLE, vehicle);
                        VehicleDetailsActivity.this.setResult(RESULT_OK, i);
                        Toast.makeText(VehicleDetailsActivity.this, "El auto se guardó exitosamente", Toast.LENGTH_SHORT).show();

                        // Add this vehicle to locally stored user
                        User user = User.fromSharedPref(mContext);
                        user.addVehicle(vehicle);
                        SharedPreferences.Editor editor = mContext.getSharedPreferences("user", MODE_PRIVATE).edit();
                        editor.putString("user", new Gson().toJson(user));
                        editor.apply();

                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().equals(""))
                            Toast.makeText(VehicleDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    private void editVehicle(){
        Map<String, String> params = new LinkedHashMap<>();
        params.put("id", String.valueOf(vehicleEdit.getId()));
        params.put("brand_id", String.valueOf(selectedBrand.getId()));
        params.put("model_id", String.valueOf(selectedModel.getId()));
        params.put("title", getEditTextValue(R.id.et_title_details_vehicle));
        params.put("plates", getEditTextValue(R.id.et_plates_details_vehicle));
        params.put("year", getEditTextValue(R.id.et_year_details_vehicle));
        params.put("color", getEditTextValue(R.id.et_color_details_vehicle));
        params.put("insurance_expiration", mSelectedInsuranceDate);
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest(
                getString(R.string.edit_vehicle, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        Intent i = new Intent();
                        VehicleDetailsActivity.this.setResult(RESULT_OK, i);
                        Toast.makeText(VehicleDetailsActivity.this, "El auto se ha actualizado exitosamente", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().equals(""))
                            Toast.makeText(VehicleDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    private void getBrands() {
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest(
                getString(R.string.catalogs, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        ApiResponse res = (ApiResponse) response;
                        brands = (ArrayList) res.getData().getBrands();
//                        selectBrand();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().equals(""))
                            Toast.makeText(VehicleDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    private void getModels() {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("brand_id", String.valueOf(selectedBrand.getId()));
        String url = getString(R.string.get_models, BuildConfig.API_URL) + "?brand_id=" + String.valueOf(selectedBrand.getId());
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest(
                url,
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        ApiResponse res = (ApiResponse) response;
                        models = (ArrayList) res.getData().getModels();
//                        selectModel();
                        if(findViewById(R.id.et_models_wrapper).getVisibility() == View.GONE) {
                            Repository.expand(findViewById(R.id.et_models_wrapper));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().equals(""))
                            Toast.makeText(VehicleDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    //<--------- FUNCTIONS ------------------------------------------------------------------------>

    private void validateVehicle() {

        FormValidator validator = new FormValidator(getResources());
        validator.addInput(getEditText(R.id.et_title_details_vehicle), FormValidator.INPUT_TYPE_TEXT, true, 2, 50);
        validator.addInput(getEditText(R.id.et_brand_details_vehicle), FormValidator.INPUT_TYPE_ALL, true);
        validator.addInput(getEditText(R.id.et_model_details_vehicle), FormValidator.INPUT_TYPE_ALL, true);
        validator.addInput(getEditText(R.id.et_plates_details_vehicle), FormValidator.INPUT_TYPE_TEXT, true, 6, 10);
        validator.addInput(getEditText(R.id.et_color_details_vehicle), FormValidator.INPUT_TYPE_TEXT, true, 2,20);
        validator.addInput(getEditText(R.id.et_year_details_vehicle), FormValidator.INPUT_TYPE_NUMBER, true);
        validator.addInput(getEditText(R.id.et_insurance_company_details_vehicle), FormValidator.INPUT_TYPE_TEXT, false);
        validator.addInput(getEditText(R.id.et_insurance_number_details_vehicle), FormValidator.INPUT_TYPE_TEXT, false);

        if (validator.isValid()) {
            View view = getWindow().getDecorView().getRootView();
            if (validator.isValid()) {
                if(mInsuranceCheckbox.isChecked()) {
                    if(isEditing) editVehicle();
                    else uploadVehicle();
                } else {
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this)
                            .setMessage("Necesitas contar con un seguro vehicular para poder usar el servicio.")
                            .setCancelable(false)
                            .setNegativeButton("Volver", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    android.support.v7.app.AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        }
    }

    private void selectBrand() {
        if (brands == null) {
            getBrands();
            return;
        }

        final String[] data = new String[brands.size()];
        for (int i = 0; i < data.length; i++) {
            data[i] = brands.get(i).getTitle();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(data, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                selectedBrand = brands.get(which);
                getEditText(R.id.et_brand_details_vehicle).setError(null);
                setEditTextValue(R.id.et_brand_details_vehicle, selectedBrand.getTitle());
                setEditTextValue(R.id.et_model_details_vehicle, null);
                selectedModel = null;
                models = null;
                getModels();
            }
        });
        builder.create().show();
    }

    private void selectModel() {
        if (selectedBrand == null) {
            getEditText(R.id.et_brand_details_vehicle).setError("Seleccione una marca");
            return;
        }

        if (models == null) {
            getModels();
            return;
        }

        final String[] data = new String[models.size()];
        for (int i = 0; i < data.length; i++) {
            data[i] = models.get(i).getTitle();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(data, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                selectedModel = models.get(which);
                setEditTextValue(R.id.et_model_details_vehicle, selectedModel.getTitle());
                getEditText(R.id.et_model_details_vehicle).setError(null);
            }
        });
        builder.create().show();
    }

    private void selectYear() {
        if (years == null) {
            setYears();
        }

        final String[] data = new String[years.size()];
        for (int i = 0; i < data.length; i++) {
            data[i] = years.get(i).toString();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(data, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                selectedYear = years.get(which);
                setEditTextValue(R.id.et_year_details_vehicle, selectedYear.toString());
                getEditText(R.id.et_year_details_vehicle).setError(null);
            }
        });
        builder.create().show();
    }

    private void setYears() {
        years = new ArrayList<>();
        for (int i = (Calendar.getInstance().get(Calendar.YEAR) + 1); i >= 1950; i--) {
            years.add(i);
        }
    }

    private void setEditTextValue(int id, String text) {
        ((EditText) findViewById(id)).setText(text);
    }

    private String getEditTextValue(int id) {
        return ((EditText) findViewById(id)).getText().toString();
    }

    private EditText getEditText(int id) {
        return (EditText) findViewById(id);
    }

    //<--------- ACTIONS -------------------------------------------------------------------------->
    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btn_done_details_vehicle:
                validateVehicle();
                break;
            case R.id.iv_back_details_vehicle:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.et_insurance_due_date_details_vehicle:
                Calendar c = Calendar.getInstance();
                DatePickerDialog dialogDatePicker = new DatePickerDialog(mContext, this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));//llama al calendario
                Calendar aux = Calendar.getInstance();;
                dialogDatePicker.getDatePicker().setMinDate(aux.getTimeInMillis());
                aux.add(Calendar.YEAR, +10);
                dialogDatePicker.getDatePicker().setMaxDate(aux.getTimeInMillis());
                dialogDatePicker.show();
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        try {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, monthOfYear);
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setEditTextValue(R.id.et_insurance_due_date_details_vehicle, PrettyFormatDate.format(c.getTime(), "dd MMMM yyyy"));
            mSelectedInsuranceDate = PrettyFormatDate.format(c.getTime(), "yyyy-MM-dd");
        } catch (Exception e) {
            Toast.makeText(mContext, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
