package mx.safo.user.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;
import mx.safo.user.models.CardM;
import mx.safo.user.models.User;
import mx.safo.user.models.Vehicle;
import mx.safo.user.utils.PrettyFormatDate;
import mx.safo.user.utils.BitmapScaler;
import mx.safo.user.utils.FormValidator;
import mx.safo.user.utils.MyReceiver;
import mx.safo.user.utils.Repository;

import static mx.safo.user.utils.Repository.isFormCompleted;
import static mx.safo.user.utils.Repository.setAllTextInputLayoutAnim;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final int TAKE_PHOTO = 27;
    private static final int PICK_PHOTO = 669;
    private static final int CAMERA_PERMISSIONS = 1234;
    private static final int REQUEST_ADD_VEHICLE = 1;
    private static final int REQUEST_ADD_CARD = 2;
    private static final int REQUEST_READ_CONTACTS = 100;
    private static final int CONTACT_PICKER_RESULT = 3;

    private User user;

    private Bitmap selectedImage;
    private String selectedBirthDate;
    private Uri imageUri;
    private View mView;
    private boolean mIsProfileCompleted = true;
    private Context mContext;
    private Vehicle mVehicle;
    private CardM mCard;
    private int mSelectedContact;
    private AlertDialog mChangePasswordDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        
        mContext = this;
        
        hideKeyboard();

        mView = getWindow().getDecorView().getRootView();
        setAllTextInputLayoutAnim(mView, R.id.form_layout, false);

        user = User.fromSharedPref(this);
        initProfile();

        if(getIntent().hasExtra("profileIncomplete")) {//if the user has not completed his profile
            mIsProfileCompleted = getIntent().getBooleanExtra("profileIncomplete", false);
            (findViewById(R.id.iv_back_profile)).setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyReceiver.getInstance().setCurrentActivity(this);
        registerReceiver(MyReceiver.getInstance(),new IntentFilter("broadCastName"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(MyReceiver.getInstance());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == TAKE_PHOTO) {
            try {
                Bitmap imageBM = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                imageBM = checkRotation(getRealPathFromURI(imageUri), imageBM);
                setProfilePhoto(imageBM);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (resultCode == RESULT_OK && requestCode == PICK_PHOTO) {
            try {
                Bitmap imageBM = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                imageBM = checkRotation(getRealPathFromURI(data.getData()), imageBM);
                setProfilePhoto(imageBM);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if(resultCode == RESULT_OK && requestCode == REQUEST_ADD_VEHICLE) {
            mVehicle = (Vehicle) data.getSerializableExtra(VehicleListActivity.EXTRA_NEW_VEHICLE);
            showSetVehicle();
        } else if(resultCode == RESULT_OK && requestCode == REQUEST_ADD_CARD) {
            mCard = (CardM) data.getSerializableExtra(CardListActivity.EXTRA_NEW_CARD);
            showSetCard();
        } else if(resultCode == RESULT_OK && requestCode == CONTACT_PICKER_RESULT) {
            loadContactsList(data);
        }
    }

    private void loadContactsList(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex).replace(" ", "");;
            name = cursor.getString(nameIndex);
            Log.d("Contact Info", "name " + name + ", phoneNo " + phoneNo);
            // Set the value to the textviews
            if(mSelectedContact == 1) {
                ((EditText) findViewById(R.id.et_contact_name_1)).setText(name);
                ((EditText) findViewById(R.id.et_contact_phone_1)).setText(phoneNo);
            } else if(mSelectedContact == 2){
                ((EditText) findViewById(R.id.et_contact_name_2)).setText(name);
                ((EditText) findViewById(R.id.et_contact_phone_2)).setText(phoneNo);
            }
            mSelectedContact = 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSetVehicle() {
        ((ImageView) findViewById(R.id.vehicle_check_img)).setImageResource(R.drawable.checked);
        findViewById(R.id.vehicle_profile_rl).setClickable(false);
    }

    private void showSetCard() {
        ((ImageView) findViewById(R.id.card_check_img)).setImageResource(R.drawable.checked);
        findViewById(R.id.card_profile_rl).setClickable(false);
    }

    //<------ INTI VIEWS -------------------------------------------------------------------------->
    private void initProfile() {
        selectedBirthDate = user.getBirthDate();

        Picasso.with(this).load(user.getImage().replace("http://", "https://")).error(R.drawable.profile_holder).into((ImageView) findViewById(R.id.riv_profile));
        setEditTextValue(R.id.et_name_profile, user.getName());
        setEditTextValue(R.id.et_lastname_profile, user.getLastName());
        setEditTextValue(R.id.et_email_profile, user.getEmail());
        setEditTextValue(R.id.et_phone_profile, user.getPhone());
        setEditTextValue(R.id.et_contact_name_1, user.getContact1Name());
        setEditTextValue(R.id.et_contact_phone_1, user.getContact1Phone());
        setEditTextValue(R.id.et_contact_name_2, user.getContact2Name());
        setEditTextValue(R.id.et_contact_phone_2, user.getContact2Phone());


        if(user.getVehicles() != null) {
            if (!user.getVehicles().isEmpty()) {
                showSetVehicle();
            }
        }

        if(user.getCards() != null) {
            if (!user.getCards().isEmpty()) {
                showSetCard();
            }
        }

        try {
            setEditTextValue(R.id.et_birthdate_profile, PrettyFormatDate.format(user.getBirthDate(), "yyyy-MM-dd", "dd MMMM yyyy"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        setAllTextInputLayoutAnim(mView, R.id.form_layout, true);
    }

    private void setEditTextValue(int id, String text) {
        ((EditText) findViewById(id)).setText(text);
    }

    private void setProfilePhoto(Bitmap b) {
        if (b.getWidth() > 1000) {
            b = BitmapScaler.scaleToFitWidth(b, 1000);
        } else if (b.getHeight() > 1000) {
            b = BitmapScaler.scaleToFitHeight(b, 1000);
        }

        selectedImage = b;
        ((ImageView) findViewById(R.id.riv_profile)).setImageBitmap(selectedImage);
    }

    private String getEditTextValue(int id) {
        return ((EditText) findViewById(id)).getText().toString();
    }

    private EditText getEditText(int id) {
        return (EditText) findViewById(id);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    //<------ FUNCTIONS --------------------------------------------------------------------------->
    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public Bitmap checkRotation(String photoPath, Bitmap bitmap) {
        ExifInterface ei;
        try {
            ei = new ExifInterface(photoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    return rotateImage(bitmap, 90);

                case ExifInterface.ORIENTATION_ROTATE_180:
                    return rotateImage(bitmap, 180);

                case ExifInterface.ORIENTATION_ROTATE_270:
                    return rotateImage(bitmap, 270);

                case ExifInterface.ORIENTATION_NORMAL:

                default:
                    return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    private String getBase64(Bitmap image) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        return "data:image/jpeg;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public void takePhoto() {
        if (checkForPermission()) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "Nueva foto");
            values.put(MediaStore.Images.Media.DESCRIPTION, "Toma una nueva fotografía");
            imageUri = getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, TAKE_PHOTO);
        }
    }

    private Boolean checkForPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_PERMISSIONS);
        } else {
            return true;
        }
        return false;
    }

    private void hideKeyboard(){
        findViewById(R.id.ll_content_view_profile).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        });
    }

    private void noVehiclesDialog(){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Aún no tienes agregado un vehículo")
                .setMessage("Es necesario primero agregar un vehículo a tu cuenta")
                .setPositiveButton("Agregar ahora", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startActivity(new Intent(ProfileActivity.this, VehicleListActivity.class));
                    }
                })
                .setNegativeButton("Más tarde", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
//                        startActivity(HomeMapActivity.newIntent(ProfileActivity.this));
                    }
                });
        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void noCardsDialog(){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Aún no tienes agregada una forma de pago")
                .setMessage("Es necesario primero agregar una forma de pago a tu cuenta")
                .setPositiveButton("Agregar ahora", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startActivity(new Intent(ProfileActivity.this, AddCardActivity.class));
                    }
                })
                .setNegativeButton("Más tarde", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getVehicles();
                    }
                });
        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    //<------ API --------------------------------------------------------------------------------->
    private void updateProfile() {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("name", getEditTextValue(R.id.et_name_profile));
        params.put("last_name", getEditTextValue(R.id.et_lastname_profile));
        params.put("email", getEditTextValue(R.id.et_email_profile));
        params.put("birth_date", selectedBirthDate);
        params.put("phone", getEditTextValue(R.id.et_phone_profile));
        if (selectedImage != null) params.put("image", getBase64(selectedImage));
        params.put("contact1_name", getEditTextValue(R.id.et_contact_name_1));
        params.put("contact1_phone", getEditTextValue(R.id.et_contact_phone_1));
        params.put("contact2_name", getEditTextValue(R.id.et_contact_name_2));
        params.put("contact2_phone", getEditTextValue(R.id.et_contact_phone_2));


        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.api_update_profile, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        List<CardM> cards = user.getCards();
                        List<Vehicle> vehicles = user.getVehicles();
                        user = response.getData().getUser();
                        user.setAuthToken(User.fromSharedPref(ProfileActivity.this).getAuthToken());
                        user.setVehicles(vehicles);
                        user.setCards(cards);
                        if(mVehicle != null) {
                            user.addVehicle(mVehicle);
                        }
                        if(mCard != null) {
                            user.addCard(mCard);
                        }

                        SharedPreferences.Editor editor = getSharedPreferences("user", MODE_PRIVATE).edit();
                        editor.putString("user", new Gson().toJson(user));
                        editor.putBoolean("isLogged", true);
                        editor.apply();

                        Toast.makeText(ProfileActivity.this, "Se actualizaron los datos con éxito", Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK);

                        if(mIsProfileCompleted) {
                            getCards();
                        } else {
                            startActivity(HomeMapActivity.newIntent(ProfileActivity.this).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            finish();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(ProfileActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    private void getCards() {
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.get_cards, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                null,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        if(response.getData().getCards().isEmpty()) {
                            noCardsDialog();
                        } else {
                            getVehicles();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, true
        ));
    }

    private void getVehicles() {
        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.get_vehicles, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.GET,
                ApiClient.Header.HEADER_DEFAULT,
                null,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        if(response.getData().getVehicles().isEmpty()) {
                            noVehiclesDialog();
                        } else {
                            finish();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }, true
        ));
    }

    public void changePass(String password, String newPassword){
        Map<String, String> params = new LinkedHashMap<>();
        params.put("password", password);
        params.put("new_password", newPassword);

        ApiClient.getInstance(this).addToRequestQueue(new ApiClient.GsonRequest<>(
                getString(R.string.change_pass, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        Toast.makeText(mContext, "Contraseña cambiada", Toast.LENGTH_SHORT).show();
                        mChangePasswordDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(!error.getMessage().isEmpty())
                            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }, true
        ));
    }

    //<------ ACTIONS ----------------------------------------------------------------------------->
    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.vehicle_profile_rl:
                startActivityForResult(VehicleDetailsActivity.newIntent(mContext, null), REQUEST_ADD_VEHICLE);
                break;
            case R.id.card_profile_rl:
                startActivityForResult(AddCardActivity.newIntent(mContext), REQUEST_ADD_CARD);
                break;
            case R.id.btn_done_profile:
                validateProfile();
                break;

            case R.id.et_birthdate_profile:
                Calendar c = Calendar.getInstance();
                DatePickerDialog dialogDatePicker = new DatePickerDialog(ProfileActivity.this, this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));//llama al calendario
                Calendar aux = Calendar.getInstance();
                aux.add(Calendar.YEAR, -16);
                dialogDatePicker.getDatePicker().setMaxDate(aux.getTimeInMillis());
                dialogDatePicker.show();
                break;

            case R.id.iv_back_profile:
                if (user.getName().isEmpty() || user.getLastName().isEmpty() || user.getBirthDate().isEmpty())
                    validateProfile();
                else
                    setResult(RESULT_CANCELED);
                onBackPressed();
                break;

            case R.id.riv_profile:
                final CharSequence[] items = {"Tomar foto", "Escoger de galería", "Cancelar"};
                AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                builder.setTitle("¡Cambiar foto!");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                        if (items[item].equals("Tomar foto")) {
//                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                                    addRecipeActivity.startActivityForResult(intent, 120);
                            takePhoto();
                        } else if (items[item].equals("Escoger de galería")) {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, PICK_PHOTO);
                        } else if (items[item].equals("Cancelar")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
                break;
            case R.id.infoBtn:
                new android.support.v7.app.AlertDialog.Builder(this)
                        .setCancelable(false)
                        .setTitle("Por tu seguridad")
                        .setMessage("Safo se comunicará con tu contacto en caso de emergencia.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(R.drawable.info)
                        .create()
                        .show();
                break;
            case R.id.search_contacts_1:
            case R.id.search_contacts_2:
                try {
                    mSelectedContact = (id == R.id.search_contacts_1) ? 1 : 2;
                    if(checkContactsPermission()) {
                        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                        startActivityForResult(intent, CONTACT_PICKER_RESULT);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.change_password_profile_rl:
                mChangePasswordDialog = new AlertDialog.Builder(mContext).create();
                final View dialogView = LayoutInflater
                        .from(mContext)
                        .inflate(R.layout.dialog_change_password, null);
                mChangePasswordDialog.setView(dialogView);
                dialogView.findViewById(R.id.btn_done_change_password).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String newPassword = Repository.getFieldText((EditText) dialogView.findViewById(R.id.et_new_password_change_password));
                        EditText repeatPassField = ((EditText) dialogView.findViewById(R.id.et_repeat_password_change_password));
                        String repeatPassword = Repository.getFieldText(repeatPassField);
                        if (!newPassword.equals(repeatPassword)) {
                            repeatPassField.setError("Las contraseñas no coinciden");
                        } else {
                            changePass(Repository.getFieldText((EditText) dialogView.findViewById(R.id.et_current_password_change_password)), newPassword);
                        }

                    }
                });
                mChangePasswordDialog.show();

                break;
        }
    }

    private void validateProfile() {
        FormValidator validator = new FormValidator(getResources());
        validator.addInput(getEditText(R.id.et_name_profile), FormValidator.INPUT_TYPE_TEXT, true, 2, 50);
        validator.addInput(getEditText(R.id.et_lastname_profile), FormValidator.INPUT_TYPE_TEXT, true);
        validator.addInput(getEditText(R.id.et_email_profile), FormValidator.INPUT_TYPE_EMAIL, true);
        validator.addInput(getEditText(R.id.et_birthdate_profile), FormValidator.INPUT_TYPE_TEXT, true);
        validator.addInput(getEditText(R.id.et_phone_profile), FormValidator.INPUT_TYPE_NUMBER, true, 10, 10);
        validator.addInput(getEditText(R.id.et_contact_name_1), FormValidator.INPUT_TYPE_TEXT, true, 2, 50);
        validator.addInput(getEditText(R.id.et_contact_phone_1), FormValidator.INPUT_TYPE_NUMBER, true, 10, 10);

        View view = getWindow().getDecorView().getRootView();
        if (validator.isValid()) {
            if (isFormCompleted(view, R.id.form_layout)) {
                updateProfile();
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    takePhoto();
                break;
            case REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    try {
                        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                        startActivityForResult(intent, CONTACT_PICKER_RESULT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(mContext,
                            "Esta función requiere que se habiliten los permisos de contactos.",
                            Toast.LENGTH_SHORT)
                            .show();
                }
            }
            break;

        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        try {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, monthOfYear);
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setEditTextValue(R.id.et_birthdate_profile, PrettyFormatDate.format(c.getTime(), "dd MMMM yyyy"));
            selectedBirthDate = PrettyFormatDate.format(c.getTime(), "yyyy-MM-dd");
        } catch (Exception e) {
            Toast.makeText(ProfileActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean checkContactsPermission() {
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext,
                    Manifest.permission.READ_CONTACTS)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new android.support.v7.app.AlertDialog.Builder(mContext)
                        .setTitle(R.string.title_contact_permission)
                        .setMessage(R.string.text_contact_permission)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions((Activity) mContext,
                                        new String[]{Manifest.permission.READ_CONTACTS},
                                        REQUEST_READ_CONTACTS);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions((Activity) mContext,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        REQUEST_READ_CONTACTS);
            }
            return false;
        } else {
            return true;
        }
    }
}
