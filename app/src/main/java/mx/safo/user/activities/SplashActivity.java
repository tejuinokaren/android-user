package mx.safo.user.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import mx.safo.user.utils.SessionManager;
import mx.safo.user.utils.UserLocationManager;

/**
 * Created by karen on 21/06/17.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SessionManager.setSplashFlag(true);
        UserLocationManager
                .getInstance(this)
                .getLastUserLocation(null);
    }
}