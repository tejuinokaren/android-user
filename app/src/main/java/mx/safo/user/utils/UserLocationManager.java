package mx.safo.user.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.LinkedHashMap;
import java.util.Map;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by karen on 06/11/17.
 */

public class UserLocationManager {

    // Instance for singleton
    private static UserLocationManager sInstance = null;
    private static UserLocationManager sLocation;
    public static Location sUserLocation;
    private static Boolean sIsAlertGpsShowing = false;
    private static Context _context;

    // Constants
    public static final String GOOGLE_DIRECTIONS_KEY = "AIzaSyDK55lj4AkNT6-4Zcs5_5Y_r3df-f7mFK4";
    public static final int MAP_PERMISSIONS = 779;

    public static UserLocationManager getInstance(Context context) {
        if (sInstance == null) {
            synchronized (UserLocationManager.class) {
                if (sInstance == null) {
                    sInstance = new UserLocationManager(context);
                    sLocation = sInstance;
                }
            }
        }
        return sInstance;
    }

    // Constructor
    private UserLocationManager(Context context){
        this._context = context;
    }

    public static void getLastUserLocation(View snakeBarContainer) {
        if(isGPSEnabled()) {
            if (checkForPermission(snakeBarContainer)) {
                LocationManager locationManager = (LocationManager) _context.getSystemService(LOCATION_SERVICE);
                sUserLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (sUserLocation != null && SessionManager.getInstance(_context).isLoggedIn()) {
                    updateUserLocation();
                }
            }
        }
        SessionManager.getInstance(_context).checkLogin();
    }

    public static Boolean isGPSEnabled() {
        LocationManager service = (LocationManager) _context.getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
        AlertDialog alertGPS = null;
        if(!enabled) {
            if (!sIsAlertGpsShowing) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_context);
                alertDialogBuilder
                        .setMessage("El GPS esta desactivado. Activarlo")
                        .setCancelable(false)
                        .setPositiveButton("Activar GPS", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sIsAlertGpsShowing = false;
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                _context.startActivity(callGPSSettingIntent);
                            }
                        });
                alertGPS = alertDialogBuilder.create();
                alertGPS.show();
                sIsAlertGpsShowing = true;
                return false;
            }
            return false;
        } else  {
            if(sIsAlertGpsShowing)
                alertGPS.cancel();
            return true;
        }
    }

    public static Boolean checkForPermission(final View snakeBarContainer) {

        if (ContextCompat.checkSelfPermission(_context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Permissions not granted, request for permissions except if splashScreen is showing
            if(!SessionManager.getInstance(_context).getSplashFlag()) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) _context, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    ActivityCompat.requestPermissions((Activity) _context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MAP_PERMISSIONS);
                    return false;
                } else {
                    ActivityCompat.requestPermissions((Activity) _context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MAP_PERMISSIONS);

                    if (snakeBarContainer != null) {
                        Snackbar snackbar = Snackbar.make(snakeBarContainer, "\n" + "Habilitar permisos desde configuraciones.", Snackbar.LENGTH_INDEFINITE)
                                .setDuration(1500)
                                .setAction("ok", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package", _context.getPackageName(), null);
                                        intent.setData(uri);
                                        _context.startActivity(intent);
                                    }
                                });
                        snackbar.setActionTextColor(_context.getResources().getColor(R.color.colorPrimary));
                        snackbar.show();
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_context);
                        alertDialogBuilder
                                .setMessage("Se requieren permisos de localización. Habilitar permisos desde configuraciones")
                                .setCancelable(false)
                                .setPositiveButton("Habilitar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        sIsAlertGpsShowing = false;
                                        Intent callGPSSettingIntent = new Intent(
                                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        _context.startActivity(callGPSSettingIntent);
                                    }
                                })
                                .create().show();
                    }

                    return false;
                }
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static void updateUserLocation() {
        if(sUserLocation != null) {
            Map<String, String> params = new LinkedHashMap<>();
            params.put("lat", String.valueOf(sUserLocation.getLatitude()));
            params.put("lng", String.valueOf(sUserLocation.getLongitude()));
            ApiClient.getInstance(_context).addToRequestQueue(new ApiClient.GsonRequest<>(
                    _context.getString(R.string.update_location, BuildConfig.API_URL),
                    ApiResponse.class,
                    Request.Method.POST,
                    ApiClient.Header.HEADER_DEFAULT,
                    params,
                    new Response.Listener<ApiResponse>() {
                        @Override
                        public void onResponse(ApiResponse response) {
                            Log.d("updatedUserLoc",
                                    "orign lat,lng: " + String.valueOf(sUserLocation.getLatitude())
                                            + "," + String.valueOf(sUserLocation.getLongitude()));
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // This error message doesn't require to be shown
                        }
                    }, false
            ));
        }

    }


}
