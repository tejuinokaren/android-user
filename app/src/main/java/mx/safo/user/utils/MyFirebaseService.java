package mx.safo.user.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.LinkedHashMap;
import java.util.Map;

import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.api.ApiClient;
import mx.safo.user.api.ApiResponse;

/**
 * Created by Moy on 11/05/17.
 */

public class MyFirebaseService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseService";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        if (!getSharedPreferences("user", MODE_PRIVATE).getBoolean("isLogged", false))
            return;

        updateToken(this);
    }

    public static void updateToken(Context context) {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + token);

        Map<String, String> params = new LinkedHashMap<>();
        params.put("fcm_token", token);
        params.put("platform", "android");

        ApiClient.getInstance(context).addToRequestQueue(new ApiClient.GsonRequest<>(
                context.getString(R.string.send_fcm_token, BuildConfig.API_URL),
                ApiResponse.class,
                Request.Method.POST,
                ApiClient.Header.HEADER_DEFAULT,
                params,
                new Response.Listener<ApiResponse>() {
                    @Override
                    public void onResponse(ApiResponse response) {
                        Log.d("FCM update", "Success");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("FCM update", "Fail");
                    }
                }, false
        ));
    }
}
