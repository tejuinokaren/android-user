package mx.safo.user.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import mx.safo.user.activities.HomeMapActivity;
import mx.safo.user.activities.LoginActivity;
import mx.safo.user.models.AuthToken;
import mx.safo.user.models.User;

import static android.content.Context.MODE_PRIVATE;
import static mx.safo.user.activities.HomeMapActivity.sDestination;
import static mx.safo.user.activities.HomeMapActivity.sGetDriverETA;
import static mx.safo.user.activities.HomeMapActivity.sGetNearDrivers;
import static mx.safo.user.activities.HomeMapActivity.sGetTripLocation;

/**
 * Created by karen on 06/11/17.
 */

public class SessionManager {

    // Instance for singleton
    private static SessionManager sInstance = null;

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "user";
//    private static final String PREF_NAME = "mx.tejuino.cadeci";

    // All Shared Preferences Keys (make variable public to access from outside)
    private static final String IS_LOGIN = "isLogged";

    // User values
    public static final String KEY_USER = "user";
    public static final String KEY_AUTH_TOKEN = "auth_token";
    public static long ONE_MINUTE_IN_MILLIS = 60000;//millisecs

    public static SessionManager sSession;
    public static User sUser;
    public static AuthToken sAuthToken;
    private static boolean sSplashFlag;

    public static SessionManager getInstance(Context context) {
        if (sInstance == null) {
            synchronized (SessionManager.class) {
                if (sInstance == null) {
                    sInstance = new SessionManager(context);
                    sSession = sInstance;
//                    sUser = sSession.loadUserFromPrefs();
//                    sAuthToken = sSession.loadTokenFromPrefs();
                }
            }
        }
        return sInstance;
    }

    // Constructor
    private SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = pref.edit();
    }

    public static void setSplashFlag(boolean splashFlag) {
        sSplashFlag = splashFlag;
    }

    public boolean getSplashFlag() {
        return sSplashFlag;
    }

    /**
     * Create login sSession
     * */
    public void createLoginSession(){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing sUser in pref
        Gson gson = new Gson();
        String json = gson.toJson(sUser);
        editor.putString(KEY_USER, json);

        json = gson.toJson(sAuthToken);
        editor.putString(KEY_AUTH_TOKEN, json);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check sUser login status
     * If false it will redirect sUser to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        Intent i;
        // Check login status
        if(!this.isLoggedIn()){
            // sUser is not logged in, redirect him to Login Activity
            i = LoginActivity.newIntent(_context);
        } else {
            // sUser is logged in, redirect him to Main Activity
            // and clean all previous activities
            i = HomeMapActivity.newIntent(_context);
        }

        // Closing all the Activities
        if(sSplashFlag) {
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            sSplashFlag = false;
        } else {
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Clear sSession details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        sDestination = null;
        clearLocalData();

        sGetTripLocation = false;
        sGetDriverETA = false;
        sGetNearDrivers = false;

        // After logout redirect sUser to Login Activity
        Intent i = new Intent(_context, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // Starting Login Activity
        _context.startActivity(i);
    }

    public void clearLocalData() {
        editor.clear();
        editor.apply();
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        Boolean isLoggedIn = pref.getBoolean(IS_LOGIN, false);
        return isLoggedIn;
    }
}
