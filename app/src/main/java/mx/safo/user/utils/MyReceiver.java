package mx.safo.user.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import mx.safo.user.activities.HomeMapActivity;
import mx.safo.user.activities.OrderDriverActivity;

/**
 * Created by moyhdez on 12/01/17.
 */

public class MyReceiver extends android.content.BroadcastReceiver {

    private static MyReceiver shared;
    private Activity activity;

    private MyReceiver() {
    }

    public static MyReceiver getInstance(){
        if(shared == null){
            shared = new MyReceiver();
        }

        return shared;
    }

    public void setCurrentActivity(Activity activity){
        this.activity = activity;
    }

    @Override
    public void onReceive(Context context1, Intent intent) {
        if (activity instanceof OrderDriverActivity)
            ((OrderDriverActivity)activity).getTrip();
        else if (activity instanceof HomeMapActivity)
            ((HomeMapActivity)activity).getLastTrip();
    }
}
