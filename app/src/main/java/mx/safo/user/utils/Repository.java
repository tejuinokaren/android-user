package mx.safo.user.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mx.safo.user.R;

/**
 * Created by karen on 07/09/17.
 */

public class Repository {

    private static Dialog dialogTransparent;

    //region Fields data treatment
    @NonNull
    public static String getFieldText(View v, int fieldId) {
        return ((EditText) v.findViewById(fieldId)).getText().toString();
    }

    @NonNull
    public static String getFieldText(EditText field) {
        return field.getText().toString();
    }

    /**
     * Requires solicitant EditTexts to have tag = tag_required to be tested.
     *
     * @param v View view = getWindow().getDecorView().getRootView();
     * @param layout
     * @return
     */
    public static boolean isFormCompleted(View v, int layout) {
        boolean allSet = true;
        ViewGroup group = (ViewGroup) v.findViewById(layout);
        String errorTxt = v.getResources().getString(R.string.required_field);
        List<View> allChildrenBFS = getAllChildrenBFS(group);

        for (int i = 0; i < allChildrenBFS.size(); i++) {
            View view = allChildrenBFS.get(i);
            if (view instanceof EditText) {
                if (view.getTag() != null) {
                    EditText field = (EditText) view;
                    if (field.getTag().toString().equals(v.getResources().getString(R.string.tag_required))) {
                        if (field.getText().toString().isEmpty()) {
                            field.setError(errorTxt);
                            allSet = false;
                        }
                    }
                }
            }
        }

        return allSet;
    }

    public static void setAllTextInputLayoutAnim(View v, int layout, boolean isEnabled) {
        ViewGroup group = (ViewGroup) v.findViewById(layout);
        List<View> allChildrenBFS = getAllChildrenBFS(group);

        for (int i = 0; i < allChildrenBFS.size(); i++) {
            View view = allChildrenBFS.get(i);
            if (view instanceof TextInputLayout) {
                TextInputLayout textInputLayout = (TextInputLayout) view;
                textInputLayout.setHintAnimationEnabled(isEnabled);
            }
        }
    }

    public static List<View> getAllChildrenBFS(View v) {
        List<View> visited = new ArrayList<View>();
        List<View> unvisited = new ArrayList<View>();
        unvisited.add(v);

        while (!unvisited.isEmpty()) {
            View child = unvisited.remove(0);
            visited.add(child);
            if (!(child instanceof ViewGroup)) continue;
            ViewGroup group = (ViewGroup) child;
            final int childCount = group.getChildCount();
            for (int i = 0; i < childCount; i++) unvisited.add(group.getChildAt(i));
        }

        return visited;
    }

    public static String formatPriceToMXN(double value) {
        return String.format("$%1$,.2f MXN", value);
    }

    public static String formatFolio(int value) {
        return String.format("%1$07d", value);
    }

    public static void openPdfURL(String url, Context ctx) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        ctx.startActivity(browserIntent);
    }
    //endregion

    //region Hardware
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    //endregion

    //region Animations
    public static void animCrossfade(View contentView) {
        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        contentView.setAlpha(0f);
        contentView.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        contentView.animate()
                .alpha(1f)
                .setDuration(400)
                .setListener(null);
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(targetHeight*2 / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
    //endregion

    //region Progress View
    public static void showProgressDialog(Context cxt) {
        hideProgressDialog();

        dialogTransparent = new Dialog(cxt, R.style.AppTheme_NoActionBar);
        dialogTransparent.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = LayoutInflater.from(cxt).inflate(
                R.layout.item_progress_view, null);
        dialogTransparent.getWindow().setBackgroundDrawableResource(
                R.color.transparent);
        dialogTransparent.setContentView(view);
        dialogTransparent.setCancelable(true);
        dialogTransparent.show();
    }

    public static void hideProgressDialog() {
        if (dialogTransparent != null) {
            if (dialogTransparent.isShowing())
                dialogTransparent.dismiss();
        }
    }
    //endregion

    //region RecyclerView with Pagination and SwipeRefresh
    /**
     * Creates a custom RecyclerView which easily supports Pagination and/or SwipeRefresh
     * for a generic List<T> of items and any number of columns in LayoutManager.
     *
     * Here are some key points to make a custom view inside an inner class...
     public static class MainClass {
        ....
        ....
        public class SubClassView extends LinearLayout {
            public SubClassView(Context context, AttributeSet attrs) {
            super(context, attrs);
            .....
        }
        ....
        ....
        }
     }

     The layout should be as follows:
     <view class = ".MainClass$SubClassView"
        android:layout_width="wrap_content"
        android:layout_height="match_parent"
        android:id="@+id/button"/>

     -Java class
     static is required
     constructor with AttributeSet is required (at least one)

     -XML file
     view tag (with lower case NOT View) is required
     class tag with the path to your inner class, using
     $ instead of "." before your SubClassView name
     * @param <T>
     *
     *
     *  **** IMPLEMENTATION EXAMPLE FOR FRAGMENTS ****

        private int mFirstPage = 1;
        private int mCurrentPage = 1;
        mPaginationRefreshRecycler = (Repository.PaginationRefreshRecycler) v.findViewById(R.id.list);
        mPaginationRefreshRecycler.setLayoutManager(new LinearLayoutManager(context));
        mPaginationRefreshRecycler.setAdapter(mAdapter);
        mPaginationRefreshRecycler.setList(mNewsList)
            .setPaginationCurrentPage(mCurrentPage)
            .addPagination(10, new Posts().setType(Posts.TAG_PROGRESS), new Repository.PaginationRefreshRecycler.PaginationListener() {
            @Override
            public void onLoadNextPage(int pageNumber) {
                mCurrentPage = pageNumber;
                loadNewsPosts(pageNumber);
            }
        })
        .addSwipeRefresh((SwipeRefreshLayout) v.findViewById(R.id.swiperefresh), new Repository.PaginationRefreshRecycler.SwipeRefreshListener() {
            @Override
            public void onRequestData() {
                loadNewsPosts(mFirstPage);
            }
        });

        loadNewsPosts(mFirstPage);
     */
    public static class PaginationRefreshRecycler<T> extends RecyclerView {
        // TODO: solve news > another fragment > news bug loading news twice
        private SwipeRefreshLayout mSwipeRefreshLayout;
        private int mPaginationTreshold;
        private PaginationRefreshRecycler mRecyclerView;
        private int mPageNumber = 1;
        private boolean isLastPage, isLoading;
        private List<T> mList;
        private T mProgressObject;
        private SwipeRefreshListener mSwipeRefreshListener;
        private PaginationListener mPaginationListener;
        private boolean mIsSwipeAction;

        public PaginationRefreshRecycler(Context context) {
            super(context);
            mRecyclerView = this;
        }

        public PaginationRefreshRecycler(Context context, @Nullable AttributeSet attrs) {
            super(context, attrs);
            mRecyclerView = this;
        }

        public PaginationRefreshRecycler(Context context, @Nullable AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            mRecyclerView = this;
        }

        public PaginationRefreshRecycler setList(List<T> tList) {
            this.mList = tList;
            return mRecyclerView;
        }

        /**
         * Set number of page to be shown. Required just for fragments.
         * @param paginationCurrentPage
         * @return
         */
        public PaginationRefreshRecycler setPaginationCurrentPage(int paginationCurrentPage) {
            mPageNumber = paginationCurrentPage;
            return this;
        }

        public void stopRefresh() {
            mSwipeRefreshLayout.setRefreshing(false);
        }

        public PaginationRefreshRecycler addSwipeRefresh(SwipeRefreshLayout swipeRefreshView, SwipeRefreshListener listener) {
            this.mSwipeRefreshListener = listener;

            // Swipe Refresh Layout
            mSwipeRefreshLayout = swipeRefreshView;
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (mList == null) {
                        Log.e("AutoRefresh", "List is null. Have you called setList() ?");
                    } else {
                        // Clear all recyclerview state
                        mIsSwipeAction = true;
                        mPageNumber = 1;
                        isLastPage = false;
                        isLoading = true;
                        mSwipeRefreshLayout.setEnabled(true);

                        // Get data from server
                        mSwipeRefreshListener.onRequestData();
                    }
                }
            });

            // On scrollListener: on REFRESH creates a boundary the first time top is reached on scroll
            // to make refresh available only after recycler is on top
            mRecyclerView.addOnScrollListener(recyclerViewOnScrollListener); // Pagination
            return mRecyclerView;
        }

        /**
         * Adds pagination feature to the recyclerView with a generic List<T> of items.
         *
         * @param treshold Number of items that should be available after lastVisibleItemPosition.
         *                 The greater the smoother, but too large numbers may cause lag on recyclerview. (recommended 10 items)
         * @param tProgressObject Progress object of type T.
         *                        Example: new Post().setType(Posts.TAG_PROGRESS);
         *                                  In which the TAG_PROGRESS is handled by the recyclersView Adapter
         *                                  to show the corresponding progressView as a new custom HeaderViewHolder.
         * @param listener After pagination is triggered, it requests to load nextPage, this request
         *                 must be handled by the user.
         * @return
         */
        public PaginationRefreshRecycler addPagination(int treshold, T tProgressObject, PaginationListener listener) {
            this.mProgressObject = tProgressObject;
            this.mPaginationListener = listener;
            mRecyclerView.mPaginationTreshold = treshold;

            // On scrollListener: on PAGINATION enables loading new items to ensure having at least
            // the mPaginationTreshold items available after last visible item
            mRecyclerView.addOnScrollListener(recyclerViewOnScrollListener); // Pagination

            return mRecyclerView;
        }

        public void paginationLoadComplete(int lastPageIndex, List<T> nextPageList) {
            // If is the last page avoid trying to load more products
            if(mPageNumber == lastPageIndex) {
                isLastPage = true;
            }

            // Remove progressView if exists (pagination)
            if(!mList.isEmpty()) {
                int lastItem = mList.size() - 1;
                //if (mList.get(lastItem) == mProgressObject) {
                    mList.remove(lastItem);
                //}
            }

            // Only if swipeRefresh was requested
            if(mSwipeRefreshLayout != null) {
                if(mIsSwipeAction) {
                    mList.clear();
                    mIsSwipeAction = false;
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            // Enable loading new data
            isLoading = false;
            mRecyclerView.getAdapter().notifyDataSetChanged();
            // Add next page list of items after progressView is erased
            mList.addAll(nextPageList);
            onLoadMore();
        }
        /**
         * Check if the nextPage is required
         */
        private void onLoadMore() {
            if((!isLoading && !isLastPage) &&
                    (mList.size() - mPaginationTreshold) < ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition()) {
                // Add progress bar "item" to mRecyclerView
                mList.add(mProgressObject);

                // Show progress view in adapter
                mRecyclerView.getAdapter().notifyItemInserted(mList.size() - 1);

                // Search for the next page
                mPageNumber++;
                isLoading = true;

                // Ask for more data to load
                mPaginationListener.onLoadNextPage(mPageNumber);
            }
        }

        /**
         * Scroll listener for the RecyclerView pagination
         */
        private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mList == null) {
                    Log.e("AutoRefresh", "List is null. Have you called setList() ?");
                } else {
                    try {
                        if (dy > 0) {
                            /** Recycle view Scrolling DOWN... Pagination */

                            onLoadMore(); // Load new data

                            mSwipeRefreshLayout.setEnabled(false);

                        } else if (dy < 0) {
                            /** Recycle view Scrolling UP... Make swipeRefresh available
                             * This validation enables swipe refresh only on after recyclerView top reached
                             * To avoid pulling down refresh immediately after top reached at an unwanted time*/
                            if (((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition() == 0)
                                mSwipeRefreshLayout.setEnabled(true);

                        }
                    } catch (NullPointerException ex) {
                        ex.printStackTrace();
                        Log.d("Repository", "mSwipeRefreshLayout is null");
                    }
                }
            }
        };

        public interface SwipeRefreshListener {
            void onRequestData();
        }

        public interface PaginationListener {
            void onLoadNextPage(int pageNumber);
        }
    }
    //endregion
}
