package mx.safo.user.utils;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.RemoteMessage;

import java.util.List;

import mx.safo.user.R;
import mx.safo.user.activities.HomeMapActivity;
import mx.safo.user.models.Schedule;

/**
 * Created by moyhdez on 29/12/16.
 */

public class NotificationPublisher {

    private static NotificationPublisher shared;
    private static final int HIDE_ORDER = 345;

    public static NotificationPublisher getInstance() {
        if (shared == null) {
            shared = new NotificationPublisher();
        }
        return shared;
    }

    private NotificationPublisher() {

    }

    public void showNotification(Context context, RemoteMessage remoteMessage) {

        if (remoteMessage.getData().get("type") != null) {

            if (isAppIsInBackground(context)) {
                Intent intent = HomeMapActivity.newIntent(context);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                if (remoteMessage.getData().get("type").equals("trip_accepted")){
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                    notificationManager.notify(0, buildNotification(context, pendingIntent,"Safo® aceptado","Tu chofer está en camino").build());
                }
//                else if(remoteMessage.getData().get("type").equals("change_trip_status")){
                    if(remoteMessage.getData().get("type").equals("trip_began")) {
                        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                        notificationManager.notify(0, buildNotification(context, pendingIntent, "Safo® en curso", "Estás en camino").build());

                    }
                    if(remoteMessage.getData().get("type").equals("trip_finished")) {
                        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                        notificationManager.notify(0, buildNotification(context, pendingIntent, "Safo® terminado", "Tu viaje ha concluído").build());
                    }
                    if(remoteMessage.getData().get("type").equals("trip_cancelled")) {
                        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                        notificationManager.notify(0, buildNotification(context, pendingIntent, "Safo® cancelado", "Tu chofer ha cancelado el viaje").build());
                    }
                    if(remoteMessage.getData().get("type").equals("trip_timeout")) {
                        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                        notificationManager.notify(0, buildNotification(context, pendingIntent, "Safo® cancelado", "No se encontró ningún chofer cercano").build());
                    }
                    if(remoteMessage.getData().get("type").equals("insurance_expiration")) {
                        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                        notificationManager.notify(0, buildNotification(context, pendingIntent, "Seguro próximo a expirar", "Manténte protegido renovando tu seguro automotriz").build());
                    }
                    if(remoteMessage.getData().get("type").equals("trip_arriving")) {
                        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                        notificationManager.notify(0, buildNotification(context, pendingIntent, "Safo® llegando", "Tu Safo® está por llegar.").build());
                    }
//                }
            } else {
                Intent intent = HomeMapActivity.newIntent(context);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                if(remoteMessage.getData().get("type").equals("insurance_expiration")) {
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                    notificationManager.notify(0, buildNotification(context, pendingIntent, "Seguro próximo a expirar", "Manténte protegido renovando tu seguro automotriz").build());
                }
                if(remoteMessage.getData().get("type").equals("trip_arriving")) {
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                    notificationManager.notify(0, buildNotification(context, pendingIntent, "Safo® llegando", "Tu conductor está por llegar.").build());
                }
                Intent i = new Intent("broadCastName");
                context.sendBroadcast(i);
            }
        }
    }

    public void showNotification(Context context, Schedule schedule) {
        Intent intent = HomeMapActivity.newIntent(context);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setContentTitle("Safo®")
                .setContentText("Recordatorio: Es hora de pedir tu Safo®")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    private NotificationCompat.Builder buildNotification(Context context, PendingIntent pendingIntent, String title, String content){

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(ContextCompat.getColor(context,R.color.colorPrimary))
                .setContentTitle(title)
                .setContentText(content)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        return notificationBuilder;
    }
}
