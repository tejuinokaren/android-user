package mx.safo.user.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by moises on 1/12/16.
 */

public class PrettyFormatDate {

    public static String format(String datetime) throws ParseException {
        return format(datetime, "yyyy-MM-dd HH:mm", "dd MMM yyyy");
    }

    public static String format(String datetime, String fromFormat, String toFormat) throws ParseException {
        SimpleDateFormat parser = new SimpleDateFormat(fromFormat);
        Date parsedDate = parser.parse(datetime);
        SimpleDateFormat formatter = new SimpleDateFormat(toFormat, new Locale("es", "MX"));
        return formatter.format(parsedDate);
    }

    public static String format(Date date, String format) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(format, new Locale("es", "MX"));
        return formatter.format(date);
    }
}
