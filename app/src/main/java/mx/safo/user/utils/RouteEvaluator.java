package mx.safo.user.utils;

import android.animation.TypeEvaluator;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by amal.chandran on 22/12/16.
 */

public class RouteEvaluator implements TypeEvaluator<LatLng> {
    @Override
    public LatLng evaluate(float t, LatLng startPoint, LatLng endPoint) {
        // Due to an unknown cause, the start point returns null at times...
        // this null check has been added to prevent app crashes
        if(startPoint != null && endPoint != null) {
            double lat = startPoint.latitude + t * (endPoint.latitude - startPoint.latitude);
            double lng = startPoint.longitude + t * (endPoint.longitude - startPoint.longitude);
            return new LatLng(lat, lng);
        } else {
            return null;
        }
    }
}
