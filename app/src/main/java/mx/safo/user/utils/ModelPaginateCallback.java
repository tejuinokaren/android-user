package mx.safo.user.utils;

/**
 * Created by alan on 5/09/16.
 */
public interface ModelPaginateCallback<Type>{

    public void onSuccess(ModelPaginate<Type> pagination);

    public void onError(Exception e);

}