package mx.safo.user.utils;

import android.content.res.Resources;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import mx.safo.user.R;

/**
 * Created by @alangvara on 19/08/16.
 */
public class FormValidator {

    public static final int INPUT_TYPE_TEXT = 1;
    public static final int INPUT_TYPE_EMAIL = 2;
    public static final int INPUT_TYPE_NUMBER = 3;
    public static final int INPUT_TYPE_ALL = 4;

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static final String NUMBER_PATTERN = "[0-9]+";

    private static final String TEXT_PATTERN = "[\\w\\s]+";
    private Resources resources;
    private List<FormInput> inputs;
    private Pattern emailPattern;
    private Pattern numberPatter;
    private Pattern textPattern;

    public FormValidator(Resources resources) {
        this.resources = resources;
        inputs = new ArrayList<>();
        emailPattern = Pattern.compile(EMAIL_PATTERN);
        numberPatter = Pattern.compile(NUMBER_PATTERN);
        textPattern = Pattern.compile(TEXT_PATTERN);
    }

    public String getText(int strResource) {
        return resources.getString(strResource);
    }

    public void addInput(EditText input, int type, boolean required , Integer min, Integer max) {
        inputs.add(new FormInput(input, type, required, min, max));
    }

    public void addInput(EditText input, int type, boolean required ) {
        inputs.add(new FormInput(input, type, required));
    }

    public boolean isValid(){
        boolean isValid = true;
        String textToValidate;

        for(FormInput input: inputs){
            if(input.required)
                if (!input.editText.getText().toString().isEmpty()){
                    textToValidate = input.editText.getText().toString();
                    if(input.min != null && input.max != null) {
                        if (input.editText.getText().toString().length() < input.min || input.max < input.editText.getText().toString().length()) {
                            input.editText.setError(sizeErrorMessage(input.min, input.max));
                            if (isValid) input.editText.requestFocus();
                            isValid = false;
                        } else validateTypes(input, textToValidate, isValid);
                    }else validateTypes(input, textToValidate, isValid);
                } else{
                    input.editText.setError(getText(R.string.formvalidator_error_required));
                    if (isValid) input.editText.requestFocus();
                    isValid = false;
                }
            if (isValid) input.editText.setError(null);
        }
        return  isValid;
    }

    private boolean validateTypes(FormInput input, String textToValidate, boolean isValid){
        switch (input.type) {
            case INPUT_TYPE_TEXT:
                if (!textPattern.matcher(textToValidate).matches()) {
                    input.editText.setError(getText(R.string.formvalidator_error_text));
                    return false;
                }
                break;
            case INPUT_TYPE_EMAIL:
                if (!emailPattern.matcher(input.editText.getText().toString()).matches()) {
                    input.editText.setError(getText(R.string.formvalidator_error_email));
                    if (isValid) input.editText.requestFocus();
                    return false;
                }
                break;
            case INPUT_TYPE_NUMBER:
                if (!numberPatter.matcher(input.editText.getText().toString()).matches()) {
                    input.editText.setError(getText(R.string.formvalidator_error_phone));
                    if (isValid) input.editText.requestFocus();
                    return false;
                } else input.editText.setError(null);
                break;
        }
        return true;
    }

    private String sizeErrorMessage(Integer min, Integer max){
        if(min == max) return min + " Caracteres";
        if(max == Integer.MAX_VALUE) return "Minimo " + min + " caracteres";
        if(min == 0 ) return "Maximo " + max + " caracteres";
        return min + "-" + max + " caracteres";
    }

    private class FormInput {
        public EditText editText;
        public boolean required;
        public int type;
        public Integer  min;
        public Integer max;

        public FormInput(EditText editText, int type, boolean required) {
            this.editText = editText;
            this.type = type;
            this.required = required;
            this.min = null;
            this.max = null;
        }

        public FormInput(EditText editText, int type, boolean required, Integer min, Integer max) {
            this.editText = editText;
            this.required = required;
            this.type = type;
            if(max == null) this.max = Integer.MAX_VALUE;
            else this.max = max;
            this.min = min;

        }
    }

}
