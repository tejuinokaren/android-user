package mx.safo.user.utils;

/**
 * Created by moyhdez on 19/12/16.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import mx.safo.user.models.Schedule;

import static android.content.Context.MODE_PRIVATE;

public class NotificationScheduleReceiver extends BroadcastReceiver {

    public static final String MY_ACTION = "schedule_action";
    private Schedule schedule;

    @Override
    public void onReceive(Context context, Intent intent) {
        schedule  =  new Gson().fromJson(context.getSharedPreferences("schedule", Context.MODE_PRIVATE).getString("alarm", null), Schedule.class);
        if(schedule != null && schedule.isActive())
            if (intent.getAction().equals(MY_ACTION)) {
                Schedule schedule = new Gson().fromJson(intent.getStringExtra("schedule"), Schedule.class);
                NotificationPublisher.getInstance().showNotification(context, schedule);
                schedule.setActive(false);
                SharedPreferences.Editor editor = context.getSharedPreferences("schedule", MODE_PRIVATE).edit();
                editor.remove("alarm");
                editor.apply();
            }

    }


}
