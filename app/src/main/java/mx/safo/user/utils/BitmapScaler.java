package mx.safo.user.utils;

import android.graphics.Bitmap;

/**
 * Created by moyhdez on 21/12/16.
 */

public class BitmapScaler {

    // scale and keep aspect ratio
    public static Bitmap scaleToFitWidth(Bitmap b, int width) {
        float factor = width / (float) b.getWidth();
        return Bitmap.createScaledBitmap(b, width, (int) (b.getHeight() * factor), true);
    }

    public static Bitmap scaleToFitSquare(Bitmap b, int size) {
        float factor = size / (float) b.getWidth();
        Bitmap scaledBitMap = Bitmap.createScaledBitmap(b, size, (int) (b.getHeight() * factor), true);

        if (scaledBitMap.getWidth() >= scaledBitMap.getHeight()) {
            return Bitmap.createBitmap(
                    scaledBitMap,
                    scaledBitMap.getWidth() / 2 - scaledBitMap.getHeight() / 2,
                    0,
                    scaledBitMap.getHeight(),
                    scaledBitMap.getHeight()
            );
        } else {
            return Bitmap.createBitmap(
                    scaledBitMap,
                    0,
                    scaledBitMap.getHeight() / 2 - scaledBitMap.getWidth() / 2,
                    scaledBitMap.getWidth(),
                    scaledBitMap.getWidth()
            );
        }
    }

    // scale and keep aspect ratio
    public static Bitmap scaleToFitHeight(Bitmap b, int height) {
        float factor = height / (float) b.getHeight();
        return Bitmap.createScaledBitmap(b, (int) (b.getWidth() * factor), height, true);
    }

    // scale and keep aspect ratio
    public static Bitmap scaleToFill(Bitmap b, int width, int height) {
        float factorH = height / (float) b.getWidth();
        float factorW = width / (float) b.getWidth();
        float factorToUse = (factorH > factorW) ? factorW : factorH;
        return Bitmap.createScaledBitmap(b, (int) (b.getWidth() * factorToUse),
                (int) (b.getHeight() * factorToUse), true);
    }

    // scale and don't keep aspect ratio
    public static Bitmap strechToFill(Bitmap b, int width, int height) {
        float factorH = height / (float) b.getHeight();
        float factorW = width / (float) b.getWidth();
        return Bitmap.createScaledBitmap(b, (int) (b.getWidth() * factorW),
                (int) (b.getHeight() * factorH), true);
    }
}