package mx.safo.user.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import java.util.LinkedHashMap;
import java.util.Map;


import mx.safo.user.BuildConfig;
import mx.safo.user.R;
import mx.safo.user.api.ApiClient;
import mx.safo.user.models.AuthToken;
import mx.safo.user.models.User;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Fabian on 29/05/17.
 */

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, Intent intent) {
        updateToken(context);
    }

    private void updateToken(final Context context){
        if (context.getSharedPreferences("user", MODE_PRIVATE).getBoolean("isLogged", false)) {
            User user = User.fromSharedPref(context);
            Map<String, String> params = new LinkedHashMap<>();
            params.put("grant_type", "refresh_token");
            params.put("client_id", BuildConfig.API_CLIENT_ID);
            params.put("client_secret", BuildConfig.API_CLIENT_SECRET);
            params.put("refresh_token", user.getAuthToken().getRefreshToken());
            params.put("scope", "");

            ApiClient.getInstance(context).addToRequestQueue(new ApiClient.GsonRequest<>(
                    context.getString(R.string.api_oauth_token, BuildConfig.API_URL),
                    AuthToken.class,
                    Request.Method.POST,
                    ApiClient.Header.HEADER_FOR_ACCESS_TOKEN,
                    params,
                    new Response.Listener<AuthToken>() {
                        @Override
                        public void onResponse(AuthToken response) {
                            User user = User.fromSharedPref(context);

                            if(user == null) user = new User();
                            user.setAuthToken(response);

                            SharedPreferences.Editor editor = context.getSharedPreferences("user", MODE_PRIVATE).edit();
                            editor.putString("user", new Gson().toJson(user));
                            editor.putBoolean("isLogged", true);
                            editor.apply();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }, true
            ));
        }
    }
}
