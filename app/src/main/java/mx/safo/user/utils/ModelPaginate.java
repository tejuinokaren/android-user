package mx.safo.user.utils;

import java.util.ArrayList;

/**
 * Created by alan on 26/09/16.
 */

public class ModelPaginate <Type> {

    private int total;
    private int perPage;
    private int currentPage;
    private int lastPage;
    private int from;
    private int to;
    private ArrayList<Type> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public ArrayList<Type> getData() {
        return data;
    }

    public void setData(ArrayList<Type> data) {
        this.data = data;
    }
}
