package mx.safo.user.api;

import com.google.gson.JsonArray;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tejuino on 7/09/17.
 */

public class DirectionsResponse {
    @SerializedName("error_message")
    @Expose
    private String errorMessage;
    @SerializedName("routes")
    @Expose
    private JsonArray routes = null;
    @SerializedName("status")
    @Expose
    private String status;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public JsonArray getRoutes() {
        return routes;
    }

    public void setRoutes(JsonArray routes) {
        this.routes = routes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
