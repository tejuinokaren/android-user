package mx.safo.user.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Fabian on 10/16/17.
 */

public class ApiError implements Serializable {

    @SerializedName("debug")
    @Expose
    private String debug;
    @SerializedName("user")
    @Expose
    private String user = null;

    public String getDebug() {
        return debug;
    }

    public void setDebug(String debug) {
        this.debug = debug;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}