package mx.turnmyapp.turn.viewhelper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by alan on 6/09/16.
 */
public class EditTextPicker  extends EditText{

    public EditTextPicker(Context context){
        super(context);
        initOnFocusListener();
        initKeyListener();
    }

    public EditTextPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        initOnFocusListener();
        initKeyListener();
    }

    @Override
    public boolean onCheckIsTextEditor() {
        return false;
    }

    private void initOnFocusListener(){
        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    getEditText().callOnClick();
                    InputMethodManager imm = (InputMethodManager) getEditText().getContext()
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getEditText().getWindowToken(), 0);
                }
            }
        });
    }

    public void initKeyListener(){
        this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return true;
            }
        });
    }

    private EditTextPicker getEditText(){
        return this;
    }
}
